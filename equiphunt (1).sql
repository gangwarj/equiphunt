-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 31, 2017 at 02:43 PM
-- Server version: 5.5.55-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `equiphunt`
--

-- --------------------------------------------------------

--
-- Table structure for table `access`
--

CREATE TABLE IF NOT EXISTS `access` (
  `userId` int(11) NOT NULL,
  `accessLevel` int(11) NOT NULL,
  KEY `userId` (`userId`),
  KEY `accessLevel` (`accessLevel`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `access`
--

INSERT INTO `access` (`userId`, `accessLevel`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(18, 7),
(25, 9),
(26, 9),
(27, 7);

-- --------------------------------------------------------

--
-- Table structure for table `access_level`
--

CREATE TABLE IF NOT EXISTS `access_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `access_level`
--

INSERT INTO `access_level` (`id`, `access`) VALUES
(1, 'renters'),
(2, 'suppliers'),
(3, 'inventory'),
(4, 'IOT'),
(5, 'spareParts'),
(6, 'maintenance'),
(7, 'leads'),
(8, 'vendors'),
(9, 'fulfillment'),
(10, 'order'),
(11, 'field');

-- --------------------------------------------------------

--
-- Table structure for table `attachments`
--

CREATE TABLE IF NOT EXISTS `attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tableId` int(11) NOT NULL,
  `tableName` varchar(100) NOT NULL,
  `url` varchar(100) NOT NULL,
  `urlType` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `call_mail_logs`
--

CREATE TABLE IF NOT EXISTS `call_mail_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerId` int(11) NOT NULL,
  `tableName` varchar(100) NOT NULL,
  `text` varchar(500) DEFAULT NULL,
  `timeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `userId` int(11) NOT NULL,
  `type` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `campaign`
--

CREATE TABLE IF NOT EXISTS `campaign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerId` int(11) NOT NULL,
  `tableName` varchar(100) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `createdBy` int(11) NOT NULL,
  `lastUpdatedOn` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastUpdatedBy` int(11) NOT NULL,
  `deletedOn` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `deletedBy` int(11) DEFAULT NULL,
  `type` varchar(100) NOT NULL,
  `templateId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `campaign_logs`
--

CREATE TABLE IF NOT EXISTS `campaign_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerId` int(11) NOT NULL,
  `timeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `userId` int(11) NOT NULL,
  `tableName` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `templateId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `equipment_type`
--

CREATE TABLE IF NOT EXISTS `equipment_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `equipment_type`
--

INSERT INTO `equipment_type` (`id`, `name`) VALUES
(1, 'Backhoe Loader'),
(2, 'Helicopter'),
(3, 'Aeroplane'),
(4, 'Truck');

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE IF NOT EXISTS `inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(100) NOT NULL,
  `tableName` varchar(100) NOT NULL,
  `equipmentType` varchar(100) DEFAULT NULL,
  `make` varchar(100) DEFAULT NULL,
  `model` varchar(100) DEFAULT NULL,
  `capacityLB` varchar(20) DEFAULT NULL,
  `capacityUB` varchar(20) DEFAULT NULL,
  `year` varchar(100) DEFAULT NULL,
  `specifications` varchar(100) DEFAULT NULL,
  `chassisNo` varchar(100) DEFAULT NULL,
  `engineNo` varchar(100) DEFAULT NULL,
  `registrationNo` varchar(100) DEFAULT NULL,
  `monthlyPrice` varchar(100) DEFAULT NULL,
  `equipmentInspected` tinyint(1) DEFAULT NULL,
  `lastInspectionsDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `serviceArea` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `RCCopy` varchar(100) DEFAULT NULL,
  `Insurance` varchar(100) DEFAULT NULL,
  `PUC` varchar(100) DEFAULT NULL,
  `isActive` tinyint(1) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `createdBy` int(11) NOT NULL,
  `lastUpdatedOn` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `lastUpdatedBy` int(11) DEFAULT NULL,
  `deletedOn` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `leads`
--

CREATE TABLE IF NOT EXISTS `leads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `leadManagerId` int(11) DEFAULT NULL,
  `fulfilmentManagerId` int(11) DEFAULT NULL,
  `LeadOperatorId` int(11) DEFAULT NULL,
  `customerId` int(11) NOT NULL,
  `tableName` varchar(100) NOT NULL,
  `leadPriority` varchar(100) NOT NULL,
  `leadDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isActive` tinyint(1) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `createdBy` int(11) NOT NULL,
  `lastUpdatedOn` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastUpdatedBy` int(11) NOT NULL,
  `deletedOn` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deletedBy` int(11) NOT NULL,
  `movedToOrders` tinyint(1) DEFAULT NULL,
  `accept` tinyint(1) DEFAULT NULL,
  `reject` tinyint(1) DEFAULT NULL,
  `activate` tinyint(1) DEFAULT NULL,
  `leadType` varchar(100) NOT NULL,
  `typeOfWork` int(11) NOT NULL,
  `expectedStartDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sources` int(11) NOT NULL,
  `approvedBy` int(11) DEFAULT NULL,
  `quotationSent` tinyint(1) DEFAULT NULL,
  `renterAcceptQuotation` tinyint(1) DEFAULT NULL,
  `supplierConfirmed` tinyint(1) DEFAULT NULL,
  `paperWorkAndAdvance` tinyint(1) DEFAULT NULL,
  `priceCardSent` tinyint(1) DEFAULT NULL,
  `priceCard` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `leadExecutiveId` (`leadManagerId`),
  KEY `fieldManagerId` (`fulfilmentManagerId`),
  KEY `LeadOperatorId` (`LeadOperatorId`),
  KEY `typeOfWork` (`typeOfWork`),
  KEY `sources` (`sources`),
  KEY `approvedBy` (`approvedBy`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=80 ;

--
-- Dumping data for table `leads`
--

INSERT INTO `leads` (`id`, `leadManagerId`, `fulfilmentManagerId`, `LeadOperatorId`, `customerId`, `tableName`, `leadPriority`, `leadDate`, `isActive`, `createdOn`, `createdBy`, `lastUpdatedOn`, `lastUpdatedBy`, `deletedOn`, `deletedBy`, `movedToOrders`, `accept`, `reject`, `activate`, `leadType`, `typeOfWork`, `expectedStartDate`, `sources`, `approvedBy`, `quotationSent`, `renterAcceptQuotation`, `supplierConfirmed`, `paperWorkAndAdvance`, `priceCardSent`, `priceCard`) VALUES
(76, 18, NULL, NULL, 12, 'renters', 'Hot', '2017-05-30 09:31:30', 1, '2017-05-29 15:33:03', 18, '2017-05-30 09:31:30', 18, '0000-00-00 00:00:00', 0, 0, 0, 0, 1, 'inventory', 1, '0000-00-00 00:00:00', 1, NULL, 0, 0, 0, 0, 0, '2666'),
(77, 18, NULL, 27, 23, 'renters', 'Hot', '2017-05-30 06:25:05', 0, '2017-05-29 15:41:32', 27, '2017-05-30 06:23:33', 18, '2017-05-30 06:25:05', 18, 0, 0, 0, 1, 'inventory', 1, '0000-00-00 00:00:00', 1, NULL, 0, 0, 0, 0, 1, '25698'),
(78, 18, NULL, 27, 22, 'renters', 'Hot', '2017-05-30 13:47:28', 1, '2017-05-29 15:44:39', 27, '2017-05-30 13:47:28', 18, '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 'inventory', 1, '0000-00-00 00:00:00', 1, NULL, 0, 0, 0, 0, 0, '23443'),
(79, 18, NULL, NULL, 24, 'renters', 'Hot', '2017-05-30 13:48:50', 1, '2017-05-29 18:31:23', 18, '2017-05-30 13:48:50', 18, '0000-00-00 00:00:00', 0, 0, 0, 0, 1, 'inventory', 1, '0000-00-00 00:00:00', 1, NULL, 1, 1, 0, 0, 1, 'w545');

-- --------------------------------------------------------

--
-- Table structure for table `lead_equipment`
--

CREATE TABLE IF NOT EXISTS `lead_equipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fulfilmentOperator` int(11) DEFAULT NULL,
  `equipmentId` int(11) DEFAULT NULL,
  `leadId` int(11) DEFAULT NULL,
  `cancelReason` varchar(200) DEFAULT NULL,
  `isCancel` tinyint(1) DEFAULT NULL,
  `makeId` int(11) DEFAULT NULL,
  `location` varchar(100) NOT NULL,
  `duration` varchar(100) NOT NULL,
  `modelId` int(11) NOT NULL,
  `district` varchar(100) NOT NULL,
  `shiftType` varchar(100) NOT NULL,
  `expectedStartDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `projectStage` varchar(100) NOT NULL,
  `operatorAllowance` varchar(100) NOT NULL,
  `batha` varchar(100) NOT NULL,
  `food` tinyint(1) NOT NULL,
  `accommodation` tinyint(1) NOT NULL,
  `year` varchar(100) DEFAULT NULL,
  `operationalHoursInADay` varchar(100) NOT NULL,
  `operationalDaysInAMonth` varchar(100) NOT NULL,
  `vehicleDocuments` int(11) NOT NULL,
  `attachmentId` int(11) DEFAULT NULL,
  `operatorLicense` int(11) NOT NULL,
  `shiftTypeDay` tinyint(1) DEFAULT NULL,
  `shiftTypeNight` tinyint(1) DEFAULT NULL,
  `priceTag` varchar(100) NOT NULL,
  `operationalHoursInAMonth` varchar(100) NOT NULL,
  `quantity` varchar(100) NOT NULL,
  `supplierPhone` varchar(100) NOT NULL,
  `confirmed` tinyint(1) NOT NULL,
  `capacity` varchar(100) NOT NULL,
  `remarks` text,
  `supplierName1` varchar(100) DEFAULT NULL,
  `supplierName2` varchar(100) DEFAULT NULL,
  `supplierName3` varchar(100) DEFAULT NULL,
  `supplierPhone1` varchar(100) DEFAULT NULL,
  `supplierPhone2` varchar(100) DEFAULT NULL,
  `supplierPhone3` varchar(100) DEFAULT NULL,
  `attachment` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `equipmentId` (`equipmentId`),
  KEY `leadId` (`leadId`),
  KEY `makeId` (`makeId`),
  KEY `modelId` (`modelId`),
  KEY `vehicleDocuments` (`vehicleDocuments`),
  KEY `operatorLicense` (`operatorLicense`),
  KEY `fieldOperator` (`fulfilmentOperator`),
  KEY `attachmentId` (`attachmentId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=65 ;

--
-- Dumping data for table `lead_equipment`
--

INSERT INTO `lead_equipment` (`id`, `fulfilmentOperator`, `equipmentId`, `leadId`, `cancelReason`, `isCancel`, `makeId`, `location`, `duration`, `modelId`, `district`, `shiftType`, `expectedStartDate`, `projectStage`, `operatorAllowance`, `batha`, `food`, `accommodation`, `year`, `operationalHoursInADay`, `operationalDaysInAMonth`, `vehicleDocuments`, `attachmentId`, `operatorLicense`, `shiftTypeDay`, `shiftTypeNight`, `priceTag`, `operationalHoursInAMonth`, `quantity`, `supplierPhone`, `confirmed`, `capacity`, `remarks`, `supplierName1`, `supplierName2`, `supplierName3`, `supplierPhone1`, `supplierPhone2`, `supplierPhone3`, `attachment`) VALUES
(42, NULL, 1, NULL, NULL, NULL, 1, 'sgdsdg', '', 1, 'sdgsdg', '', '2017-05-31 09:08:45', 'dgsdg', '0', '', 1, 1, '2018', 'dsgsd', 'sdgsdg', 1, NULL, 1, 1, 0, '', 'sdgsd', 'sdgsdg', '', 0, '', 'Changes Needed.', 'Supplier Name1', 'Supplier Name2', '', 'Phone1', 'Phone2', '', NULL),
(43, NULL, 1, NULL, NULL, NULL, 1, 'sgdsdg', '', 1, 'sdgsdg', '', '2017-05-31 09:09:50', 'dgsdg', '0', '', 1, 1, '2018', 'dsgsd', 'sdgsdg', 1, NULL, 1, 1, 0, '', 'sdgsd', 'sdgsdg', '', 0, '', 'fdbdfdfb', 'ffbfdbfb', 'fdbdfbfd', 'dfbfbddfb', 'fdbfdbfdb', 'bdfbdfb', 'fdbdfbfd', 'uploads/image1.jpg'),
(44, NULL, 1, NULL, NULL, NULL, 1, 'sgdsdg', '', 1, 'sdgsdg', '', '2017-05-31 09:06:11', 'dgsdg', '0', '', 1, 1, '2018', 'dsgsd', 'sdgsdg', 1, NULL, 1, 1, 0, '', 'sdgsd', 'sdgsdg', '', 0, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(45, NULL, 1, NULL, NULL, NULL, 1, 'sgdsdg', '', 1, 'sdgsdg', '', '2017-05-31 09:06:11', 'dgsdg', '0', '', 1, 1, '2018', 'dsgsd', 'sdgsdg', 1, NULL, 1, 1, 0, '', 'sdgsd', 'sdgsdg', '', 0, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(46, NULL, 1, NULL, NULL, NULL, 1, 'sgdsdg', '', 1, 'sdgsdg', '', '2017-05-31 09:06:11', 'dgsdg', '0', '', 1, 1, '2018', 'dsgsd', 'sdgsdg', 1, NULL, 1, 1, 0, '', 'sdgsd', 'sdgsdg', '', 0, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(47, NULL, 1, NULL, NULL, NULL, 1, 'sgdsdg', '', 1, 'sdgsdg', '', '2017-05-31 09:07:51', 'dgsdg', '0', '', 1, 1, '2018', 'dsgsd', 'sdgsdg', 1, NULL, 1, 1, 0, '', 'sdgsd', 'sdgsdg', '', 0, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, NULL, 3, NULL, NULL, NULL, 1, 'sdvsdv', '', 1, 'dsvsdv', '', '2017-05-31 09:07:51', 'dsvsdv', '1', '', 1, 1, '2018', 'sdvsdv', 'sdvsdv', 1, NULL, 1, 1, 0, '', 'sdvsv', 'sdvsdv', '', 0, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, NULL, 1, NULL, NULL, NULL, 1, 'sgdsdg', '', 1, 'sdgsdg', '', '2017-05-31 09:07:51', 'dgsdg', '0', '', 1, 1, '2018', 'dsgsd', 'sdgsdg', 1, NULL, 1, 1, 0, '', 'sdgsd', 'sdgsdg', '', 0, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(50, NULL, 3, NULL, NULL, NULL, 1, 'sdvsdv', '', 3, 'dsvsdv', '', '2017-05-31 09:06:11', 'dsvsdv', '1', '', 1, 1, '2018', 'sdvsdv', 'sdvsdv', 1, NULL, 1, 1, 0, '', 'sdvsv', 'sdvsdv', '', 0, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(51, NULL, 1, NULL, NULL, NULL, 1, 'asdfgh', '', 1, 'sdfg', '', '2017-05-31 09:06:11', 'zsx', '1', '', 1, 1, '2018', 'asdfg', 'qwfg', 1, NULL, 1, 1, 0, '', 'sdfgh', 'sdfg', '', 0, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(52, NULL, 1, NULL, NULL, NULL, 1, 'ghj', '', 1, 'sdf', '', '2017-05-31 09:06:11', 'sdfg', '0', '', 0, 0, '412', 'dfg', 'sdfgh', 0, NULL, 0, 1, 0, '', 'fghjm,', 'dfghj', '', 0, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(53, NULL, 1, NULL, NULL, NULL, 1, 'asda', '', 1, 'jhgfd', '', '2017-05-31 09:06:11', 'asdfgh', '1', '', 1, 1, '2015', 'sdfghjkl', 'rfgtui', 1, NULL, 1, 1, 1, '', 'ashjk', 'rfkl', '', 0, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(54, NULL, 1, NULL, NULL, NULL, 1, 'asda', '', 1, 'jhgfd', '', '2017-05-31 09:06:11', 'asdfgh', '0', '', 1, 1, '2015', 'sdfghjkl', 'rfgtui', 1, NULL, 1, 1, 1, '', 'ashjk', 'rfkl', '', 0, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(55, NULL, 1, NULL, NULL, NULL, 1, 'asda', '', 1, 'jhgfd', '', '2017-05-31 09:06:11', 'asdfgh', '0', '', 1, 1, '2015', 'sdfghjkl', 'rfgtui', 1, NULL, 1, 1, 1, '', 'ashjk', 'rfkl', '', 0, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(56, 26, 1, 76, NULL, NULL, 1, 'asda', '', 1, 'jhgfd', '', '2017-05-31 09:06:11', 'asdfgh', '0', '', 1, 1, '2015', 'sdfghjkl', 'rfgtui', 1, NULL, 1, 1, 1, '', 'ashjk', 'rfkl', '', 0, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(57, NULL, 1, NULL, NULL, NULL, 1, 'asdfgb', '', 1, 'sdf', '', '2017-05-31 09:06:11', 'sdf', '1', '', 1, 1, '', 'gfbfb', 'bzfdt', 1, NULL, 1, 1, 0, '', 'fde', ' fgnh', '', 0, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(58, NULL, 1, NULL, NULL, NULL, 1, 'asdfgb', '', 1, 'sdf', '', '2017-05-31 09:06:11', 'sdf', '0', '', 1, 1, '', 'gfbfb', 'bzfdt', 1, NULL, 1, 1, 0, '', 'fde', ' fgnh', '', 0, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, NULL, 1, NULL, NULL, NULL, 1, 'asdfgb', '', 1, 'sdf', '', '2017-05-31 09:06:11', 'sdf', '0', '', 1, 1, '', 'gfbfb', 'bzfdt', 1, NULL, 1, 1, 0, '', 'fde', ' fgnh', '', 0, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(60, NULL, 1, 78, NULL, NULL, 1, 'ugyh', '', 1, 'ikjn', '', '2017-05-31 09:06:11', 'hj', '1', '', 1, 1, '', 'iuhj', 'dghk', 1, NULL, 1, 1, 0, '', 'ibjk', 'ikjn', '', 0, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, NULL, 1, NULL, NULL, NULL, 1, 'asdfgb', '', 1, 'sdf', '', '2017-05-31 09:06:11', 'sdf', '0', '', 1, 1, '', 'gfbfb', 'bzfdt', 1, NULL, 1, 1, 0, '', 'fde', ' fgnh', '', 0, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, NULL, 1, NULL, NULL, NULL, 1, 'dsgsdg', '', 1, 'xcbc', '', '2017-05-31 09:06:11', 'cbxcxb', '1', '', 1, 1, '1234', 'bxcb', 'cxbcxb', 1, NULL, 1, 1, 0, '', 'xbcxbxcb', 'cxbcxb', '', 0, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(63, NULL, 1, 79, NULL, NULL, 1, 'dsgsdg', '', 1, 'xcbc', '', '2017-05-31 09:07:51', 'cbxcxb', '0', '', 1, 1, '1234', 'bxcb', 'cxbcxb', 1, NULL, 1, 1, 0, '', 'xbcxbxcb', 'cxbcxb', '', 0, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(64, NULL, 1, NULL, NULL, NULL, 1, 'asdf', '', 1, 'sadbn', '', '2017-05-31 09:06:11', 'asdfghj', '1', '', 1, 1, '2018', 'sdfj', 'aZSnmjk,', 1, NULL, 1, 1, 0, '', 'fghjk', 'sdfghj,', '', 0, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `maintenance`
--

CREATE TABLE IF NOT EXISTS `maintenance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `phone` varchar(20) NOT NULL,
  `tableName` varchar(100) NOT NULL,
  `category` varchar(30) DEFAULT NULL,
  `authorizedServiceCentreOrNormalWorkShop` varchar(20) DEFAULT NULL,
  `fieldExecutiveAvailable` tinyint(1) DEFAULT NULL,
  `radiusInWhichServicePartnerOperates` varchar(20) DEFAULT NULL,
  `isActive` tinyint(1) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `createdBy` int(11) NOT NULL,
  `lastUpdatedOn` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `lastUpdatedBy` int(11) DEFAULT NULL,
  `deletedOn` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `phone` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `make`
--

CREATE TABLE IF NOT EXISTS `make` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `make`
--

INSERT INTO `make` (`id`, `name`) VALUES
(1, 'JCB'),
(2, 'Hitler');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `tableName` varchar(100) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `text` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `model`
--

CREATE TABLE IF NOT EXISTS `model` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Model` varchar(100) NOT NULL,
  `CapacityLB` varchar(50) NOT NULL,
  `CapacityUB` varchar(50) NOT NULL,
  `year` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `model`
--

INSERT INTO `model` (`id`, `Model`, `CapacityLB`, `CapacityUB`, `year`) VALUES
(1, 'Hxcs', '12', '15', '2012'),
(2, 'RESD', '18', '20', '2013'),
(3, 'QWER', '56', '90', '2014');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderManagerId` int(11) NOT NULL,
  `orderStartDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `leadId` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `tableName` varchar(100) NOT NULL,
  `renterRating` varchar(100) NOT NULL,
  `isActive` tinyint(1) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `createdBy` int(11) NOT NULL,
  `lastUpdatedOn` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastUpdatedBy` int(11) NOT NULL,
  `deletedOn` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deletedBy` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `leadId` (`leadId`),
  KEY `orderManager` (`orderManagerId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `order_equipment`
--

CREATE TABLE IF NOT EXISTS `order_equipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderId` int(11) NOT NULL,
  `orderOperatorId` int(11) NOT NULL,
  `fieldOperatorId` int(11) DEFAULT NULL,
  `duration` varchar(100) NOT NULL,
  `siteLocation` varchar(100) NOT NULL,
  `equipmentId` int(11) NOT NULL,
  `makeId` int(11) NOT NULL,
  `modelId` int(11) NOT NULL,
  `district` varchar(100) NOT NULL,
  `shiftType` varchar(100) NOT NULL,
  `expectedStartDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `projectStage` varchar(100) NOT NULL,
  `operatorAllowance` varchar(100) NOT NULL,
  `batha` varchar(100) NOT NULL,
  `food` tinyint(1) NOT NULL,
  `accommodation` tinyint(1) NOT NULL,
  `operationalHoursInADay` varchar(100) NOT NULL,
  `operationalDaysInAMonth` varchar(100) NOT NULL,
  `vehicleDocuments` int(11) NOT NULL,
  `operatorLicense` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `orderExecutiveId` (`orderOperatorId`),
  KEY `equipmentId` (`equipmentId`),
  KEY `makeId` (`makeId`),
  KEY `modelId` (`modelId`),
  KEY `vehicleDocuments` (`vehicleDocuments`),
  KEY `operatorLicense` (`operatorLicense`),
  KEY `fieldOperatorId` (`fieldOperatorId`),
  KEY `orderId` (`orderId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `renters`
--

CREATE TABLE IF NOT EXISTS `renters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `renterName` varchar(100) DEFAULT NULL,
  `smsEnrolled` tinyint(1) DEFAULT NULL,
  `renterType` varchar(100) DEFAULT NULL,
  `companyOrIndividual` tinyint(1) DEFAULT NULL,
  `pocName` varchar(100) DEFAULT NULL,
  `pocDesignation` varchar(100) DEFAULT NULL,
  `phone` varchar(100) NOT NULL,
  `alternateContactNo` varchar(100) DEFAULT NULL,
  `emailId` varchar(100) DEFAULT NULL,
  `businessDomain` varchar(100) DEFAULT NULL,
  `officeLocation` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `howDoWeKnowClient` varchar(100) DEFAULT NULL,
  `fromWhereDidClientGetToKnowUs` varchar(100) DEFAULT NULL,
  `clientFirstContactDate` timestamp NULL DEFAULT NULL,
  `salesExecutive` varchar(100) DEFAULT NULL,
  `meetingDoneInPerson` tinyint(1) DEFAULT NULL,
  `meetingRemarks` varchar(500) DEFAULT NULL,
  `followUpDate` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `clientStatus` varchar(100) DEFAULT NULL,
  `renterInterestLevel` varchar(100) DEFAULT NULL,
  `noOfEnquiresTillNow` varchar(100) DEFAULT NULL,
  `emailEnrolled` tinyint(1) DEFAULT NULL,
  `renterActiveProjects` varchar(100) DEFAULT NULL,
  `renterUpcomingProjects` varchar(500) DEFAULT NULL,
  `turnoverOfCompany` varchar(100) DEFAULT NULL,
  `frequencyOfEquipmentUsage` varchar(100) DEFAULT NULL,
  `noOfOrdersDoneTillNow` varchar(100) DEFAULT NULL,
  `renterPaymentPolicy` varchar(100) DEFAULT NULL,
  `renterPaymentHistory` varchar(100) DEFAULT NULL,
  `paymentCycle` varchar(100) DEFAULT NULL,
  `advance` varchar(100) DEFAULT NULL,
  `serviceTax` varchar(100) DEFAULT NULL,
  `secondaryResearchOnline` varchar(100) DEFAULT NULL,
  `feedbackOfRenterFromSupplier` varchar(100) DEFAULT NULL,
  `isActive` tinyint(1) NOT NULL,
  `isVerified` tinyint(1) DEFAULT NULL,
  `verifiedBy` int(11) DEFAULT NULL,
  `createdOn` timestamp NULL DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `lastUpdatedOn` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastUpdatedBy` int(11) DEFAULT NULL,
  `deletedOn` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phone` (`phone`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `renters`
--

INSERT INTO `renters` (`id`, `userId`, `renterName`, `smsEnrolled`, `renterType`, `companyOrIndividual`, `pocName`, `pocDesignation`, `phone`, `alternateContactNo`, `emailId`, `businessDomain`, `officeLocation`, `city`, `address`, `howDoWeKnowClient`, `fromWhereDidClientGetToKnowUs`, `clientFirstContactDate`, `salesExecutive`, `meetingDoneInPerson`, `meetingRemarks`, `followUpDate`, `clientStatus`, `renterInterestLevel`, `noOfEnquiresTillNow`, `emailEnrolled`, `renterActiveProjects`, `renterUpcomingProjects`, `turnoverOfCompany`, `frequencyOfEquipmentUsage`, `noOfOrdersDoneTillNow`, `renterPaymentPolicy`, `renterPaymentHistory`, `paymentCycle`, `advance`, `serviceTax`, `secondaryResearchOnline`, `feedbackOfRenterFromSupplier`, `isActive`, `isVerified`, `verifiedBy`, `createdOn`, `createdBy`, `lastUpdatedOn`, `lastUpdatedBy`, `deletedOn`, `deletedBy`) VALUES
(11, NULL, 'hello2', NULL, NULL, NULL, NULL, NULL, '3434343434', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 18, '0000-00-00 00:00:00', 18, '0000-00-00 00:00:00', NULL),
(12, NULL, 'lead manager', NULL, NULL, NULL, NULL, NULL, '1236547985', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 18, '0000-00-00 00:00:00', 18, '0000-00-00 00:00:00', NULL),
(13, NULL, 'qwer', NULL, NULL, NULL, NULL, NULL, '1234567892', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 18, '0000-00-00 00:00:00', 18, '0000-00-00 00:00:00', NULL),
(14, NULL, 'qwerty', NULL, NULL, NULL, NULL, NULL, '1234567890', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 18, '0000-00-00 00:00:00', 18, '0000-00-00 00:00:00', NULL),
(15, NULL, '12345678', NULL, NULL, NULL, NULL, NULL, '456789', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 18, '0000-00-00 00:00:00', 18, '0000-00-00 00:00:00', NULL),
(16, NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 18, '0000-00-00 00:00:00', 18, '0000-00-00 00:00:00', NULL),
(17, NULL, 'gaurav', NULL, NULL, NULL, NULL, NULL, '8602229193', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 18, '0000-00-00 00:00:00', 18, '0000-00-00 00:00:00', NULL),
(18, NULL, 'azswfgthjk', NULL, NULL, NULL, NULL, NULL, '1236547874', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 18, '0000-00-00 00:00:00', 18, '0000-00-00 00:00:00', NULL),
(19, NULL, 'leads op', NULL, NULL, NULL, NULL, NULL, '2569874582', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 27, '0000-00-00 00:00:00', 27, '0000-00-00 00:00:00', NULL),
(20, NULL, 'lead op', NULL, NULL, NULL, NULL, NULL, '8523641578', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 27, '0000-00-00 00:00:00', 27, '0000-00-00 00:00:00', NULL),
(21, NULL, 'lead op', NULL, NULL, NULL, NULL, NULL, '7412589654', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 27, '0000-00-00 00:00:00', 27, '0000-00-00 00:00:00', NULL),
(22, NULL, 'lead op', NULL, NULL, NULL, NULL, NULL, '8569874528', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 27, '0000-00-00 00:00:00', 27, '0000-00-00 00:00:00', NULL),
(23, NULL, 'lead op2', NULL, NULL, NULL, NULL, NULL, '2354125987', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 27, '0000-00-00 00:00:00', 27, '0000-00-00 00:00:00', NULL),
(24, NULL, 'asdfghjk', NULL, NULL, NULL, NULL, NULL, '9876543215', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 18, '0000-00-00 00:00:00', 18, '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sources`
--

CREATE TABLE IF NOT EXISTS `sources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sources`
--

INSERT INTO `sources` (`id`, `name`) VALUES
(1, 'source1');

-- --------------------------------------------------------

--
-- Table structure for table `staff_login`
--

CREATE TABLE IF NOT EXISTS `staff_login` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(10) NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(20) NOT NULL,
  `majorRole` varchar(20) NOT NULL,
  `managerId` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `isActive` tinyint(1) NOT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `phone` (`phone`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `staff_login`
--

INSERT INTO `staff_login` (`userId`, `phone`, `password`, `name`, `email`, `majorRole`, `managerId`, `adminId`, `isActive`) VALUES
(1, '9876543210', '8bb0cf6eb9b17d0f7d22b456f121257dc1254e1f01665370476383ea776df414', 'sdfsdfsdf', 'dfdf@sdfsdf.dff', 'admin', 0, 0, 1),
(18, '0123456789', '8bb0cf6eb9b17d0f7d22b456f121257dc1254e1f01665370476383ea776df414', 'Jitender', 'asdf@asd.com', 'manager', 0, 1, 1),
(23, '9876543214', '4eac34814996b1abaaddb7e7b97f19dcc3bda76025e0f7fa5b8dfcf9facd759e', 'dfdsf', 'd@d.com', 'manager', 0, 1, 0),
(24, '9876543212', '96ed0d46381553677984e6238a3cbc153cc5bd52deaa6f28288d23cb8473c29b', 'sdgsdgdsg', 'h@h.com', 'manager', 0, 1, 0),
(25, '9876543219', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 'Gaurav Sharma', 'gaurav@gaurav.org', 'manager', 0, 1, 1),
(26, '9874563210', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 'asdfg', 'asdaf@asdafs.com', 'operator', 25, 0, 1),
(27, '9632587410', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 'qwerty', 'qwerty@qwert.com', 'operator', 18, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE IF NOT EXISTS `suppliers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `companyName` varchar(100) DEFAULT NULL,
  `OwnerName` varchar(100) DEFAULT NULL,
  `POCName` varchar(100) DEFAULT NULL,
  `POCDesignation` varchar(100) DEFAULT NULL,
  `phone` varchar(100) NOT NULL,
  `AlternateContactNo` varchar(100) DEFAULT NULL,
  `EmailId` varchar(100) DEFAULT NULL,
  `OfficeLocation` varchar(500) DEFAULT NULL,
  `City` varchar(100) DEFAULT NULL,
  `Address` varchar(500) DEFAULT NULL,
  `ServiceArea` varchar(100) DEFAULT NULL,
  `CategoriesOfEquipment` varchar(100) DEFAULT NULL,
  `HowDoWeKnowClient` varchar(100) DEFAULT NULL,
  `FromWhereDidClientKnowAboutUs` varchar(100) DEFAULT NULL,
  `currentModeOfLeadGeneration` varchar(100) DEFAULT NULL,
  `clientFirstContactDate` varchar(100) DEFAULT NULL,
  `numberOfTimesEngaged` varchar(100) DEFAULT NULL,
  `lastEngagementDate` varchar(100) DEFAULT NULL,
  `supplierExecutive` varchar(100) DEFAULT NULL,
  `verificationType` varchar(100) DEFAULT NULL,
  `Onboarded` varchar(100) DEFAULT NULL,
  `supplierInterestLevel` varchar(100) DEFAULT NULL,
  `supplierStatus` varchar(100) DEFAULT NULL,
  `smsAndEmailEnrollment` varchar(100) DEFAULT NULL,
  `remarks` varchar(100) DEFAULT NULL,
  `supplierPaymentPolicy` varchar(100) DEFAULT NULL,
  `equipmentOnEmiOrSelfFinanced` varchar(100) DEFAULT NULL,
  `PANCard` varchar(100) DEFAULT NULL,
  `accountDetails` varchar(500) DEFAULT NULL,
  `IDProof` varchar(100) DEFAULT NULL,
  `isActive` tinyint(1) NOT NULL,
  `isVerified` tinyint(1) DEFAULT NULL,
  `verifiedBy` int(11) DEFAULT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `createdBy` int(11) NOT NULL,
  `lastUpdatedOn` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastUpdatedBy` int(11) NOT NULL,
  `deletedOn` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `deletedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ContactNo` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `type_of_work`
--

CREATE TABLE IF NOT EXISTS `type_of_work` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `type_of_work`
--

INSERT INTO `type_of_work` (`id`, `name`) VALUES
(1, 'type1');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `access`
--
ALTER TABLE `access`
  ADD CONSTRAINT `access` FOREIGN KEY (`accessLevel`) REFERENCES `access_level` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user` FOREIGN KEY (`userId`) REFERENCES `staff_login` (`userId`) ON DELETE CASCADE;

--
-- Constraints for table `leads`
--
ALTER TABLE `leads`
  ADD CONSTRAINT `approvedBy` FOREIGN KEY (`approvedBy`) REFERENCES `staff_login` (`userId`) ON DELETE CASCADE,
  ADD CONSTRAINT `field_manager` FOREIGN KEY (`fulfilmentManagerId`) REFERENCES `staff_login` (`userId`) ON DELETE CASCADE,
  ADD CONSTRAINT `lead_manager` FOREIGN KEY (`leadManagerId`) REFERENCES `staff_login` (`userId`) ON DELETE CASCADE,
  ADD CONSTRAINT `lead_operator` FOREIGN KEY (`LeadOperatorId`) REFERENCES `staff_login` (`userId`) ON DELETE CASCADE,
  ADD CONSTRAINT `sources` FOREIGN KEY (`sources`) REFERENCES `sources` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `typeOfWork` FOREIGN KEY (`typeOfWork`) REFERENCES `type_of_work` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lead_equipment`
--
ALTER TABLE `lead_equipment`
  ADD CONSTRAINT `equipmentId` FOREIGN KEY (`equipmentId`) REFERENCES `equipment_type` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `field_operator` FOREIGN KEY (`fulfilmentOperator`) REFERENCES `staff_login` (`userId`) ON DELETE CASCADE,
  ADD CONSTRAINT `leadId` FOREIGN KEY (`leadId`) REFERENCES `leads` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `lead_equipment_ibfk_1` FOREIGN KEY (`attachmentId`) REFERENCES `attachments` (`id`),
  ADD CONSTRAINT `makeId` FOREIGN KEY (`makeId`) REFERENCES `make` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `modelId` FOREIGN KEY (`modelId`) REFERENCES `model` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orderManager` FOREIGN KEY (`orderManagerId`) REFERENCES `staff_login` (`userId`) ON DELETE CASCADE;

--
-- Constraints for table `order_equipment`
--
ALTER TABLE `order_equipment`
  ADD CONSTRAINT `equipment_type` FOREIGN KEY (`equipmentId`) REFERENCES `equipment_type` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fieldOperator` FOREIGN KEY (`fieldOperatorId`) REFERENCES `staff_login` (`userId`) ON DELETE CASCADE,
  ADD CONSTRAINT `make` FOREIGN KEY (`makeId`) REFERENCES `make` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `model` FOREIGN KEY (`modelId`) REFERENCES `model` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `operatorLicense` FOREIGN KEY (`operatorLicense`) REFERENCES `attachments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orderExecutiveId` FOREIGN KEY (`orderOperatorId`) REFERENCES `staff_login` (`userId`) ON DELETE CASCADE,
  ADD CONSTRAINT `orderId` FOREIGN KEY (`orderId`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orderOperator` FOREIGN KEY (`orderOperatorId`) REFERENCES `staff_login` (`userId`) ON DELETE CASCADE,
  ADD CONSTRAINT `vehicleDocs` FOREIGN KEY (`vehicleDocuments`) REFERENCES `attachments` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
