<?php

include('config.php');
include('leadClass.php');
$leadClass = new leadClass();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    http_response_code($badRequest);
}

if(!isset($_SESSION['userId']) || empty($_SESSION['userId'])){
    session_destroy();
    http_response_code($session_error);
}

$phone=$_POST['phone'];
$table=$_POST['type'];
if($table=="renters")
{
	$lid=$leadClass->getSuggestionsRenters($phone);
}
else if($table=="suppliers")
{
	$lid=$leadClass->getSuggestionsSuppliers($phone);
}
else if($table=="fulfilment operators")
{
	$lid=$leadClass->getSuggestionsFulfilmentOperators($phone);
}
if($lid)
{
	echo json_encode($lid);
	http_response_code($success);
}
else
{
	http_response_code($noContent);
}

?>

