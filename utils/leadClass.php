<?php
class leadClass{
	public function leadUserLogin($phone,$password)
     {
          try{
               $db = getDB();
               $accessSpecifier="leads";
               $hash_password= hash('sha256', $password);
               $stmt = $db->prepare("SELECT userId,majorRole FROM staff_login WHERE phone=:phone AND  password=:hash_password");
               $stmt->bindParam("phone", $phone,PDO::PARAM_STR) ;
               $stmt->bindParam("hash_password", $hash_password,PDO::PARAM_STR) ;
               $stmt->execute();
               $count=$stmt->rowCount();
               $data=$stmt->fetch(PDO::FETCH_OBJ);

               if($count==1)
               {
                    $stmt = $db->prepare("SELECT access FROM access_level INNER JOIN access ON access.accessLevel=access_level.id AND access.userId=:id");
                    $stmt->bindParam("id", $data->userId,PDO::PARAM_STR) ;
                    $stmt->execute();
                    $count=$stmt->rowCount();
                    $access=$stmt->fetchAll(PDO::FETCH_OBJ);
                    $db = null;
                    $accessLevelArray=array();
                    for($i=0;$i<count($access);$i++)
                    {
                         $accessData=($access[$i]);
                         array_push($accessLevelArray,$accessData->access);
                         
                    }
                    echo var_dump($accessLevelArray);
                    if(in_array("leads",$accessLevelArray)||in_array("fulfillment",$accessLevelArray))
                    {	
	                    $_SESSION['userId']=$data->userId;
	                    $_SESSION['majorRole']=$data->majorRole;
	                    $_SESSION['accessLevel']=$accessLevelArray;
	                    return true;
                    }
                    else
                    {
                    	return false;
                    }
               }
               else
               {
                    return false;
               }
          } 
          catch(PDOException $e) {
           http_response_code($GLOBALS['connection_error']);
           echo $e->getMessage(); 
          }
     }

	public function getEquipmentDetails($id)
     {
     	try{

	          $db = getDB();
	          $isActive=1;
	          $stmt = $db->prepare("SELECT * FROM lead_equipment WHERE id=:id");
	          $stmt->bindParam("id", $id,PDO::PARAM_BOOL);
	          $stmt->execute();
	          $data=$stmt->fetch(PDO::FETCH_OBJ);
	          $db = null;
	          $responseObj=new stdClass;
	          if($data)
	          {
	          		$responseObj->model=$this->getNameFromModelId($data->modelId);
	          		$responseObj->equipName=$this->getNameFromEquipmentId($data->equipmentId);
	          		$responseObj->make=$this->getNameFromMakeId($data->makeId);
	          		$responseObj->jobLocation=$data->location;
	          		$responseObj->district=$data->district;
	          		$responseObj->shiftTypeDay=$data->shiftTypeDay;
	          		$responseObj->stage=$data->projectStage;
	          		$responseObj->shiftTypeNight=$data->shiftTypeNight;
	          		$responseObj->startDate=$data->expectedStartDate;
	          		$responseObj->operationalHoursInADay=$data->operationalHoursInADay;
	          		$responseObj->operationalDaysInAMonth=$data->operationalDaysInAMonth;
	          		$responseObj->operationalHoursInAMonth=$data->operationalHoursInAMonth;
	          		$responseObj->vehicleDocuments=$data->vehicleDocuments;
	          		$responseObj->operatorLicense=$data->operatorLicense;
	          		$responseObj->capacity=$data->capacity;
	          		$responseObj->year=$data->year;
	          		$responseObj->operatorAllowance=$data->operatorAllowance;
	          		return $responseObj;
	          }
	          else
	          {
	          		http_response_code($GLOBALS['noContent']);
          			return false;
	          } 	
     	}
	      catch(PDOException $e) {
	      	http_response_code($GLOBALS['connection_error']);
          	echo $e->getMessage(); 
	      }   
     }

	public function getLeadsNotActivated()
	 {
 		 $responseObj=new stdClass;
     	 try{
			$db = getDB();
			$isActive=1;
			$activate=0;
			if((in_array("leads", $_SESSION["accessLevel"]))&&($_SESSION['majorRole']=='manager'))
			{
				$stmt = $db->prepare("SELECT * FROM leads WHERE leadManagerId=:leadManagerId AND isActive=:isActive AND activate=:activate");
				$stmt->bindParam("leadManagerId", $_SESSION['userId'],PDO::PARAM_INT);
	          	$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
				$stmt->bindParam("activate", $activate,PDO::PARAM_BOOL);
				$stmt->execute();
				$responseObj->columns=array("Activate","Reject");
			}
			else if((in_array("leads", $_SESSION["accessLevel"]))&&($_SESSION['majorRole']=='operator'))
			{
				$stmt = $db->prepare("SELECT * FROM leads WHERE LeadOperatorId=:LeadOperatorId AND isActive=:isActive AND activate=:activate");
				$stmt->bindParam("LeadOperatorId", $_SESSION['userId'],PDO::PARAM_INT);
					$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
				$stmt->bindParam("activate", $activate,PDO::PARAM_BOOL);
				$stmt->execute();
				$responseObj->columns=array("Activate");
			}
			else
			{
				http_response_code($GLOBALS['unauthorized']);
			}
			$data=$stmt->fetchAll(PDO::FETCH_OBJ);
			$leadArray=$this->getLeadArray($data);
			$db = null;
			$responseObj->leadArray=$leadArray;
			return $responseObj;
	        
     	}
     	 catch(PDOException $e) {
      			http_response_code($GLOBALS['connection_error']);
      			$responseObj->status=$e->getMessage();
	      }
     }

    public function getLeadsActivatedButPriceNotSent()
     {
 		 $responseObj=new stdClass;
     	 try{

			$db = getDB();
			$isActive=1;
			$activate=1;
			$priceCardSent=0;
			if((in_array("leads", $_SESSION["accessLevel"]))&&($_SESSION['majorRole']=='manager'))
			{
				$stmt = $db->prepare("SELECT * FROM leads WHERE leadManagerId=:leadManagerId AND isActive=:isActive AND activate=:activate AND priceCardSent=:priceCardSent");
				$stmt->bindParam("leadManagerId", $_SESSION['userId'],PDO::PARAM_INT);
	          	$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
				$stmt->bindParam("activate", $activate,PDO::PARAM_BOOL);
				$stmt->bindParam("priceCardSent", $priceCardSent,PDO::PARAM_BOOL);
				$stmt->execute();
				$responseObj->columns=array("Deactivate","Reject","Send Price Card");
			}
			else if((in_array("fulfillment", $_SESSION["accessLevel"]))&&($_SESSION['majorRole']=='manager'))
			{
				$stmt = $db->prepare("SELECT * FROM leads WHERE isActive=:isActive AND activate=:activate AND priceCardSent=:priceCardSent");
				// $stmt->bindParam("fulfilmentManagerId", $_SESSION['userId'],PDO::PARAM_INT);
	          	$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
				$stmt->bindParam("activate", $activate,PDO::PARAM_BOOL);
				$stmt->bindParam("priceCardSent", $priceCardSent,PDO::PARAM_BOOL);
				$stmt->execute();
				$responseObj->columns=array("Reject");
			}
			else if((in_array("leads", $_SESSION["accessLevel"]))&&($_SESSION['majorRole']=='operator'))
			{
				$stmt = $db->prepare("SELECT * FROM leads WHERE LeadOperatorId=:LeadOperatorId AND isActive=:isActive AND activate=:activate AND priceCardSent=:priceCardSent");
				$stmt->bindParam("LeadOperatorId", $_SESSION['userId'],PDO::PARAM_INT);
					$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
				$stmt->bindParam("activate", $activate,PDO::PARAM_BOOL);
				$stmt->bindParam("priceCardSent", $priceCardSent,PDO::PARAM_BOOL);
				$stmt->execute();
				$responseObj->columns=array("Deactivate","Send Price Card");
			}
			else
			{
				http_response_code($GLOBALS['unauthorized']);
				// echo "here";
			}
			$data=$stmt->fetchAll(PDO::FETCH_OBJ);
			$leadArray=$this->getLeadArray($data);
			$db = null;
			$responseObj->leadArray=$leadArray;
			return $responseObj;
	        
     	}
	      catch(PDOException $e) {
	      			http_response_code($GLOBALS['connection_error']);
          			 // echo $e->getMessage();
	      }
     }
    
    public function getLeadsPriceSentButQuotationNotSent()
     {
 		 $responseObj=new stdClass;
     	 try{
     	 	$db=null;
			$db = getDB();
			$isActive=1;
			$activate=1;
			$priceCardSent=1;
			$quotationSent=0;
			if( (in_array("leads", $_SESSION["accessLevel"])) && ($_SESSION['majorRole']=='manager') )
			{
				// echo "hey there";
				// echo $isActive."\n".$activate."\n".$priceCardSent."\n".$quotationSent."\n".$_SESSION['userId'];
				$stmt = $db->prepare("SELECT * FROM leads WHERE isActive=:isActive AND leadManagerId=:leadManagerId AND activate=:activate AND priceCardSent=:priceCardSent AND quotationSent=:quotationSent");
				$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
				$stmt->bindParam("activate", $activate,PDO::PARAM_BOOL);
				$stmt->bindParam("priceCardSent", $priceCardSent,PDO::PARAM_BOOL);
				$stmt->bindParam("quotationSent", $quotationSent,PDO::PARAM_BOOL);
				$stmt->bindParam("leadManagerId", $_SESSION['userId'],PDO::PARAM_INT);
		      	$stmt->execute();
		      	$responseObj->columns=array("Deactivate","Reject","Send Quotation");
		      	// $responseObj->leadArray=$data;
		      	// return $responseObj;
			}
			elseif((in_array("fulfillment", $_SESSION["accessLevel"]))&&($_SESSION['majorRole']=='manager'))
			{
				$stmt = $db->prepare("SELECT * FROM leads WHERE isActive=:isActive AND activate=:activate AND priceCardSent=:priceCardSent AND quotationSent=:quotationSent");
				// $stmt->bindParam("fulfilmentManagerId", $_SESSION['userId'],PDO::PARAM_INT);
		      	$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
				$stmt->bindParam("activate", $activate,PDO::PARAM_BOOL);
				$stmt->bindParam("quotationSent", $quotationSent,PDO::PARAM_BOOL);
				$stmt->bindParam("priceCardSent", $priceCardSent,PDO::PARAM_BOOL);
				$stmt->execute();
				$responseObj->columns=array("Reject");
			}
			elseif((in_array("leads", $_SESSION["accessLevel"]))&&($_SESSION['majorRole']=='operator'))
			{
				$stmt = $db->prepare("SELECT * FROM leads WHERE LeadOperatorId=:LeadOperatorId AND isActive=:isActive AND activate=:activate AND priceCardSent=:priceCardSent AND quotationSent=:quotationSent");
				$stmt->bindParam("LeadOperatorId", $_SESSION['userId'],PDO::PARAM_INT);
				$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
				$stmt->bindParam("quotationSent", $quotationSent,PDO::PARAM_BOOL);
				$stmt->bindParam("activate", $activate,PDO::PARAM_BOOL);
				$stmt->bindParam("priceCardSent", $priceCardSent,PDO::PARAM_BOOL);
				$stmt->execute();
				$responseObj->columns=array("Deactivate","Send Quotation");
			}
			else
			{
				http_response_code($GLOBALS['unauthorized']);
			}
			$data=$stmt->fetchAll(PDO::FETCH_OBJ);
			$leadArray=$this->getLeadArray($data);
			$responseObj->leadArray=$leadArray;
			// $responseObj->yeah="yoo";
			// echo var_dump($data);
			$db = null;
			return $responseObj;	
		}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			$responseObj->status=$e->getMessage();
	      }
	      
     }

    public function getLeadsQuotationSentButNotAccepted()
     {
 		 $responseObj=new stdClass;
     	 try{

			$db = getDB();
			$isActive=1;
			$activate=1;
			$quotationSent=1;
			$renterAcceptQuotation=0;
			if((in_array("leads", $_SESSION["accessLevel"]))&&($_SESSION['majorRole']=='manager'))
			{
				$stmt = $db->prepare("SELECT * FROM leads WHERE isActive=:isActive AND leadManagerId=:leadManagerId AND activate=:activate AND renterAcceptQuotation=:renterAcceptQuotation AND quotationSent=:quotationSent");
				$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
				$stmt->bindParam("activate", $activate,PDO::PARAM_BOOL);
				$stmt->bindParam("renterAcceptQuotation", $renterAcceptQuotation,PDO::PARAM_BOOL);
	          	$stmt->bindParam("quotationSent", $quotationSent,PDO::PARAM_BOOL);
				$stmt->bindParam("leadManagerId", $_SESSION['userId'],PDO::PARAM_INT);
		      	$stmt->execute();
				$responseObj->columns=array("Deactivate","Reject","Renter Accept Quotation");
			}
			else if((in_array("leads", $_SESSION["accessLevel"]))&&($_SESSION['majorRole']=='operator'))
			{
				$stmt = $db->prepare("SELECT * FROM leads WHERE LeadOperatorId=:LeadOperatorId AND isActive=:isActive AND activate=:activate AND priceCardSent=:priceCardSent AND renterAcceptQuotation=:renterAcceptQuotation");
				$stmt->bindParam("LeadOperatorId", $_SESSION['userId'],PDO::PARAM_INT);
				$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
				$stmt->bindParam("activate", $activate,PDO::PARAM_BOOL);
				$stmt->bindParam("renterAcceptQuotation", $renterAcceptQuotation,PDO::PARAM_BOOL);
	          	$stmt->bindParam("priceCardSent", $priceCardSent,PDO::PARAM_BOOL);
				$stmt->execute();
				$responseObj->columns=array("Deactivate","Renter Accept Quotation");
			}
			else
			{
				http_response_code($GLOBALS['unauthorized']);
			}
			$data=$stmt->fetchAll(PDO::FETCH_OBJ);
			$leadArray=$this->getLeadArray($data);
			$db = null;
			$responseObj->leadArray=$leadArray;
			return $responseObj;	
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			$responseObj->status=$e->getMessage();
	      }
	     
     }

     public function getLeadsQuotationAcceptedButSupplierNotConfirmed()
     {
 		 $responseObj=new stdClass;
     	 try{

			$db = getDB();
			$isActive=1;
			$activate=1;
			$supplierConfirmed=0;
			$renterAcceptQuotation=1;
			if((in_array("leads", $_SESSION["accessLevel"]))&&($_SESSION['majorRole']=='manager'))
			{
				$stmt = $db->prepare("SELECT * FROM leads WHERE isActive=:isActive AND leadManagerId=:leadManagerId AND activate=:activate AND renterAcceptQuotation=:renterAcceptQuotation AND supplierConfirmed=:supplierConfirmed");
				$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
				$stmt->bindParam("activate", $activate,PDO::PARAM_BOOL);
				$stmt->bindParam("renterAcceptQuotation", $renterAcceptQuotation,PDO::PARAM_BOOL);
	          	$stmt->bindParam("supplierConfirmed", $supplierConfirmed,PDO::PARAM_BOOL);
				$stmt->bindParam("leadManagerId", $_SESSION['userId'],PDO::PARAM_INT);
		      	$stmt->execute();
				$responseObj->columns=array("Deactivate","Reject");
			}
			else if((in_array("fulfillment", $_SESSION["accessLevel"]))&&($_SESSION['majorRole']=='manager'))
			{
				$stmt = $db->prepare("SELECT * FROM leads WHERE isActive=:isActive AND activate=:activate AND renterAcceptQuotation=:renterAcceptQuotation AND supplierConfirmed=:supplierConfirmed");
				$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
				$stmt->bindParam("activate", $activate,PDO::PARAM_BOOL);
				$stmt->bindParam("renterAcceptQuotation", $renterAcceptQuotation,PDO::PARAM_BOOL);
	          	$stmt->bindParam("supplierConfirmed", $supplierConfirmed,PDO::PARAM_BOOL);
				$stmt->execute();
				$responseObj->columns=array("Deactivate","Reject","Confirm Supplier");
			}
			else
			{
				http_response_code($GLOBALS['unauthorized']);
			}
			$data=$stmt->fetchAll(PDO::FETCH_OBJ);
			$leadArray=$this->getLeadArray($data);
			$db = null;
			$responseObj->leadArray=$leadArray;
			return $responseObj;	
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			$responseObj->status=$e->getMessage();
	      }
	     
     }
     
    public function getLeadsSupplierConfirmedButPaperworkNotDone()
     {
 		 $responseObj=new stdClass;
     	 try{

			$db = getDB();
			$isActive=1;
			$activate=1;
			$paperWorkAndAdvance=0;
			$supplierConfirmed=1;
			if((in_array("fulfillment", $_SESSION["accessLevel"]))&&($_SESSION['majorRole']=='manager')||(in_array("leads", $_SESSION["accessLevel"]))&&($_SESSION['majorRole']=='manager'))
			{
				$stmt = $db->prepare("SELECT * FROM leads WHERE isActive=:isActive AND activate=:activate AND supplierConfirmed=:supplierConfirmed AND paperWorkAndAdvance=:paperWorkAndAdvance");
				// $stmt->bindParam("fulfilmentManagerId", $_SESSION['userId'],PDO::PARAM_INT);
	          	$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
				$stmt->bindParam("activate", $activate,PDO::PARAM_BOOL);
				$stmt->bindParam("supplierConfirmed", $supplierConfirmed,PDO::PARAM_BOOL);
				$stmt->bindParam("paperWorkAndAdvance", $paperWorkAndAdvance,PDO::PARAM_BOOL);
				$stmt->execute();
				$responseObj->columns=array("Reject","Paperwork And Advance Done");
			}
			else
			{
				http_response_code($GLOBALS['unauthorized']);
			}
			$data=$stmt->fetchAll(PDO::FETCH_OBJ);
			// echo var_dump($data);
			$leadArray=$this->getLeadArray($data);
			$db = null;
			$responseObj->leadArray=$leadArray;
			return $responseObj;
			// echo $responseObj;
			// echo var_dump($responseObj);
	        
     	}
	      catch(PDOException $e) {
	      			http_response_code($GLOBALS['connection_error']);
          			echo $e->getMessage();
	      }
	      
     }
    
    public function getLeadsPaperworkDoneButNotConvertedToOrder()
     {
 		 $responseObj=new stdClass;
     	 try{

			$db = getDB();
			$isActive=1;
			$activate=1;
			$paperWorkAndAdvance=1;
			$accept=0;
			if((in_array("fulfillment", $_SESSION["accessLevel"]))&&($_SESSION['majorRole']=='manager'))
			{
				$stmt = $db->prepare("SELECT * FROM leads WHERE isActive=:isActive AND activate=:activate AND accept=:accept AND paperWorkAndAdvance=:paperWorkAndAdvance");
				// $stmt->bindParam("fulfilmentManagerId", $_SESSION['userId'],PDO::PARAM_INT);
	          	$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
				$stmt->bindParam("activate", $activate,PDO::PARAM_BOOL);
				$stmt->bindParam("paperWorkAndAdvance", $paperWorkAndAdvance,PDO::PARAM_BOOL);
				$stmt->bindParam("accept", $accept,PDO::PARAM_BOOL);
				$stmt->execute();
				$responseObj->columns=array("Convert To Order","Reject");
			}
			else
			{
				http_response_code($GLOBALS['unauthorized']);
			}
			$data=$stmt->fetchAll(PDO::FETCH_OBJ);
			$leadArray=$this->getLeadArray($data);
			$db = null;
			$responseObj->leadArray=$leadArray;
			return $responseObj;
			
     	}
	      catch(PDOException $e) {
	      			http_response_code($GLOBALS['connection_error']);
          			echo $e->getMessage();
	      }
	      
     }
    
    public function getLeadsAcceptedButNotMovedToOrder()
     {
 		 $responseObj=new stdClass;
     	 try{

	          $db = getDB();
	          $isActive=1;
	          $activate=1;
	          $movedToOrders=0;
	          $accept=1;

	          if((in_array("leads", $_SESSION["accessLevel"]))&&($_SESSION['majorRole']=='manager'))
			{
				$stmt = $db->prepare("SELECT * FROM leads WHERE isActive=:isActive AND leadManagerId=:leadManagerId AND activate=:activate AND accept=:accept AND movedToOrders=:movedToOrders");
				$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
				$stmt->bindParam("activate", $activate,PDO::PARAM_BOOL);
				$stmt->bindParam("accept", $accept,PDO::PARAM_BOOL);
				$stmt->bindParam("movedToOrders", $movedToOrders,PDO::PARAM_BOOL);
				$stmt->bindParam("leadManagerId", $_SESSION['userId'],PDO::PARAM_INT);
		      	$stmt->execute();
				$responseObj->columns=array("Move To Orders","Reject");
			}
			else
			{
				http_response_code($GLOBALS['unauthorized']);
			}
			$data=$stmt->fetchAll(PDO::FETCH_OBJ);
			$leadArray=$this->getLeadArray($data);
			$db = null;
			$responseObj->leadArray=$leadArray;
			return $responseObj;	

     	}
	      catch(PDOException $e) {
	      			http_response_code($GLOBALS['connection_error']);
          			echo $e->getMessage();
	      }
	      
     }

    public function getAcceptedLeads()
     {
 		 $responseObj=new stdClass;
     	 try{

	          $db = getDB();
	          $isActive=1;
	          $accept=1;
	          $stmt = $db->prepare("SELECT * FROM leads WHERE isActive=:isActive AND accept=:accept");
	          $stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
	          $stmt->bindParam("accept", $accept,PDO::PARAM_BOOL);
	          $stmt->execute();
	          $data=$stmt->fetchAll(PDO::FETCH_OBJ);
	          $responseArr=array();
	          for($i=0;$i<count($data);$i++)
	          {
	          	$x=$data[i];
	          	if(in_array($x->leadType, $_SESSION['accessLevel']))
	          	{
	          		array_push($responseArr,$x->leadType);
	          	}
	          }
	          $db = null;
	          if($responseArr)
	          {
	                http_response_code($GLOBALS['success']);
	                $responseObj->data=$responseArr;
					$responseObj->status="Success";
	          }
	          else
	          {
	                http_response_code($GLOBALS['noContent']);
          			$responseObj->data=$data;
					$responseObj->status="Nothing Found";
	          } 	
     	}
	      catch(PDOException $e) {
	      			http_response_code($GLOBALS['connection_error']);
          			$responseObj->status=$e->getMessage();
	      }
	      return $responseObj;
     }

    public function getMyLeads()
     {
 		 $responseObj=new stdClass;
     	 try{

	          $db = getDB();
	          $isActive=1;
	          if($_SESSION["majorRole"]=="operator")
	          {
	          	$stmt = $db->prepare("SELECT * FROM leads WHERE isActive=:isActive AND LeadOperatorId=:LeadOperatorId");
	          	$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
	          	$stmt->bindParam("LeadOperatorId", $_SESSION['userId'],PDO::PARAM_STR);
	          	$stmt->execute();
	          }
	          else if($_SESSION["majorRole"]=="manager")
	          {
	          	$stmt = $db->prepare("SELECT * FROM leads WHERE isActive=:isActive AND leadManagerId=:leadManagerId");
	          	$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
	          	$stmt->bindParam("leadManagerId", $_SESSION['userId'],PDO::PARAM_STR);
	          	$stmt->execute();
	          }
	          else
	          {
	          	http_response_code($GLOBALS['forbidden']);
           		return false;
	          }
	          $data=$stmt->fetchAll(PDO::FETCH_OBJ);
	          $responseArr=array();
	          for($i=0;$i<count($data);$i++)
	          {
	          	$x=$data[i];
	          	if(in_array($x->leadType, $_SESSION['accessLevel']))
	          	{
	          		array_push($responseArr,$x->leadType);
	          	}
	          }
	          $db = null;
	          if($responseArr)
	          {
	                http_response_code($GLOBALS['success']);
	                $responseObj->data=$responseArr;
					$responseObj->status="Success";
	          }
	          else
	          {
	                http_response_code($GLOBALS['noContent']);
          			$responseObj->data=$data;
					$responseObj->status="Nothing Found";
	          } 	
     	}
	      catch(PDOException $e) {
	      			http_response_code($GLOBALS['connection_error']);
          			$responseObj->status=$e->getMessage();
	      }
	      return $responseObj;
     }

    public function getLeadsByCustomerID($customerID)
     {
     	$responseObj=new stdClass;
     	try{
	          $db = getDB();
	          $stmt = $db->prepare("SELECT * FROM leads WHERE customerID=:customerID");
	          $stmt->bindParam("customerID", $customerID,PDO::PARAM_INT);
	          $stmt->execute();
	          $data=$stmt->fetchAll(PDO::FETCH_OBJ);
	          $db = null;
	          if($data)
	          {
	                http_response_code($GLOBALS['success']);
	                $responseObj->data=$data;
					$responseObj->status="Success";
	          }
	          else
	          {
	                http_response_code($GLOBALS['noContent']);
          			$responseObj->data=$data;
					$responseObj->status="Nothing Found";
	          } 	
     	}
	      catch(PDOException $e) {
	      			http_response_code($GLOBALS['connection_error']);
          			$responseObj->status=$e->getMessage();
	      }
	      return $responseObj;
     }

    public function getMyInfo()
     {
 	    $responseObj=new stdClass;
     	try{
			$db = getDB();
			$stmt = $db->prepare("SELECT * FROM staff_login WHERE userId=:id");
			$stmt->bindParam("id", $_SESSION['userId'],PDO::PARAM_INT);
			$stmt->execute();
			$data=$stmt->fetch(PDO::FETCH_OBJ);
			$db = null;
			// echo var_dump($this->getMyTeam());
			// $data;
			// echo $data;
			// return true;
			if($data)
	        {
                $responseObj->majorRole=$data->majorRole;
				if($data->majorRole=="admin")
				{
					$responseObj->minorRole=array("operator","manager");
				}
				else if($data->majorRole=="manager")
				{
					$responseObj->minorRole=array("operator");
				}
				else
				{
					$responseObj->minorRole=array();
				}
				$responseObj->name=$data->name;
				if(in_array("leads",$_SESSION['accessLevel']))
				{		
					$responseObj->role="Leads";
				}
				else if(in_array("fulfillment",$_SESSION['accessLevel']))
				{		
					$responseObj->role="Fulfillment";
				}
				else
				{
					// echo "hi1";
					return false;
				}
				$responseObj->newUser=1;		
				if($_SESSION["majorRole"]=="manager")
				{
					$responseObj->role=$responseObj->role." Manager";
				}
				else if($_SESSION["majorRole"]=="operator")
				{
					$responseObj->newUser=0;
					$responseObj->role=$responseObj->role." Operator";
				}
				else 
					$responseObj->role="Admin";
				$responseObj->phone=$data->phone;
				if($responseObj->newUser)
				{
					$responseObj->team=$this->getMyTeam();
				}
				$responseObj->email=$data->email;
				// http_response_code($GLOBALS['success']);
				// echo var_dump($responseObj);
                return $responseObj;
	        }
	        else
	        {
                http_response_code($GLOBALS['noContent']);
      			echo "Record Not Found";
      			return false;
	        } 	
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
	    
     }

    public function getTabs()
     {
 	    $responseObj=new stdClass;
     	try{
     		if(($_SESSION['majorRole']=='admin'))
			{
 				$tabsArray=array("New","Approved By Renter","Price Card Sent","Quotation Sent","Renter Accept Quotation","Supplier Confirmed","Paperwork And Advance Done","Converted to Order");
 			}

			else if((in_array("leads", $_SESSION["accessLevel"]))&&($_SESSION['majorRole']=='manager'))
			{
				$tabsArray=array("New","Approved By Renter","Price Card Sent","Quotation Sent","Renter Accept Quotation","Converted to Order");
			}
			else if((in_array("leads", $_SESSION["accessLevel"]))&&($_SESSION['majorRole']=='operator'))
			{
				$tabsArray=array("New","Approved By Renter","Price Card Sent","Quotation Sent");

			}
			else if((in_array("fulfillment", $_SESSION["accessLevel"]))&&(($_SESSION['majorRole']=='manager')))
			{
				$tabsArray=array("Approved By Renter","Renter Accept Quotation","Price Card Sent","Supplier Confirmed","Paperwork And Advance Done","Converted to Order");
			}
			else return false;
			$responseObj->tabs=$tabsArray;
			if((in_array("leads", $_SESSION["accessLevel"]))&&(($_SESSION['majorRole']=='operator')||($_SESSION['majorRole']=='manager')))$responseObj->addLead=1;
			else
				$responseObj->addLead=0;
			$id=$this->getMyInfo();
			$responseObj->name=$id->name;
            return $responseObj;
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();	
	    }
     }

	public function getTypesOfWork()
     {
 	    $responseObj=new stdClass;
     	try{
			if((in_array("leads", $_SESSION["accessLevel"]))&&(($_SESSION['majorRole']=='manager')||($_SESSION['majorRole']=='operator')));
			else if((in_array("fulfillment", $_SESSION["accessLevel"]))&&(($_SESSION['majorRole']=='manager')));
			else return false;
			
			$db = getDB();
			$stmt = $db->prepare("SELECT name FROM type_of_work");
			$stmt->execute();
			$typeofworkArray=$stmt->fetchAll(PDO::FETCH_COLUMN);
			$db = null;

			
            return $typeofworkArray;
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();	
	    }
     }

    public function getMakes()
     {
 	    $responseObj=new stdClass;
     	try{
			if((in_array("leads", $_SESSION["accessLevel"]))&&(($_SESSION['majorRole']=='manager')||($_SESSION['majorRole']=='operator')));
			else if((in_array("fulfillment", $_SESSION["accessLevel"]))&&(($_SESSION['majorRole']=='manager')));
			else return false;
			
			$db = getDB();
			$stmt = $db->prepare("SELECT name FROM make");
			$stmt->execute();
			$makeArray=$stmt->fetchAll(PDO::FETCH_COLUMN);
			$db = null;

            return $makeArray;
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();	
	    }
     }

    public function getModels()
     {
 	    $responseObj=new stdClass;
     	try{
			if((in_array("leads", $_SESSION["accessLevel"]))&&(($_SESSION['majorRole']=='manager')||($_SESSION['majorRole']=='operator')));
			else if((in_array("fulfillment", $_SESSION["accessLevel"]))&&(($_SESSION['majorRole']=='manager')));
			else return false;
			
			$db = getDB();
			$stmt = $db->prepare("SELECT Model FROM model");
			$stmt->execute();
			$modelArray=$stmt->fetchAll(PDO::FETCH_COLUMN);
			$db = null;

			return $modelArray;
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();	
	    }
     }

    public function getEquipmentTypes()
     {
 	    $responseObj=new stdClass;
     	try{
			if((in_array("leads", $_SESSION["accessLevel"]))&&(($_SESSION['majorRole']=='manager')||($_SESSION['majorRole']=='operator')));
			else if((in_array("fulfillment", $_SESSION["accessLevel"]))&&(($_SESSION['majorRole']=='manager')));
			else return false;
			
			$db = getDB();
			$stmt = $db->prepare("SELECT name FROM equipment_type");
			$stmt->execute();
			$eqTypeArray=$stmt->fetchAll(PDO::FETCH_COLUMN);
			$db = null;

            return $eqTypeArray;
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();	
	    }
     }

    public function getLeadArray($data)
     {
		try{

          	$leadArray=array();
			for($i=0;$i<count($data);$i++)
			{
				$leadObj=new stdClass;
				$leadObj->customerId=$data[$i]->customerId;
				$leadObj->name=$this->getNameFromCustomerId($data[$i]->customerId);
				$leadObj->phone=$this->getPhoneFromCustomerId($data[$i]->customerId);
				$leadObj->typeOfWork=$this->getNameFromtypeOfWorkId($data[$i]->typeOfWork);
				$leadObj->leadSource=$this->getNameFromSourceId($data[$i]->sources);
				$date= new DateTime($data[$i]->leadDate);
				$leadObj->leadDate=$date->format('m/d/Y h:i A');
				$leadObj->leadPriority=$data[$i]->leadPriority;
				$leadObj->activate=$data[$i]->activate;
				$leadObj->reject=$data[$i]->reject;
				$leadObj->accept=$data[$i]->accept;
				$leadObj->priceCardSent=$data[$i]->priceCardSent;
				$leadObj->quotationSent=$data[$i]->quotationSent;
				$leadObj->renterAcceptQuotation=$data[$i]->renterAcceptQuotation;
				$leadObj->paperWorkAndAdvance=$data[$i]->paperWorkAndAdvance;
				$leadObj->supplierConfirmed=$data[$i]->supplierConfirmed;
				$leadObj->movedToOrders=$data[$i]->movedToOrders;
				$leadObj->leadId=$data[$i]->id;
				$equipments=$this->getEquipmentsFromLeadId($data[$i]->id);
				$equipmentArray=array();
				foreach ($equipments as $equipment) 
				{
					$equipObj=new stdClass;
					$equipObj->equipId=$equipment->id;
					$equipObj->equipName=$this->getNameFromEquipmentId($equipment->equipmentId);
					$date = new DateTime($equipment->expectedStartDate);
    				$equipObj->startDate=$date->format('m/d/Y h:i A') ;
					$equipObj->make=$this->getNameFromMakeId($equipment->makeId);
					$equipObj->model=$this->getNameFromModelId($equipment->equipmentId);
					$equipObj->operationHoursPerDay=$equipment->operationalHoursInADay;
					$equipObj->operationDaysPerMonth=$equipment->operationalDaysInAMonth;
					if((in_array("fulfillment", $_SESSION["accessLevel"]))&&(($_SESSION['majorRole']=='manager')))
					{
						$equipObj->fulfilmentOperator=$this->getNameAndPhoneOfFulfilmentOperator($equipment->fulfilmentOperator);
					}
					
					$equipObj->operationHoursPerMonth=$equipment->operationalHoursInAMonth;
					$equipObj->vehicleDocuments=$equipment->vehicleDocuments;
					$equipObj->confirmed=$equipment->confirmed;
					$equipObj->operatorLicense=$equipment->operatorLicense;
					$equipObj->capacity=$equipment->capacity;
					$equipObj->accomodation=$equipment->accommodation;
					$equipObj->quantity=$equipment->quantity;
					$equipObj->food=$equipment->food;
					$equipObj->district=$equipment->district;
					$equipObj->year=$equipment->year;
					$equipObj->shiftTypeDay=$equipment->shiftTypeDay;
					$equipObj->shiftTypeNight=$equipment->shiftTypeNight;
					$equipObj->jobLocation=$equipment->location;
					$equipObj->stage=$equipment->projectStage;
					array_push($equipmentArray, $equipObj);
				}
				$leadObj->equipments=$equipmentArray;
				array_push($leadArray, $leadObj);
			}
			return $leadArray;
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
	 }

	public function getNameAndPhoneOfFulfilmentOperator($id)
     {
		try{

			$db = getDB();
			$accessLevels=$this->getAccessLevels($id);
			if(in_array("fulfillment", $accessLevels))
			{  
				$isActive=1;
				$majorRole="operator";
				$stmt = $db->prepare("SELECT name,phone FROM staff_login WHERE userId=:id AND majorRole=:majorRole AND isActive=:isActive");
				$stmt->bindParam("id", $id,PDO::PARAM_INT);
				$stmt->bindParam("majorRole", $majorRole,PDO::PARAM_STR);
				$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
				$stmt->execute();
				$data=$stmt->fetch(PDO::FETCH_OBJ);
				if($data)
				{
					return array($data->name,$data->phone);
				}
				else
				{
					return array();
				}
			}
			else
			{
				return false;
			}
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
	 }


    public function getNameFromtypeOfWorkId($id)
     {
		try{

	          $db = getDB();
	          $stmt = $db->prepare("SELECT name FROM type_of_work WHERE id=:id");
	          $stmt->bindParam("id", $id,PDO::PARAM_STR);
	          $stmt->execute();
	          $data=$stmt->fetch(PDO::FETCH_OBJ);
	          return $data->name; 	
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
	 }

	public function getNameFromEquipmentId($id)
	 {
		try{

	          $db = getDB();
	          $stmt = $db->prepare("SELECT name FROM equipment_type WHERE id=:id");
	          $stmt->bindParam("id", $id,PDO::PARAM_STR);
	          $stmt->execute();
	          $data=$stmt->fetch(PDO::FETCH_OBJ);
	          return $data->name; 	
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
	 }

	public function getEquipmentsFromLeadId($id)
	 {
		try{

	          $db = getDB();
	          $stmt = $db->prepare("SELECT * FROM lead_equipment WHERE leadId=:id");
	          $stmt->bindParam("id", $id,PDO::PARAM_STR);
	          $stmt->execute();
	          $data=$stmt->fetchAll(PDO::FETCH_OBJ);
	          return $data; 	
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
	 }

	public function getNameFromSourceId($id)
	 {
		try{

	          $db = getDB();
	          $stmt = $db->prepare("SELECT name FROM sources WHERE id=:id");
	          $stmt->bindParam("id", $id,PDO::PARAM_STR);
	          $stmt->execute();
	          $data=$stmt->fetch(PDO::FETCH_OBJ);
	          return $data->name; 	
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
	 }
	
	public function getNameFromCustomerId($id)
	 {
		try{

	          $db = getDB();
	          $stmt = $db->prepare("SELECT renterName FROM renters WHERE id=:id");
	          $stmt->bindParam("id", $id,PDO::PARAM_STR);
	          $stmt->execute();
	          $data=$stmt->fetch(PDO::FETCH_OBJ);
	          return $data->renterName; 	
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
	 }
	
	public function getPhoneFromCustomerId($id)
	 {
		try{

	          $db = getDB();
	          $stmt = $db->prepare("SELECT phone FROM renters WHERE id=:id");
	          $stmt->bindParam("id", $id,PDO::PARAM_STR);
	          $stmt->execute();
	          $data=$stmt->fetch(PDO::FETCH_OBJ);
	          return $data->phone; 	
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
	 }

	public function getNameFromModelId($id)
	 {
		try{

	          $db = getDB();
	          $stmt = $db->prepare("SELECT Model FROM model WHERE id=:id");
	          $stmt->bindParam("id", $id,PDO::PARAM_STR);
	          $stmt->execute();
	          $data=$stmt->fetch(PDO::FETCH_OBJ);
	          return $data->Model; 	
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
	 }

	public function getNameFromMakeId($id)
	 {
		try{

	          $db = getDB();
	          $stmt = $db->prepare("SELECT name FROM make WHERE id=:id");
	          $stmt->bindParam("id", $id,PDO::PARAM_STR);
	          $stmt->execute();
	          $data=$stmt->fetch(PDO::FETCH_OBJ);
	          return $data->name; 	
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
	 }

    public function getSources()
     {
 	    $responseObj=new stdClass;
     	try{
			if((in_array("leads", $_SESSION["accessLevel"]))&&(($_SESSION['majorRole']=='manager')||($_SESSION['majorRole']=='operator')));
			else if((in_array("fulfillment", $_SESSION["accessLevel"]))&&(($_SESSION['majorRole']=='manager')));
			else return false;
			$db = getDB();
			$stmt = $db->prepare("SELECT name FROM sources");
			$stmt->execute();
			$sourceArray=$stmt->fetchAll(PDO::FETCH_COLUMN);
			$db = null;
            return $sourceArray;
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();	
	    }
     }

    public function getAccessLevels($id)
     {
 	    $responseObj=new stdClass;
     	try{
			$db = getDB();
			$stmt = $db->prepare("SELECT access FROM access_level INNER JOIN access ON access.accessLevel=access_level.id AND access.userId=:id");
            $stmt->bindParam("id", $id,PDO::PARAM_STR) ;
            $stmt->execute();
            $count=$stmt->rowCount();
            $access=$stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            $accessLevelArray=array();
            for($i=0;$i<count($access);$i++)
            {
                 $accessData=($access[$i]);
                 array_push($accessLevelArray,$accessData->access);
                 
            }
            return $accessLevelArray; 	
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();	
	    }
     }
    
    public function getMyManager()
     {
     	try{
	      $db = getDB();
	      $isActive=1;
          $stmt = $db->prepare("SELECT managerId FROM staff_login WHERE userId=:id AND isActive=:isActive");
          $stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
	      $stmt->bindParam("id", $_SESSION['userId'],PDO::PARAM_INT);
          $stmt->execute();
          $data=$stmt->fetch(PDO::FETCH_OBJ);
          $db = null;
          if($data)
      	  {
          	return $data->managerId;
          }
          else
          {
          	return false;
          }
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			$responseObj->status=$e->getMessage();
	    }
     }
    
    public function getMyTeam()
     {
     	$responseObj=new stdClass;
     	try{
     		  $db = getDB();
     		  $isActive=1;
              if($_SESSION["majorRole"]=="admin")
              {
              	$stmt = $db->prepare("SELECT * FROM staff_login WHERE adminId=:adminId AND isActive=:isActive");
              	$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
	            $stmt->bindParam("adminId", $_SESSION['userId'],PDO::PARAM_INT);
	          }
	          else if($_SESSION["majorRole"]=="manager")
              {
              	$stmt = $db->prepare("SELECT * FROM staff_login WHERE managerId=:managerId AND isActive=:isActive");
              	$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
	            $stmt->bindParam("managerId", $_SESSION['userId'],PDO::PARAM_INT);
	          }
	          else
	          {
	          	return false;
	          }
	          $stmt->execute();
	          $data=$stmt->fetchAll(PDO::FETCH_OBJ);
	          $db = null;
	          return $data;	
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
	    
     }
    
    public function getEquipmentsByFulfilmentOperator()
     {
     	$responseObj=new stdClass;
     	try{

			$db = getDB();
			$isActive=1;
			// $stmt = $db->prepare("SELECT * FROM lead_equipment WHERE fulfilmentOperatorId=:fulfilmentOperatorId");
			// $stmt->bindParam("fulfilmentOperatorId", $_SESSION['userId'],PDO::PARAM_INT);
			$stmt = $db->prepare("SELECT * FROM lead_equipment");
			$stmt->execute();
			$data=$stmt->fetchAll(PDO::FETCH_OBJ);
			$db = null;
			$leadEquipArray=array();
			if($data)
        	{
        		$responseObj=new stdClass;
        		for($i=0;$i<count($data);$i++)
        		{
	        		$responseObj=new stdClass;
					$responseObj->model=$this->getNameFromModelId($data[$i]->modelId);
	          		$responseObj->equipName=$this->getNameFromEquipmentId($data[$i]->equipmentId);
	          		$responseObj->make=$this->getNameFromMakeId($data[$i]->makeId);
	          		$responseObj->jobLocation=$data[$i]->location;
	          		$responseObj->district=$data[$i]->district;
	          		$responseObj->equipId=$data[$i]->id;
	          		$responseObj->shiftTypeDay=$data[$i]->shiftTypeDay;
	          		$responseObj->stage=$data[$i]->projectStage;
	          		if($data[$i]->supplierPhone1)$responseObj->supply=1;
					else $responseObj->supply=0;
					if($data[$i]->priceTag)$responseObj->price=1;
					else $responseObj->price=0;
	          		$responseObj->shiftTypeNight=$data[$i]->shiftTypeNight;
	          		$responseObj->startDate=$data[$i]->expectedStartDate;
	          		$responseObj->confirmed=$data[$i]->confirmed;
	          		$responseObj->operationalHoursInADay=$data[$i]->operationalHoursInADay;
	          		$responseObj->operationalDaysInAMonth=$data[$i]->operationalDaysInAMonth;
	          		$responseObj->operationalHoursInAMonth=$data[$i]->operationalHoursInAMonth;
	          		$responseObj->vehicleDocuments=$data[$i]->vehicleDocuments;
	          		$responseObj->operatorLicense=$data[$i]->operatorLicense;
	          		$responseObj->capacity=$data[$i]->capacity;
	          		$responseObj->year=$data[$i]->year;
	          		$responseObj->operatorAllowance=$data[$i]->operatorAllowance;
	          		array_push($leadEquipArray, $responseObj);
	        	}
	        	return $leadEquipArray;
	        }
	        else
	        {
	        	return false;
                http_response_code($GLOBALS['noContent']);
      			
	        } 	

     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
	    return $responseObj;
     }
    
    public function getLeadsByLeadManagerId($leadManagerId)
     {
     	$responseObj=new stdClass;
     	try{

	          $db = getDB();
	          $isActive=1;
	          $stmt = $db->prepare("SELECT * FROM leads WHERE leadManagerId=:leadManagerId");
	          $stmt->bindParam("leadManagerId", $leadManagerId,PDO::PARAM_INT);
	          $stmt->execute();
	          $data=$stmt->fetchAll(PDO::FETCH_OBJ);
	          $db = null;
	          if($data)
	        {
                http_response_code($GLOBALS['success']);
                $responseObj->data=$data;
				$responseObj->status="Success";
	        }
	        else
	        {
                http_response_code($GLOBALS['noContent']);
      			$responseObj->data=$data;
				$responseObj->status="Nothing Found";
	        } 	
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			$responseObj->status=$e->getMessage();
	    }
	    return $responseObj;
     }

    public function leadDelete($id)
     {
     	try{

	          $db = getDB();
	          $stmt = $db->prepare("SELECT * FROM leads WHERE id=:id and isActive=:isActive");
	          $isActive=1;
	          $stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);  
	          $stmt->bindParam("id", $id,PDO::PARAM_INT);  
	          $stmt->execute();
	          $data=$stmt->fetch(PDO::FETCH_OBJ);
	          if($data)
	          {
	          		$isActive=0;
	          		$deletedBy=$_SESSION['userId'];
	          		$time= date('Y-m-d H:i:s',time());
	                $st=$db->prepare("UPDATE leads SET isActive=:isActive,deletedBy=:deletedBy,deletedOn=:deletedOn WHERE id=:id");
	                $st->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
	                $st->bindParam("deletedOn",$time,PDO::PARAM_STR);
                    $st->bindParam("deletedBy",$deletedBy,PDO::PARAM_INT);
	                $st->bindParam("id", $id,PDO::PARAM_INT);  
	          		$st->execute();
	          		$x=$this->leadEquipDelete($id);
	          		$db=null;
	          		return true;
	          }
	          else
	          {
	          		$db=null;
	               return false;
	          } 	
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
     }

    public function leadActivate($id)
     {
     	try{

	          $db = getDB();
	          $isActive=1;
	          $stmt = $db->prepare("SELECT * FROM leads WHERE id=:id AND isActive=:isActive");
	          $stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
	          $stmt->bindParam("id", $id,PDO::PARAM_INT);  
	          $stmt->execute();
	          $data=$stmt->fetch(PDO::FETCH_OBJ);
	          if($data)
	          {
	          		$activate=1;
	          		$lastUpdatedBy=$_SESSION['userId'];
	          		$time= date('Y-m-d H:i:s',time());
	                $st=$db->prepare("UPDATE leads SET activate=:activate,lastUpdatedOn=:lastUpdatedOn,lastUpdatedBy=:lastUpdatedBy WHERE id=:id");
	                $st->bindParam("activate", $activate,PDO::PARAM_BOOL);  
	          		$st->bindParam("lastUpdatedOn",$time,PDO::PARAM_STR);
                    $st->bindParam("lastUpdatedBy",$lastUpdatedBy,PDO::PARAM_INT);
	                $st->bindParam("id", $id,PDO::PARAM_INT);  
	          		$st->execute();
	          		$db=null;
	          		return true;
	          }
	          else
	          {
	          		$db=null;
	               return false;
	          } 	
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
 	 }

 	public function leadDeactivate($id)
     {
     	try{

	          $db = getDB();
	          $isActive=1;
	          $stmt = $db->prepare("SELECT * FROM leads WHERE id=:id AND isActive=:isActive");
	          $stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
	          $stmt->bindParam("id", $id,PDO::PARAM_INT);  
	          $stmt->execute();
	          $data=$stmt->fetch(PDO::FETCH_OBJ);
	          if($data)
	          {
	          		$activate=0;
	          		$renterAcceptQuotation=0;
					$supplierConfirmed=0;
					$priceCardSent=0;
					$movedToOrders=0;
					$quotationSent=0;
					$paperWorkAndAdvance=0;
					$lastUpdatedBy=$_SESSION['userId'];
	          		$time= date('Y-m-d H:i:s',time());
	                $st=$db->prepare("UPDATE leads SET quotationSent=:quotationSent,renterAcceptQuotation=:renterAcceptQuotation,supplierConfirmed=:supplierConfirmed,priceCardSent=:priceCardSent,movedToOrders=:movedToOrders,activate=:activate,paperWorkAndAdvance=:paperWorkAndAdvance,lastUpdatedOn=:lastUpdatedOn,lastUpdatedBy=:lastUpdatedBy WHERE id=:id");
	                $st->bindParam("quotationSent", $quotationSent,PDO::PARAM_BOOL) ;
		    		$st->bindParam("renterAcceptQuotation", $renterAcceptQuotation,PDO::PARAM_BOOL) ;
		    		$st->bindParam("supplierConfirmed", $supplierConfirmed,PDO::PARAM_BOOL) ;
		    		$st->bindParam("paperWorkAndAdvance", $paperWorkAndAdvance,PDO::PARAM_BOOL) ;
		    		$st->bindParam("priceCardSent", $priceCardSent,PDO::PARAM_BOOL) ;
		    		$st->bindParam("movedToOrders", $movedToOrders,PDO::PARAM_BOOL) ;
	    			$st->bindParam("activate", $activate,PDO::PARAM_BOOL);  
	          		$st->bindParam("lastUpdatedOn",$time,PDO::PARAM_STR);
                    $st->bindParam("lastUpdatedBy",$lastUpdatedBy,PDO::PARAM_INT);
	                $st->bindParam("id", $id,PDO::PARAM_INT);  
	          		$st->execute();
	          		$db=null;
	          		return true;
	          }
	          else
	          {
	          		$db=null;
	               return false;
	          } 	
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
 	 }

    public function leadAccept($id)
     {
     	try{

	          $db = getDB();
	          $isActive=1;
	          $stmt = $db->prepare("SELECT * FROM leads WHERE id=:id AND isActive=:isActive");
	          $stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
	          $stmt->bindParam("id", $id,PDO::PARAM_INT);  
	          $stmt->execute();
	          $data=$stmt->fetch(PDO::FETCH_OBJ);
	          if($data)
	          {
	          		$accept=1;
	          		$lastUpdatedBy=$_SESSION['userId'];
	          		$time= date('Y-m-d H:i:s',time());
	                $st=$db->prepare("UPDATE leads SET accept=:accept,lastUpdatedOn=:lastUpdatedOn,lastUpdatedBy=:lastUpdatedBy WHERE id=:id");
	                $st->bindParam("accept", $accept,PDO::PARAM_BOOL);  
	          		$st->bindParam("lastUpdatedOn",$time,PDO::PARAM_STR);
                    $st->bindParam("lastUpdatedBy",$lastUpdatedBy,PDO::PARAM_INT);
	                $st->bindParam("id", $id,PDO::PARAM_INT);  
	          		$st->execute();
	          		$db=null;
	          		return true;
	          }
	          else
	          {
	          		$db=null;
	               return false;
	          } 	
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
     }

    public function leadReject($id)
     {
     	try{

	          $db = getDB();
	          $isActive=1;
	          $stmt = $db->prepare("SELECT * FROM leads WHERE id=:id AND isActive=:isActive");
	          $stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
	          $stmt->bindParam("id", $id,PDO::PARAM_INT);  
	          $stmt->execute();
	          $data=$stmt->fetch(PDO::FETCH_OBJ);
	          if($data)
	          {
	          		$reject=1;
	          		$lastUpdatedBy=$_SESSION['userId'];
	          		$time= date('Y-m-d H:i:s',time());
	                $st=$db->prepare("UPDATE leads SET accept=:accept,reject=:reject,leadStatus=:leadStatus,lastUpdatedOn=:lastUpdatedOn,lastUpdatedBy=:lastUpdatedBy WHERE id=:id");
	                $st->bindParam("reject", $reject,PDO::PARAM_BOOL);  
	          		$st->bindParam("lastUpdatedOn",$time,PDO::PARAM_STR);
                    $st->bindParam("lastUpdatedBy",$lastUpdatedBy,PDO::PARAM_INT);
	                $st->bindParam("id", $id,PDO::PARAM_INT);  
	          		$st->execute();
	          		$db=null;
	          		return true;
	          }
	          else
	          {
	          		$db=null;
	               return false;
	          } 	
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
     }
     
    public function leadEquipAdd($shiftTypeDay,$shiftTypeNight,$equipmentId,$leadId,$makeId,$location,$modelId,$district,$expectedStartDate,$operatorAllowance,$year,$food,$accommodation,$operationalHoursInADay,$operationalDaysInAMonth,$vehicleDocuments,$operatorLicense,$capacity,$operationalHoursInAMonth,$projectStage,$quantity)
		{
			try{
			// echo "hi";
				$db = getDB();
			    $stmt = $db->prepare("INSERT INTO lead_equipment(shiftTypeDay,shiftTypeNight,equipmentId,leadId,makeId,location,modelId,district,expectedStartDate,operatorAllowance,year,food,accommodation,operationalHoursInADay,operationalDaysInAMonth,vehicleDocuments,operatorLicense,capacity,operationalHoursInAMonth,projectStage,quantity) VALUES (:shiftTypeDay,:shiftTypeNight,:equipmentId,:leadId,:makeId,:location,:modelId,:district,:expectedStartDate,:operatorAllowance,:year,:food,:accommodation,:operationalHoursInADay,:operationalDaysInAMonth,:vehicleDocuments,:operatorLicense,:capacity,:operationalHoursInAMonth,:projectStage,:quantity)");  
			    

				$st=$db->prepare("SELECT id from equipment_type WHERE name=:name");
				$st->bindParam("name", $equipmentId,PDO::PARAM_STR);
				$st->execute();
				$data=$st->fetch(PDO::FETCH_OBJ);
				$equipmentTypeId=$data->id;
				
				$st=$db->prepare("SELECT id from make WHERE name=:name");
				$st->bindParam("name",$makeId,PDO::PARAM_STR);
				$st->execute();
				$data=$st->fetch(PDO::FETCH_OBJ);
				$makeId=$data->id;
				
				$st=$db->prepare("SELECT id from model WHERE Model=:name");
				$st->bindParam("name",$modelId,PDO::PARAM_STR);
				$st->execute();
				$data=$st->fetch(PDO::FETCH_OBJ);
				$modelId=$data->id;
				
				$stmt->bindParam("equipmentId", $equipmentTypeId,PDO::PARAM_INT) ;
				$stmt->bindParam("leadId", $leadId,PDO::PARAM_INT) ;
				$stmt->bindParam("makeId", $makeId,PDO::PARAM_INT) ;
				$stmt->bindParam("quantity", $quantity,PDO::PARAM_STR) ;
				$stmt->bindParam("projectStage", $projectStage,PDO::PARAM_STR) ;
				$stmt->bindParam("capacity", $capacity,PDO::PARAM_STR) ;
				$stmt->bindParam("location", $location,PDO::PARAM_STR) ;
				$stmt->bindParam("modelId", $modelId,PDO::PARAM_INT) ;
				$stmt->bindParam("district", $district,PDO::PARAM_STR) ;
				$stmt->bindParam("year", $year,PDO::PARAM_STR) ;
				$stmt->bindParam("expectedStartDate", $expectedStartDate,PDO::PARAM_STR) ;
				$stmt->bindParam("operatorAllowance", $operatorAllowance,PDO::PARAM_BOOL) ;
				$stmt->bindParam("food", $food,PDO::PARAM_BOOL) ;
				$stmt->bindParam("shiftTypeNight", $shiftTypeNight,PDO::PARAM_BOOL) ;
				$stmt->bindParam("shiftTypeDay", $shiftTypeDay,PDO::PARAM_BOOL) ;
				$stmt->bindParam("accommodation", $accommodation,PDO::PARAM_BOOL) ;
				$stmt->bindParam("operationalHoursInADay", $operationalHoursInADay,PDO::PARAM_STR) ;
				$stmt->bindParam("operationalDaysInAMonth", $operationalDaysInAMonth,PDO::PARAM_STR) ;
				$stmt->bindParam("operationalHoursInAMonth", $operationalHoursInAMonth,PDO::PARAM_STR) ;
				$stmt->bindParam("vehicleDocuments", $vehicleDocuments,PDO::PARAM_STR) ;
				$stmt->bindParam("operatorLicense", $operatorLicense,PDO::PARAM_STR) ;
				$stmt->execute();
				$db=null;
				return true;
			}
			catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
     }


	public function setAttachment($target_file,$leadEquipId)
	{
		try{

			$db=getDB();
			$st=$db->prepare("INSERT INTO attachments (tableName,url) VALUES (:tableName,:url)");
			$tableName="lead_equipment";
			$st->bindParam("tableName",$tableName,PDO::PARAM_STR);
			$st->bindParam("url",$target_file,PDO::PARAM_STR);
			$st->execute();
			$attachment_id=$db->lastInsertId();
			$st=$db->prepare("UPDATE lead_equipment SET attachmentId=:attachmentId WHERE id=:id");
			$st->bindParam("id",$leadEquipId,PDO::PARAM_STR);
			$st->bindParam("attachmentId",$attachment_id,PDO::PARAM_STR);
			$st->execute();
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
	}



	public function getTypeOfWorkID($name)
	{
		try{

	          $db = getDB();
	          $stmt = $db->prepare("SELECT id FROM type_of_work WHERE name=:name");
	          $stmt->bindParam("name", $name,PDO::PARAM_STR);
	          $stmt->execute();
	          $data=$stmt->fetch(PDO::FETCH_OBJ);
	          return $data->id; 	
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
	}

	public function getSourcesID($name)
	{
		try{

	          $db = getDB();
	          $stmt = $db->prepare("SELECT id FROM sources WHERE name=:name");
	          $stmt->bindParam("name", $name,PDO::PARAM_STR);  
	          $stmt->execute();
	          $data=$stmt->fetch(PDO::FETCH_OBJ);
	          return $data->id; 	
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
	}


     public function leadAdd($name,$phone,$leadManagerId,$LeadOperatorId,$tableName,$leadPriority,$leadDate,$leadType,$sources,$typeOfWork)
		{
			try{
				$db = getDB();
			    $customerId=$this->checkRenterInDB($phone);
			    $sourcesID=$this->getSourcesID($sources);
			    $typeOfWorkID=$this->getTypeOfWorkID($typeOfWork);
			    if($customerId==0)
				{
					$isActive=1;
					$stmt = $db->prepare("INSERT INTO renters(renterName,phone,isActive,createdBy,lastUpdatedBy) VALUES (:renterName,:phone,:isActive,:createdBy,:lastUpdatedBy)"); 
					$stmt->bindParam("renterName", $name,PDO::PARAM_STR) ;
					$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL) ;
					$stmt->bindParam("phone", $phone,PDO::PARAM_STR);
					$stmt->bindParam("createdBy", $_SESSION['userId'],PDO::PARAM_INT) ;
					$stmt->bindParam("lastUpdatedBy", $_SESSION['userId'],PDO::PARAM_INT) ;
					$stmt->execute();
				}
				else
				{
					$isActive=1;
          			$stmt = $db->prepare("UPDATE renters SET renterName=:renterName,lastUpdatedBy=:lastUpdatedBy WHERE phone=:phone AND isActive=:isActive"); 
					$stmt->bindParam("isActive", $isActive,PDO::PARAM_STR) ;
					$stmt->bindParam("renterName", $name,PDO::PARAM_STR) ;
					$stmt->bindParam("phone", $phone,PDO::PARAM_STR);
					$stmt->bindParam("lastUpdatedBy", $_SESSION['userId'],PDO::PARAM_INT) ;
					$stmt->execute();	
				}
				$stmt = $db->prepare("INSERT INTO leads(leadManagerId,customerId,tableName,leadPriority,leadDate,isActive,createdOn,createdBy,lastUpdatedOn,lastUpdatedBy,deletedOn,deletedBy,leadType,sources,typeOfWork,renterAcceptQuotation,supplierConfirmed,paperWorkAndAdvance,priceCardSent,movedToOrders,accept,reject,activate,quotationSent) VALUES (:leadManagerId,:customerId,:tableName,:leadPriority,:leadDate,:isActive,:createdOn,:createdBy,:lastUpdatedOn,:lastUpdatedBy,:deletedOn,:deletedBy,:leadType,:sources,:typeOfWork,:renterAcceptQuotation,:supplierConfirmed,:paperWorkAndAdvance,:priceCardSent,:movedToOrders,:accept,:reject,:activate,:quotationSent)");  
			    
			    $time = date('Y-m-d H:i:s',time());
			    $defaultTime="0000-00-00 00:00:00";
			    $isActive=1;
			    $deletedOn=$defaultTime;
			    $deletedBy=0;
			    $renterAcceptQuotation=0;
			    $supplierConfirmed=0;
			    $priceCardSent=0;
			    $movedToOrders=0;
			    $paperWorkAndAdvance=0;
			    $accept=0;
			    $reject=0;
			    $activate=0;
		    	$createdBy=$_SESSION['userId'];
		    	$lastUpdatedBy=$_SESSION['userId'];
		    	$customerId=$this->checkRenterInDB($phone);
			    
	    		$stmt->bindParam("quotationSent", $quotationSent,PDO::PARAM_BOOL) ;
	    		$stmt->bindParam("renterAcceptQuotation", $renterAcceptQuotation,PDO::PARAM_BOOL) ;
	    		$stmt->bindParam("supplierConfirmed", $supplierConfirmed,PDO::PARAM_BOOL) ;
	    		$stmt->bindParam("paperWorkAndAdvance", $paperWorkAndAdvance,PDO::PARAM_BOOL) ;
	    		$stmt->bindParam("priceCardSent", $priceCardSent,PDO::PARAM_BOOL) ;
	    		$stmt->bindParam("movedToOrders", $movedToOrders,PDO::PARAM_BOOL) ;
	    		$stmt->bindParam("accept", $accept,PDO::PARAM_BOOL) ;
	    		$stmt->bindParam("reject", $reject,PDO::PARAM_BOOL) ;
	    		$stmt->bindParam("activate", $activate,PDO::PARAM_BOOL) ;	    		
				$stmt->bindParam("leadManagerId", $leadManagerId,PDO::PARAM_INT) ;
				$stmt->bindParam("typeOfWork", $typeOfWorkID,PDO::PARAM_INT) ;
				$stmt->bindParam("sources", $sourcesID,PDO::PARAM_INT) ;
				$stmt->bindParam("customerId", $customerId,PDO::PARAM_INT) ;
				$stmt->bindParam("tableName", $tableName,PDO::PARAM_STR) ;
				$stmt->bindParam("leadPriority", $leadPriority,PDO::PARAM_STR) ;
				$stmt->bindParam("leadDate", $leadDate,PDO::PARAM_STR) ;
				$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL) ;
				$stmt->bindParam("createdOn", $time,PDO::PARAM_STR) ;
				$stmt->bindParam("createdBy", $createdBy,PDO::PARAM_INT) ;
				$stmt->bindParam("lastUpdatedOn", $time,PDO::PARAM_STR) ;
				$stmt->bindParam("lastUpdatedBy", $lastUpdatedBy,PDO::PARAM_INT) ;
				$stmt->bindParam("deletedOn", $deletedOn,PDO::PARAM_STR) ;
				$stmt->bindParam("deletedBy", $deletedBy,PDO::PARAM_INT) ;
				$stmt->bindParam("leadType", $leadType,PDO::PARAM_STR) ;
				$stmt->execute();
				$last_id = $db->lastInsertId();
				if($LeadOperatorId!=0)
				{
					$stmt = $db->prepare("UPDATE leads set LeadOperatorId=:LeadOperatorId WHERE id=:id");
					$stmt->bindParam("id", $last_id,PDO::PARAM_INT) ;
					$stmt->bindParam("LeadOperatorId", $LeadOperatorId,PDO::PARAM_INT) ;
					$stmt->execute();
				}
				$db = null;
				echo $last_id;
				return $last_id;
			}
			catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    	}
      	

		}

	public function checkLevelInDB($level)
	{
		try{
      	  $db = getDB();
          $stmt = $db->prepare("SELECT access FROM access_level WHERE access=:access");
          $stmt->bindParam("access", $level,PDO::PARAM_STR);
  		  $stmt->execute();
          $data=$stmt->fetch(PDO::FETCH_OBJ);
          $db = null;
          if($data)
          {
          		return true;
          }
          else
          {
               return false;
          }
      }catch(PDOException $e) {
			http_response_code($GLOBALS['connection_error']);
			echo $e->getMessage();
    }
  	  
  	
 	}

 	public function checkUserInDB($phone)
		{
			try{
          	  $db = getDB();
	          $stmt = $db->prepare("SELECT * FROM staff_login WHERE phone=:phone AND isActive=:isActive");
	          $stmt->bindParam("phone", $phone,PDO::PARAM_STR);
      		  $isActive=1;
      		  $stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL) ;
              $stmt->execute();
	          $data=$stmt->fetch(PDO::FETCH_OBJ);
	          $db = null;
	          if($data)
	          {
	          		return $data;
	          }
	          else
	          {
	               return false;
	          }
	      }catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
      	
     	}
     	public function checkUserInDBAll($phone)
		{
			try{
          	  $db = getDB();
	          $stmt = $db->prepare("SELECT * FROM staff_login WHERE phone=:phone");
	          $stmt->bindParam("phone", $phone,PDO::PARAM_STR);
      		  $stmt->execute();
	          $data=$stmt->fetch(PDO::FETCH_OBJ);
	          $db = null;
	          if($data)
	          {
	          		return $data;
	          }
	          else
	          {
	               return false;
	          }
	      }catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
      	
     	}
     public function checkRenterInDB($phone)
		{
			try{
          	  $db = getDB();
	          $stmt = $db->prepare("SELECT * FROM renters WHERE phone=:phone");
	          $stmt->bindParam("phone", $phone,PDO::PARAM_STR);
      		  $stmt->execute();
	          $data=$stmt->fetch(PDO::FETCH_OBJ);
	          $db = null;
	          if($data)
	          {
	          		return $data->id;
	          }
	          else
	          {
	               return false;
	          }
	      }catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
      	
     	}

	public function leadUserAdd($password,$email,$name,$phone,$accessLevels,$majorRole)
	{
		try{
		  
		  $db = getDB();
          $id1=$this->checkUserInDBAll($phone);
          if($id1)
          {
		   //    $accessLevelsfromDB=$this->getAccessLevels($id);
		   //    $teams=array("field","order","leads","fulfillment","vendors");
		   //    $c=0;
		   //    for($j=0;$j<count($teams);$j++)
			  // {
			  // 	if(in_array($teams[$j], $accessLevelsfromDB))$c++;
			  // }
			  // if($c>1){
			  // 	http_response_code($GLOBALS['forbidden']);
			  // 	return false;
			  // }
          		$id=$id1->userId;
          		if($id1->isActive==0)	
		      	{
		      		$isActive=1;
		      		if($_SESSION['majorRole']=="manager")
	            	{
	            		// $majorRole="operator";
	            		$y=$this->checkUserInDB($_SESSION['userId']);
		                $stmt = $db->prepare("UPDATE staff_login SET isActive=:isActive,adminId=:adminId,managerId=:managerId,majorRole=:majorRole WHERE userId=:id");
		                $stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
	                	$stmt->bindParam("managerId", $_SESSION['userId'],PDO::PARAM_INT);
	                	$stmt->bindParam("adminId", $y->adminId,PDO::PARAM_INT);
	                	$stmt->bindParam("majorRole", $majorRole,PDO::PARAM_STR);
		      			$stmt->bindParam("id", $id,PDO::PARAM_INT);
						$stmt->execute();
	      			}
	      			else if($_SESSION['majorRole']=="admin")
	            	{
	            		// $majorRole=="manager";
		                $stmt = $db->prepare("UPDATE staff_login SET isActive=:isActive,adminId=:adminId,majorRole=:majorRole WHERE userId=:id");
		                $stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
	                	$stmt->bindParam("adminId", $_SESSION['userId'],PDO::PARAM_INT);
	                	$stmt->bindParam("majorRole", $majorRole,PDO::PARAM_STR);
		      			$stmt->bindParam("id", $id,PDO::PARAM_INT);
						$stmt->execute();
	      			}
	      			else
	            	{
		                $db = null;
	                	return false;
	      			}
	      			if(in_array("leads", $accessLevels))
	                {
	                	$accessLevel=7;
	                	$stmt2 = $db->prepare("INSERT INTO access(userId,accessLevel) VALUES (:userid,:accessLevel)");  
		                $stmt2->bindParam("userid", $id,PDO::PARAM_INT) ;
		                $stmt2->bindParam("accessLevel", $accessLevel,PDO::PARAM_INT) ;
		                $stmt2->execute();
	                }
	                else if(in_array("fulfillment", $accessLevels))
	                {
	                	$accessLevel=9;
	                	$stmt2 = $db->prepare("INSERT INTO access(userId,accessLevel) VALUES (:userid,:accessLevel)");  
		                $stmt2->bindParam("userid", $id,PDO::PARAM_INT) ;
		                $stmt2->bindParam("accessLevel", $accessLevel,PDO::PARAM_INT) ;
		                $stmt2->execute();
	                }
	                else
	                	http_response_code($GLOBALS['unauthorized']);
			    }
			    else
			    {
			    	return false;
			    	http_response_code($GLOBALS['forbidden']);
			    }
		      	$x=$this->leadUserUpdate($password,$email,$name,$phone,$phone);
		      	if($x)echo "done";
		      	else echo "not done";
		      	$db = null;
                return true;
			}
		  else{
				$stmt = $db->prepare("INSERT INTO staff_login(password,email,name,phone,majorRole,adminId,managerId,isActive) VALUES (:hash_password,:email,:name,:phone,:majorRole,:adminId,:managerId,:isActive)");  
                $hash_password= hash('sha256', $password);
                $isActive=1;
                $stmt->bindParam("hash_password", $hash_password,PDO::PARAM_STR) ;
                $stmt->bindParam("email", $email,PDO::PARAM_STR) ;
                $stmt->bindParam("name", $name,PDO::PARAM_STR) ;
                $stmt->bindParam("phone", $phone,PDO::PARAM_STR) ;
                $stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL) ;
                
                if($_SESSION['majorRole']=="manager")
            	{
            		// $majorRole="operator";
            		$stmt->bindParam("majorRole", $majorRole,PDO::PARAM_STR) ;
                    $adminId=0;
	                $stmt->bindParam("managerId", $_SESSION['userId'],PDO::PARAM_INT);
                	$stmt->bindParam("adminId", $adminId,PDO::PARAM_INT) ;
                }
      			else if($_SESSION['majorRole']=="admin")
            	{
	                // $majorRole=="manager";
	                $stmt->bindParam("majorRole", $majorRole,PDO::PARAM_STR) ;
                    $managerId=0;
	                $stmt->bindParam("adminId", $_SESSION['userId'],PDO::PARAM_INT);
	                $stmt->bindParam("managerId", $managerId,PDO::PARAM_INT);              
                }
      			else
            	{
	                $db=null;
					return false;
                
      			}
                $stmt->execute();
                $userinfo=$this->checkUserInDB($phone);
                $userid=$userinfo->userId;
                if(in_array("leads", $accessLevels))
                {
                	$accessLevel=7;
                	$stmt2 = $db->prepare("INSERT INTO access(userId,accessLevel) VALUES (:userid,:accessLevel)");  
	                $stmt2->bindParam("userid", $userid,PDO::PARAM_INT) ;
	                $stmt2->bindParam("accessLevel", $accessLevel,PDO::PARAM_INT) ;
	                $stmt2->execute();
                }
                else if(in_array("fulfillment", $accessLevels))
                {
                	$accessLevel=9;
                	$stmt2 = $db->prepare("INSERT INTO access(userId,accessLevel) VALUES (:userid,:accessLevel)");  
	                $stmt2->bindParam("userid", $userid,PDO::PARAM_INT) ;
	                $stmt2->bindParam("accessLevel", $accessLevel,PDO::PARAM_INT) ;
	                $stmt2->execute();
                }
                else
                	http_response_code($GLOBALS['unauthorized']);
				$db=null;
				return true;
			}
           }catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
	}

	
	public function leadUserDelete($phone)
	{
		try{
		  
			$db = getDB();
			$id=$this->checkUserInDB($phone);
			if($id)
			{
				if(($_SESSION['majorRole']=="manager" && $id->majorRole=="operator")||($_SESSION['majorRole']=="admin" && $id->majorRole=="manager"))
          		{
          			$isActive=0;
      				$stmt = $db->prepare("UPDATE staff_login SET isActive=:isActive WHERE userId=:userId");  
      				$stmt->bindParam("userId", $id->userId,PDO::PARAM_INT) ;
      				$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
      				$stmt->execute();
      				$stmt = $db->prepare("DELETE from access WHERE userId=:userId");
      		    	$stmt->bindParam("userId", $id->userId,PDO::PARAM_INT) ;
      				$stmt->execute();
      				return true;

          		}
          		else
          		{
          			http_response_code($GLOBALS['unauthorized']);	
          		}
  		 	
		  	}
			else
			{
				http_response_code($GLOBALS['noContent']);	
				return false;
			}
           }catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
	}

	public function leadUserUpdate($password,$email,$name,$newPhone,$oldPhone)
	{
		try{
		  
		  $db = getDB();
          $id=$this->checkUserInDB($oldPhone);
          if($id)
          {
          	$id=$id->userId;
          	if($password)
      		{
			    $stmt = $db->prepare("UPDATE staff_login SET password=:hash_password,email=:email,name=:name,phone=:phone WHERE userId=:userId");  
                $hash_password= hash('sha256', $password);
                $stmt->bindParam("hash_password", $hash_password,PDO::PARAM_STR) ;
                $stmt->bindParam("email", $email,PDO::PARAM_STR) ;
                $stmt->bindParam("name", $name,PDO::PARAM_STR) ;
                $stmt->bindParam("phone", $newPhone,PDO::PARAM_STR) ;
                $stmt->bindParam("userId", $id,PDO::PARAM_INT) ;
                $stmt->execute();
                
			}
			else
			{
				$stmt = $db->prepare("UPDATE staff_login SET email=:email,name=:name,phone=:phone WHERE userId=:userId");  
                $stmt->bindParam("email", $email,PDO::PARAM_STR) ;
                $stmt->bindParam("name", $name,PDO::PARAM_STR) ;
                $stmt->bindParam("phone", $newPhone,PDO::PARAM_STR) ;
                $stmt->bindParam("userId", $id,PDO::PARAM_INT) ;
                $stmt->execute();
			}
			$db = null;
            http_response_code($GLOBALS['success']);
			return true;
		  }
			else
			{
				return false;
			}
           }catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
	}
	
	public function leadUserProfileUpdate($email,$name,$newPhone,$oldPhone,$password)
	{
		try{
		  
		  $db = getDB();
          $id=$_SESSION['userId'];
          if($id)
          {
          	$stmt = $db->prepare("UPDATE staff_login SET password=:hash_password,email=:email,name=:name,phone=:phone WHERE userId=:userId AND isActive=:isActive");  
            $hash_password= hash('sha256', $password);
            $stmt->bindParam("hash_password", $hash_password,PDO::PARAM_STR) ;
            $stmt->bindParam("email", $email,PDO::PARAM_STR) ;
            $stmt->bindParam("name", $name,PDO::PARAM_STR) ;
            $stmt->bindParam("phone", $newPhone,PDO::PARAM_STR) ;
            $isActive=1;
            $stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL) ;
			$stmt->bindParam("userId", $id,PDO::PARAM_INT) ;
            $stmt->execute();
            $db = null;
            http_response_code($GLOBALS['success']);
			return true;
		  }
			else
			{
				return false;
			}
           }catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
	}
	

	public function leadUserAssignLevel($id,$level)
	{
		try{
		  
		  $db = getDB();
          $st = $db->prepare("SELECT * from staff_login WHERE userId=:id and isActive=:isActive");  
          $st->bindParam("id", $id,PDO::PARAM_INT);
          $isActive=1;
          $st->bindParam("isActive", $isActive,PDO::PARAM_BOOL) ;
	  	  $st->execute();
          $count=$st->rowCount();
          if($count==1)
          {
                
                $stmt = $db->prepare("SELECT id FROM access_level WHERE access=:access");
	            $stmt->bindParam("access", $level,PDO::PARAM_STR);
      		    $stmt->execute();
      		    $data=$stmt->fetch(PDO::FETCH_OBJ);
      		    if($data)
      		    {
      		    	$stmt = $db->prepare("INSERT INTO access(userId,accessLevel) VALUES (:userId,:accessLevel)");
      		    	$stmt->bindParam("userId", $id,PDO::PARAM_INT);
      		    	$stmt->bindParam("accessLevel", $data->id,PDO::PARAM_INT);
      		    	$stmt->execute();
      		    }
                $db = null;
                return true;
			}else{
				$db=null;
				return false;

           	}
           	}catch(PDOException $e){
           		http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
           	}
		}

	public function leadUserDeallocateLevel($id,$level)
	{
		try{
		  
		  $db = getDB();
          $st = $db->prepare("SELECT * from staff_login WHERE userId=:id and isActive=:isActive");  
          $st->bindParam("id", $id,PDO::PARAM_INT);
          $isActive=1;
          $st->bindParam("isActive", $isActive,PDO::PARAM_BOOL) ;
	  	  $st->execute();
          $count=$st->rowCount();
          if($count==1)
          {
                
                $stmt = $db->prepare("SELECT id FROM access_level WHERE access=:access");
	            $stmt->bindParam("access", $level,PDO::PARAM_STR);
      		    $stmt->execute();
      		    $data=$stmt->fetch(PDO::FETCH_OBJ);
      		    if($data)
      		    {
      		    	$stmt = $db->prepare("DELETE from access WHERE userId=:userId,accessLevel=:accessLevel");
      		    	$stmt->bindParam("userId", $id,PDO::PARAM_INT);
      		    	$stmt->bindParam("accessLevel", $data->id,PDO::PARAM_INT);
      		    	$stmt->execute();
      		    }
                $db = null;
                return true;
			}else{
				$db=null;
				return false;

           	}
           	}catch(PDOException $e){
           		http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
           	}
		}

	public function getLeadsMovedToOrders()
	{
		try{
      	  $db = getDB();
          $movedToOrders=1;
          $isActive=1;
          $stmt = $db->prepare("SELECT * FROM leads WHERE movedToOrders=:movedToOrders and isActive=:isActive");
          $st->bindParam("isActive", $isActive,PDO::PARAM_BOOL) ;
	  	  $stmt->bindParam("movedToOrders", $movedToOrders,PDO::PARAM_BOOL);
          $stmt->execute();
          $data=$stmt->fetchAll(PDO::FETCH_OBJ);
          $db = null;
          if($data)
          {
          		echo $data;
          }
          else
          {
               return "noData";
          } 	
 	}
      catch(PDOException $e) {
      echo '{"error":{"text":'. $e->getMessage() .'}}'; 
      }   
	}


	public function setLastUpdated($id)
	{
		try{
      $db = getDB();
      $st = $db->prepare("SELECT * from leads WHERE id=:id and isActive=:isActive");  
      $isActive=1;
      $st->bindParam("isActive", $isActive,PDO::PARAM_BOOL) ;
	  $st->bindParam("id", $id,PDO::PARAM_INT);
  	  $st->execute();
      $count=$st->rowCount();
      if($count>=1)
      {
            $time = date('Y-m-d H:i:s',time());
            $lastUpdatedBy=$_SESSION['userId'];
            $stmt = $db->prepare("UPDATE leads SET lastUpdatedBy=:lastUpdatedBy,lastUpdatedOn=:lastUpdatedOn WHERE id=:id");  
            $stmt->bindParam("lastUpdatedBy", $lastUpdatedBy,PDO::PARAM_INT) ;
			$stmt->bindParam("lastUpdatedOn", $time,PDO::PARAM_STR) ;
			$stmt->bindParam("id", $id,PDO::PARAM_INT);
  			$stmt->execute();
            $db = null;
            return true;
		}else{
			$db=null;
			return false;

       	}
       	}catch(PDOException $e){
       		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
       	}	
	}



	public function assignFulfilmentOperator($id,$phone)
	{
	  try{
      $db = getDB();
      $st = $db->prepare("SELECT * from lead_equipment WHERE id=:id");  
      $st->bindParam("id", $id,PDO::PARAM_INT);
  	  $st->execute();
      $count=$st->rowCount();
      if($count>=1)
      {
            $stmt = $db->prepare("SELECT leadId from lead_equipment WHERE id=:id");  
            $stmt->bindParam("id", $id,PDO::PARAM_INT);
  			$stmt->execute();
            $data=$stmt->fetch(PDO::FETCH_OBJ);
      		$this->setLastUpdated($data->leadId);

      		$userDetails=$this->checkUserInDB($phone);
      		if($userDetails)
  			{
  				$fulfilmentOperator=$userDetails->userId;
  				$accessLevels=$this->getAccessLevels($fulfilmentOperator);
  				// echo var_dump($accessLevels);
  				if(in_array("fulfillment", $accessLevels))
  				{
  					$stmt = $db->prepare("UPDATE lead_equipment SET fulfilmentOperator=:fulfilmentOperator WHERE id=:id");  
		            $stmt->bindParam("fulfilmentOperator", $fulfilmentOperator,PDO::PARAM_INT) ;
					$stmt->bindParam("id", $id,PDO::PARAM_INT);
		  			$stmt->execute();
		            $db = null;
  					return true;
  				}
  				else
  				{
  					// echo "yaha se 1";
  					return false;
  				}
            }
            else
            {
            	// echo "yaha se 2";
  				return false;
            }
            
		}else{
			$db=null;
			// echo "yaha se 3";
  			return false;

       	}
       	}catch(PDOException $e){
           		http_response_code($GLOBALS['connection_error']);
  				echo $e->getMessage();
           	}
	}

	public function assignFulfilmentManager($id,$fulfilmentManager)
	{
		try{
      $db = getDB();
      $st = $db->prepare("SELECT * from leads WHERE id=:id and isActive=:isActive");
      $isActive=1;
      $st->bindParam("isActive", $isActive,PDO::PARAM_BOOL) ;
	  $st->bindParam("id", $id,PDO::PARAM_INT);
  	  $st->execute();
      $count=$st->rowCount();
      if($count>=1)
      {
            $stmt = $db->prepare("UPDATE lead_equipment SET fulfilmentOperator=:fulfilmentOperator WHERE id=:id");  
            $stmt->bindParam("fulfilmentOperator", $fulfilmentOperator,PDO::PARAM_INT) ;
			$stmt->bindParam("id", $id,PDO::PARAM_INT);
  			$stmt->execute();
            $db = null;
            
            return true;
		}else{
			$db=null;
			return false;

       	}
       	}catch(PDOException $e){
           		http_response_code($GLOBALS['connection_error']);
  				echo $e->getMessage();
           	}
	}

	public function leadEquipDelete($leadId)
     {
          try{
          $db = getDB();
          $st = $db->prepare("SELECT * from lead_equipment WHERE leadId=:id");  
          $st->bindParam("id", $leadId,PDO::PARAM_INT);
          $st->execute();
          $count=$st->rowCount();
          if($count>=1)
          {
                $stmt = $db->prepare("UPDATE lead_equipment SET leadId=NULL WHERE leadId=:leadId");  
                $stmt->bindParam("leadId", $leadId,PDO::PARAM_INT) ;
				$stmt->execute();
                $db = null;
                return true;
			}else{
				$db=null;
				return false;

           	}
           	}catch(PDOException $e){
           		http_response_code($GLOBALS['connection_error']);
  				echo $e->getMessage();
           	}

   	}	

   	public function getSuggestionsRenters($phone)
     {
          try{

			$db = getDB();
			$isActive=1;
            $phonestr = '%'.$phone.'%';
            $stmt = $db->prepare("SELECT renterName,phone FROM renters WHERE isActive=:isActive AND phone LIKE :phone");
            $stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
            $stmt->bindParam("phone", $phonestr,PDO::PARAM_STR);
            // $stmt->bindParam("renterName", $namestr,PDO::PARAM_STR);
            $stmt->execute();
            $data=$stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
			if($data)
			{
			   return $data;
			}
			else
			{
			    http_response_code($GLOBALS['notFound']);
			}    
          }
          catch(PDOException $e) {
               http_response_code($GLOBALS['connection_error']);
          }   
     }

    public function getSuggestionsSuppliers($phone)
     {
          try{

			$db = getDB();
			$isActive=1;
            $phonestr = '%'.$phone.'%';
            $stmt = $db->prepare("SELECT OwnerName,phone FROM suppliers WHERE isActive=:isActive AND phone LIKE :phone");
            $stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
            $stmt->bindParam("phone", $phonestr,PDO::PARAM_STR);
            // $stmt->bindParam("renterName", $namestr,PDO::PARAM_STR);
            $stmt->execute();
            $data=$stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
			if($data)
			{
			   return $data;
			}
			else
			{
			    http_response_code($GLOBALS['notFound']);
			}    
          }
          catch(PDOException $e) {
               http_response_code($GLOBALS['connection_error']);
          }   
     }

    public function getSuggestionsFulfilmentOperators($phone)
     {
          try{

			$db = getDB();
			$isActive=1;
            $phonestr = '%'.$phone.'%';
            $majorRole="operator";
            $stmt = $db->prepare("SELECT userId,name,phone FROM staff_login WHERE isActive=:isActive AND majorRole=:majorRole AND phone LIKE :phone");
            $stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
            $stmt->bindParam("phone", $phonestr,PDO::PARAM_STR);
            $stmt->bindParam("majorRole", $majorRole,PDO::PARAM_STR);
            // $stmt->bindParam("renterName", $namestr,PDO::PARAM_STR);
            $stmt->execute();
            $data=$stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
			if($data)
			{
				$responseArr=array();
				for($i=0;$i<count($i);$i++)
				{
					$accessLevels=$this->getAccessLevels($data[$i]->userId);
					if(in_array("fulfillment", $accessLevels))
					{
						array_push($responseArr, $data[$i]);
					}
					
			    }
			    return $responseArr;
			}
			else
			{
			    http_response_code($GLOBALS['notFound']);
			}    
          }
          catch(PDOException $e) {
               http_response_code($GLOBALS['connection_error']);
          }   
     }

   	public function assignSupplierToEquipment($equipId,$supplierName1,$supplierName2,$supplierName3,$supplierPhone1,$supplierPhone2,$supplierPhone3,$remarks,$attachment)
     {
          try{
          	echo "entered";
          $db = getDB();
          $st = $db->prepare("SELECT * from lead_equipment WHERE id=:id");  
          $st->bindParam("id", $equipId,PDO::PARAM_INT);
          $st->execute();
          $count=$st->rowCount();
          if($count>=1)
          {
          	echo "entered-inside";
                $stmt = $db->prepare("UPDATE lead_equipment SET supplierName1=:supplierName1,supplierName2=:supplierName2,supplierName3=:supplierName3,supplierPhone1=:supplierPhone1,supplierPhone2=:supplierPhone2,supplierPhone3=:supplierPhone3,remarks=:remarks,attachment=:attachment WHERE id=:equipId");
                $confirmed=1;

                $stmt->bindParam("equipId", $equipId,PDO::PARAM_INT) ;
				$stmt->bindParam("supplierName1", $supplierName1,PDO::PARAM_STR) ;
				$stmt->bindParam("supplierName2", $supplierName2,PDO::PARAM_STR) ;
				$stmt->bindParam("supplierName3", $supplierName3,PDO::PARAM_STR) ;
				$stmt->bindParam("supplierPhone1", $supplierPhone1,PDO::PARAM_STR) ;
				$stmt->bindParam("supplierPhone2", $supplierPhone2,PDO::PARAM_STR) ;
				$stmt->bindParam("supplierPhone3", $supplierPhone3,PDO::PARAM_STR) ;
				// $stmt->bindParam("confirmed", $confirmed,PDO::PARAM_BOOL) ;
				$stmt->bindParam("remarks", $remarks,PDO::PARAM_STR) ;
				$uploadFile = new uploadFile();
				// print_r($attachment);
				// echo "hello";
				$attachment = $uploadFile->upload($attachment);
				// $attachment="nanana";
				$stmt->bindParam("attachment", $attachment,PDO::PARAM_STR) ;
				$stmt->execute();
                $db = null;
                // echo "done";
                return true;
			}else{
				$db=null;
				// echo "not happening bruh";
				return false;

           	}
           	}catch(PDOException $e){
           		echo $e;
           		http_response_code($GLOBALS['connection_error']);
  				// echo $e->getMessage();
           	}

   	}	

   	public function assignPriceTag($id,$priceTag)
     {
          try{
          $db = getDB();
          $st = $db->prepare("SELECT * from lead_equipment WHERE id=:id");  
          $st->bindParam("id", $id,PDO::PARAM_INT);
          $st->execute();
          $count=$st->rowCount();
          if($count>=1)
          {
                $stmt = $db->prepare("UPDATE lead_equipment SET priceTag=:priceTag WHERE id=:id");  
                $stmt->bindParam("id", $id,PDO::PARAM_INT);
      			$stmt->bindParam("priceTag", $priceTag,PDO::PARAM_STR) ;
				$stmt->execute();
                $db = null;
                return true;
			}else{
				$db=null;
				return false;

           	}
           	}catch(PDOException $e){
           		http_response_code($GLOBALS['connection_error']);
  				echo $e->getMessage();
           	}

   	}

   	public function assignConfirmed($id)
     {
		try{
			$db = getDB();
			$st = $db->prepare("SELECT * from lead_equipment WHERE id=:id AND supplierPhone1 IS NOT NULL AND supplierName1 IS NOT NULL AND priceTag IS NOT NULL");  
			$st->bindParam("id", $id,PDO::PARAM_INT);
			$st->execute();
			$data=$st->fetchAll(PDO::FETCH_OBJ);
			if($data)
			{
			    $confirmed=1;
				$stmt = $db->prepare("UPDATE lead_equipment SET confirmed=:confirmed WHERE id=:id");  
			    $stmt->bindParam("id", $id,PDO::PARAM_INT);
				$stmt->bindParam("confirmed", $confirmed,PDO::PARAM_BOOL) ;
				$stmt->execute();
				return true;
			}
			else
			{
				$db=null;
				return false;
			}
           	}catch(PDOException $e){
           		http_response_code($GLOBALS['connection_error']);
  				echo $e->getMessage();
           	}

   	}	

   	public function assignPriceCard($id,$priceCard)
     {
          try{
          $db = getDB();
          $st = $db->prepare("SELECT id from leads WHERE id=:id AND isActive=:isActive");  
          $isActive=1;
          echo "hiii";
          $st->bindParam("isActive", $isActive,PDO::PARAM_BOOL) ;
	  	  $st->bindParam("id", $id,PDO::PARAM_INT);
          $st->execute();
          $count=$st->rowCount();
          if($count>=1)
          {
                $stmt = $db->prepare("UPDATE leads SET priceCard=:priceCard WHERE id=:id");  
                $stmt->bindParam("id", $id,PDO::PARAM_INT);
      			$stmt->bindParam("priceCard", $priceCard,PDO::PARAM_STR) ;
				$stmt->execute();
                $db = null;
                $this->setLastUpdated($id);
                return true;
			}else{

				$db=null;
				return false;

           	}
           	}catch(PDOException $e){
           		http_response_code($GLOBALS['connection_error']);
  				echo $e->getMessage();
           	}

   	}	

	public function assignPriceCardSent($id)
     {
          try{
          $db = getDB();
          $st = $db->prepare("SELECT id from leads WHERE id=:id and isActive=:isActive");  
          $isActive=1;
          $st->bindParam("isActive", $isActive,PDO::PARAM_BOOL) ;
	  	  $st->bindParam("id", $id,PDO::PARAM_INT);
          $st->execute();
          $count=$st->rowCount();
          if($count>=1)
          {
          		$priceCardSent=1;
                $stmt = $db->prepare("UPDATE leads SET priceCardSent=:priceCardSent WHERE id=:id");  
                $stmt->bindParam("id", $id,PDO::PARAM_INT);
      			$stmt->bindParam("priceCardSent", $priceCardSent,PDO::PARAM_BOOL) ;
				$stmt->execute();
                $db = null;
                $this->setLastUpdated($id);
                return true;
			}else{

				$db=null;
				return false;

           	}
           	}catch(PDOException $e){
           		http_response_code($GLOBALS['connection_error']);
  				echo $e->getMessage();
           	}

   	}	

   	public function assignPaperworkAndAdvance($id)
     {
          try{
          $db = getDB();
          $st = $db->prepare("SELECT id from leads WHERE id=:id AND isActive=:isActive");  
          $isActive=1;
          $st->bindParam("isActive", $isActive,PDO::PARAM_BOOL) ;
	  	  $st->bindParam("id", $id,PDO::PARAM_INT);
          $st->execute();
          $count=$st->rowCount();
          if($count>=1)
          {
          		$paperWorkAndAdvance=1;
                $stmt = $db->prepare("UPDATE leads SET paperWorkAndAdvance=:paperWorkAndAdvance WHERE id=:id");  
                $stmt->bindParam("id", $id,PDO::PARAM_INT);
      			$stmt->bindParam("paperWorkAndAdvance", $paperWorkAndAdvance,PDO::PARAM_BOOL) ;
				$stmt->execute();
                $db = null;
                $this->setLastUpdated($id);
                return true;
			}else{

				$db=null;
				return false;

           	}
           	}catch(PDOException $e){
           		http_response_code($GLOBALS['connection_error']);
  				echo $e->getMessage();
           	}

   	}	
	public function assignQuotationSent($id)
     {
          try{
          $db = getDB();
          $st = $db->prepare("SELECT id from leads WHERE id=:id AND isActive=:isActive");  
          $isActive=1;
          $st->bindParam("isActive", $isActive,PDO::PARAM_BOOL) ;
	  	  $st->bindParam("id", $id,PDO::PARAM_INT);
          $st->execute();
          $count=$st->rowCount();
          if($count>=1)
          {
          		$quotationSent=1;
                $stmt = $db->prepare("UPDATE leads SET quotationSent=:quotationSent WHERE id=:id");  
                $stmt->bindParam("id", $id,PDO::PARAM_INT);
      			$stmt->bindParam("quotationSent", $quotationSent,PDO::PARAM_BOOL) ;
				$stmt->execute();
                $db = null;
                $this->setLastUpdated($id);
                return true;
			}else{

				$db=null;
				return false;

           	}
          }catch(PDOException $e){
           		http_response_code($GLOBALS['connection_error']);
  				echo $e->getMessage();
           	}

   	}	
	public function assignRenterAcceptQuotation($id)
     {
          try{
          $db = getDB();
          $st = $db->prepare("SELECT id from leads WHERE id=:id AND isActive=:isActive");  
          $isActive=1;
          $st->bindParam("isActive", $isActive,PDO::PARAM_BOOL) ;
	  	  $st->bindParam("id", $id,PDO::PARAM_INT);
          $st->execute();
          $count=$st->rowCount();
          if($count>=1)
          {
          		$renterAcceptQuotation=1;
                $stmt = $db->prepare("UPDATE leads SET renterAcceptQuotation=:renterAcceptQuotation WHERE id=:id");  
                $stmt->bindParam("id", $id,PDO::PARAM_INT);
      			$stmt->bindParam("renterAcceptQuotation", $renterAcceptQuotation,PDO::PARAM_BOOL) ;
				$stmt->execute();
                $db = null;
                $this->setLastUpdated($id);
                return true;
			}else{

				$db=null;
				return false;

           	}
           	}catch(PDOException $e){
           		http_response_code($GLOBALS['connection_error']);
  				echo $e->getMessage();
           	}

   	}	

   	public function assignApprovedBy($id)
     {
          try{
          $db = getDB();
          $st = $db->prepare("SELECT id from leads WHERE id=:id AND isActive=:isActive");  
          $isActive=1;
          $st->bindParam("isActive", $isActive,PDO::PARAM_BOOL) ;
	  	  $st->bindParam("id", $id,PDO::PARAM_INT);
          $st->execute();
          $count=$st->rowCount();
          if($count>=1)
          {
          		$approvedBy=$_SESSION["userId"];
                $stmt = $db->prepare("UPDATE leads SET approvedBy=:approvedBy WHERE id=:id");  
                $stmt->bindParam("id", $id,PDO::PARAM_INT);
      			$stmt->bindParam("approvedBy", $approvedBy,PDO::PARAM_INT) ;
				$stmt->execute();
                $db = null;
                $this->setLastUpdated($id);
                return true;
			}else{

				$db=null;
				return false;

           	}
           	}catch(PDOException $e){
           		http_response_code($GLOBALS['connection_error']);
  				echo $e->getMessage();
           	}

   	}

   	public function assignMoveToOrders($id)
	{
		try{
      $db = getDB();
      $st = $db->prepare("SELECT * from leads WHERE id=:id AND isActive=:isActive");  
      $isActive=1;
      $st->bindParam("isActive", $isActive,PDO::PARAM_BOOL) ;
	  $stmt->bindParam("id", $id,PDO::PARAM_INT);
  	  $st->execute();
      $count=$st->rowCount();
      if($count>=1)
      {
            $time = date('Y-m-d H:i:s',time());
            $lastUpdatedBy=$_SESSION['userId'];
            $movedToOrders=1;
	    	$stmt = $db->prepare("UPDATE leads SET lastUpdatedBy=:lastUpdatedBy,lastUpdatedOn=:lastUpdatedOn,movedToOrders=:movedToOrders WHERE id=:id");  
            $stmt->bindParam("lastUpdatedBy", $lastUpdatedBy,PDO::PARAM_INT) ;
			$stmt->bindParam("lastUpdatedOn", $time,PDO::PARAM_STR) ;
			$stmt->bindParam("movedToOrders", $movedToOrders,PDO::PARAM_BOOL);
  			$stmt->bindParam("id", $id,PDO::PARAM_INT);
  			$stmt->execute();
            $db = null;
            return true;
		}else{
			$db=null;
			return false;

       	}
       	}catch(PDOException $e){
       		http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
       	}
	}

   	public function assignSupplierConfirmed($id)
     {
          try{
          $db = getDB();
          $st = $db->prepare("SELECT id from leads WHERE id=:id AND isActive=:isActive");  
          $isActive=1;
          $st->bindParam("isActive", $isActive,PDO::PARAM_BOOL) ;
		  $st->bindParam("id", $id,PDO::PARAM_INT);
          $st->execute();
          $count=$st->rowCount();
          if($count>=1)
          {

          		$leadId=$data->leadId;
				$stmt2 = $db->prepare("SELECT confirmed from lead_equipment WHERE leadId=:leadId");  
				$stmt2->bindParam("$leadId", $leadId,PDO::PARAM_INT);
				$stmt2->execute();
				$data2=$stmt2->fetchAll(PDO::FETCH_OBJ);
				$flag=1;
				for($i=0;$i<count($data2);$i++)
				{
					if($data[$i]->confirmed){}
					else $flag=0;
				}
				if($flag)
				{
					$supplierConfirmed=1;
	                $stmt = $db->prepare("UPDATE leads SET supplierConfirmed=:supplierConfirmed WHERE id=:id");  
	                $stmt->bindParam("id", $id,PDO::PARAM_INT);
	      			$stmt->bindParam("supplierConfirmed", $supplierConfirmed,PDO::PARAM_BOOL) ;
					$stmt->execute();
	                $db = null;
	                return true;
				}
				else
				{
					return false;
				}
          		
			}else{

				$db=null;
				return false;

           	}
           	}catch(PDOException $e){
           		http_response_code($GLOBALS['connection_error']);
  				echo $e->getMessage();
           	}

   	}	
     public function leadUpdate($id,$name,$phone,$leadManagerId,$fulfilmentManagerId,$LeadOperatorId,$tableName,$leadPriority,$leadDate,$leadType,$sources,$typeOfWork)
     {
          try{
          $db = getDB();
          echo $id;
          $st = $db->prepare("SELECT id FROM leads WHERE id=:id AND isActive=:isActive");  
          $isActive=1;
          $st->bindParam("isActive", $isActive,PDO::PARAM_BOOL) ;
		  $st->bindParam("id", $id,PDO::PARAM_INT);
          $st->execute();
          $count=$st->rowCount();
          if($count)
          {
                $customerId=$this->checkRenterInDB($phone);
			    if($customerId==0)
				{
					$isActive=1;
					$stmt = $db->prepare("INSERT INTO renters(renterName,phone,isActive,createdBy,lastUpdatedBy) VALUES (:renterName,:phone,:isActive,:createdBy,:lastUpdatedBy)"); 
					$stmt->bindParam("renterName", $name,PDO::PARAM_STR) ;
					$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL) ;
					$stmt->bindParam("phone", $phone,PDO::PARAM_STR);
					$stmt->bindParam("createdBy", $_SESSION['userId'],PDO::PARAM_INT) ;
					$stmt->bindParam("lastUpdatedBy", $_SESSION['userId'],PDO::PARAM_INT) ;
					$stmt->execute();
				}
				else
				{
					$isActive=1;
          			$stmt = $db->prepare("UPDATE renters SET renterName=:renterName,lastUpdatedBy=:lastUpdatedBy WHERE phone=:phone AND isActive=:isActive"); 
					$stmt->bindParam("renterName", $name,PDO::PARAM_STR) ;
					$stmt->bindParam("isActive", $isActive,PDO::PARAM_STR) ;
					$stmt->bindParam("phone", $phone,PDO::PARAM_STR);
					$stmt->bindParam("lastUpdatedBy", $_SESSION['userId'],PDO::PARAM_INT) ;
					$stmt->execute();
				}
				$stmt = $db->prepare("UPDATE leads SET customerId=:customerId,leadPriority=:leadPriority,leadDate=:leadDate,sources=:sources,typeOfWork=:typeOfWork,lastUpdatedBy=:lastUpdatedBy,lastUpdatedOn=:lastUpdatedOn WHERE id=:id AND isActive=:isActive");  
                
                $time = date('Y-m-d H:i:s',time());
			    $defaultTime="0000-00-00 00:00:00";
			    $isActive=1;
			    $deletedOn=$defaultTime;
			    $deletedBy=0;
		    	$lastUpdatedBy=$_SESSION['userId'];
		    	
		    	$typeOfWorkID=$this->getTypeOfWorkID($typeOfWork);
		    	$sourcesID=$this->getSourcesID($sources);
		    	$customerId=$this->checkRenterInDB($phone);
			    
	    		$stmt->bindParam("id", $id,PDO::PARAM_INT) ;
				$stmt->bindParam("typeOfWork", $typeOfWorkID,PDO::PARAM_INT) ;
				$stmt->bindParam("sources", $sourcesID,PDO::PARAM_INT) ;
				$stmt->bindParam("customerId", $customerId,PDO::PARAM_INT) ;
				$stmt->bindParam("leadPriority", $leadPriority,PDO::PARAM_STR) ;
				$stmt->bindParam("leadDate", $leadDate,PDO::PARAM_STR) ;
				$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL) ;
				$stmt->bindParam("lastUpdatedOn", $time,PDO::PARAM_STR) ;
				$stmt->bindParam("lastUpdatedBy", $lastUpdatedBy,PDO::PARAM_INT);
				$stmt->execute();
				
				$db = null;
                return true;
			}else{
				$db=null;
				return false;

           	}
           	}catch(PDOException $e){
           		// http_response_code($GLOBALS['connection_error']);
  				echo $e->getMessage();
           	}


   	}	


}



?>