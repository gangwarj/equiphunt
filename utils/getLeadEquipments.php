<?php

include('config.php');
include('leadClass.php');
$leadClass = new leadClass();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    http_response_code($badRequest);
}

if(!isset($_SESSION['userId']) || empty($_SESSION['userId'])){
    session_destroy();
    http_response_code($session_error);
}

if((in_array("fulfillment", $_SESSION['accessLevel'])&&$_SESSION['majorRole']=="operator"))
{
	$lid=$leadClass->getEquipmentsByFulfilmentOperator();
	if($lid)
	{
		echo json_encode($lid);
		http_response_code($success);
	}
	else
	{
		http_response_code($noContent);
	}
}
else
{
	http_response_code($unauthorized);
}
?>

