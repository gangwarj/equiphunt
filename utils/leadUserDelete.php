<?php

include('config.php');
include('leadClass.php');
$leadClass = new leadClass();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    http_response_code($badRequest);
}

if(!isset($_SESSION['userId']) || empty($_SESSION['userId'])){
    session_destroy();
    http_response_code($session_error);
}

if(($_SESSION['majorRole']=='manager')||($_SESSION['majorRole']=='admin'))
{
	$phone=$_POST['phone'];
	
	if(in_array("leads", $_SESSION['accessLevel']))$accessLevels=array("leads");
	else if(in_array("fulfillment", $_SESSION['accessLevel']))$accessLevels=array("fulfillment");
	else http_response_code($unauthorized);
	
	if(in_array("leads", $accessLevels)||in_array("fulfillment", $accessLevels))
	{
		if(($_SESSION['majorRole']=='manager'))$majorRole="operator";
		else if(($_SESSION['majorRole']=='admin'))$majorRole="manager";
		else http_response_code($unauthorized);

		$lid= $leadClass->leadUserDelete($phone);
		if($lid)
		{
			echo "success";
		    http_response_code($success);
		}
		else
		{
			echo "Failed";
			http_response_code($noContent);
		}
	}
		
	else
	{
		http_response_code($forbidden);
	}
		
}
else
{
	http_response_code($unauthorized);
}
?>

