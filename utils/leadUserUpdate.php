<?php

include('config.php');
include('leadClass.php');
$leadClass = new leadClass();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    http_response_code($badRequest);
}

if(!isset($_SESSION['userId']) || empty($_SESSION['userId'])){
    session_destroy();
    http_response_code($session_error);
}

if(($_SESSION['majorRole']=='manager')||($_SESSION['majorRole']=='admin'))
{
    $password=trim($_POST['password']);
    $email=trim($_POST['email']);
    $name=trim($_POST['name']);
    $newPhone=trim($_POST['newphone']);
    $oldPhone=trim($_POST['oldphone']);
    echo $password.$newPhone.$oldPhone.$email;
    // $accessLevel=$_POST['accessLevel'];
    if(in_array("leads", $_SESSION['accessLevel']))$accessLevel=array("leads");
    else if(in_array("fulfillment", $_SESSION['accessLevel']))$accessLevel=array("fulfillment");
    else http_response_code($unauthorized);

    if(in_array("leads", $accessLevel)||in_array("fulfillment", $accessLevel))
    {
        $email_check = preg_match('~^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.([a-zA-Z]{2,4})$~i', $email);
        $password_check = preg_match('~^[A-Za-z0-9!@#$%^&*()_]{6,20}$~i', $password);
        $phone_check = preg_match('~^[0-9]{10}$~i', $newPhone);
        if($password=="")$password_check=1;
        if($email_check && $password_check && $phone_check) 
        {
            $lid= $leadClass->leadUserUpdate($password,$email,$name,$newPhone,$oldPhone);
            if($lid)
            {
                http_response_code($success);
            }
            else
            {
                http_response_code($noContent);
            }
        }
        else
        {
            echo "hello";
         // http_response_code($forbidden);
        }
        
    }
    else
    {
        http_response_code($unauthorized);
    }
}
else
{
    http_response_code($unauthorized);
}
?>

