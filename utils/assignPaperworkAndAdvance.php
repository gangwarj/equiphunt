<?php

include('config.php');
include('leadClass.php');
$leadClass = new leadClass();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    http_response_code($badRequest);
}

if(!isset($_SESSION['userId']) || empty($_SESSION['userId'])){
    session_destroy();
    http_response_code($session_error);
}


if((($_SESSION['majorRole']=='manager')&&(in_array("leads",$_SESSION['accessLevel'])))||(($_SESSION['majorRole']=='manager')&&(in_array("leads",$_SESSION['accessLevel'])))
{
    
    $leadEquipId=$_POST['leadId'];
    
    $lid= $leadClass->assignPaperworkAndAdvance($leadId);
    if($lid)
    {
    	http_response_code($success);
    }
    else
    {
         http_response_code($forbidden);
    }

}
else
{
    http_response_code($unauthorized);
}   

?>

