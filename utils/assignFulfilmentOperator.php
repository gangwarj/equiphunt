<?php

include('config.php');
include('leadClass.php');
$leadClass = new leadClass();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    http_response_code($badRequest);
}

if(!isset($_SESSION['userId']) || empty($_SESSION['userId'])){
    session_destroy();
    http_response_code($session_error);
}

if(($_SESSION['majorRole']=='manager')&&(in_array("fulfillment",$_SESSION['accessLevel'])))
{
    
    $phone=$_POST['phone'];
    $leadEquipId=$_POST['id'];
    
    $lid= $leadClass->assignFulfilmentOperator($leadEquipId,$phone);
    if($lid)
    {
        
        http_response_code($success);
    }
    else{
        // echo "YOYOYO";
        http_response_code($forbidden);
    }
}
else
{
	http_response_code($unauthorized);
}
    

?>

