<?php

include('config.php');
include('config.php');
include('leadClass.php');
$leadClass = new leadClass();

$data=$_POST['data'];
$data=json_decode($data);

if($data["status"]=="getAllLeads")
{
	echo json_encode($leadClass->getAllLeads());
}
if($data["status"]=="getMyLeads")
{
	echo json_encode($leadClass->getMyLeads());
}
else if($data["status"]=="getMyOperators")
{
 	if($_SESSION['userId']=="manager")
 	{
 		echo json_encode($leadClass->getMyOperators());
 	}
 	else
 	{
 		echo "No permissions";
 	}
}
else if($data["status"]=="getLeadsMovedToOrders")
{
	echo json_encode($leadClass->getLeadsMovedToOrders());
}
else if($data["status"]=="getLeadDetails")
{
	$leadId=$data["leadId"];
	echo json_encode($leadClass->getLeadDetails($leadId));
}
?>

