<?php

include('config.php');
include('leadClass.php');
$leadClass = new leadClass();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    http_response_code($badRequest);
}

if(!isset($_SESSION['userId']) || empty($_SESSION['userId'])){
    session_destroy();
    http_response_code($session_error);
}


if((($_SESSION['majorRole']=='operator')||($_SESSION['majorRole']=='manager'))&&(in_array("leads",$_SESSION['accessLevel']))){
    
    if($_SESSION['majorRole']=='operator')
    {
        $LeadOperatorId=$_SESSION['userId'];
        $leadManagerId=$leadClass->getMyManager();
    }
    if($_SESSION['majorRole']=='manager')
    {
        $leadManagerId=$_SESSION['userId'];
        $LeadOperatorId=0;
    }
    $name=trim($_POST['name']);
    $phone=trim($_POST['phone']);
    $tableName="renters";
    $leadType="inventory";
    $leadPriority=trim($_POST['leadPriority']);
    $leadDate=trim($_POST['leadDate']);
    // $raw_date = '10/25/2014 14:00 PM';
    $new_date = DateTime::createFromFormat('m/d/Y H:i A', $leadDate);
    $leadDate=$new_date->format('Y-m-d H:i:s'); 
    $sources=trim($_POST['leadSource']);
    $typeOfWork=trim($_POST['typeOfWork']);
    // echo $leadClass->getSourcesID($sources);
    $equipments=$_POST['equipments'];
    $lid= $leadClass->leadAdd($name,$phone,$leadManagerId,$LeadOperatorId,$tableName,$leadPriority,$leadDate,$leadType,$sources,$typeOfWork);
    if($lid)
    {
        // echo var_dump($equipments);
        foreach ($equipments as $equipment)
        {
            // echo "hi";
            $equipmentId=trim($equipment['equipName']);
            $leadId=$lid;
            $makeId=trim($equipment['make']);
            $location=trim($equipment['jobLocation']);
            $modelId=trim($equipment['model']);
            $district=trim($equipment['district']);
            $shiftTypeDay=$equipment['shiftTypeDay'];
            $projectStage=trim($equipment['stage']);
            $shiftTypeNight=$equipment['shiftTypeNight'];
            $expectedStartDate=trim($equipment['startDate']);
            $new_date = DateTime::createFromFormat('m/d/Y H:i A', $expectedStartDate);
            $expectedStartDate=$new_date->format('Y-m-d H:i:s'); 
            if($equipment['operatorAllowance']=="")$operatorAllowance=0;
            else $operatorAllowance=1;
            $year=$equipment['year'];
            if($equipment['food']=="")$food=0;
            else $food=1;
            $quantity=$equipment['quantity'];
            if($equipment['accomodation']=="")$accomodation=0;
            else $accomodation=1;
            $operationalHoursInADay=$equipment['operationHoursPerDay'];
            $operationalDaysInAMonth=$equipment['operationDaysPerMonth'];
            $operationalHoursInAMonth=$equipment['operationHoursPerMonth'];
            if($equipment['vehicleDocuments']=="")$vehicleDocuments=0;
            else $vehicleDocuments=1;
            if($equipment['operatorLicense']=="")$operatorLicense=0;
            else $operatorLicense=1;
            $capacity=$equipment['capacity'];
            $leid= $leadClass->leadEquipAdd($shiftTypeDay,$shiftTypeNight,$equipmentId,$leadId,$makeId,$location,$modelId,$district,$expectedStartDate,$operatorAllowance,$year,$food,$accomodation,$operationalHoursInADay,$operationalDaysInAMonth,$vehicleDocuments,$operatorLicense,$capacity,$operationalHoursInAMonth,$projectStage,$quantity);
            echo "yeah";
            if($leid)
            {
                http_response_code($success);
            }
            else{
                http_response_code($forbidden);
            }
        }
    }
    else
    {
        // echo "hi";
        http_response_code($forbidden);
    }

}
else
{
    http_response_code($unauthorized);
}
    

?>

