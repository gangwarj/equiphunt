<?php

include('config.php');
include('leadClass.php');
$leadClass = new leadClass();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    http_response_code($badRequest);
}

if(!isset($_SESSION['userId']) || empty($_SESSION['userId'])){
    session_destroy();
    http_response_code($session_error);
}


if((($_SESSION['majorRole']=='operator')||($_SESSION['majorRole']=='manager'))&&(in_array("leads",$_SESSION['accessLevel']))){
    // echo "hello";
    if($_SESSION['majorRole']=='operator')
    {
        $LeadOperatorId=$_SESSION['userId'];
        $leadManagerId=$leadClass->getMyManager();
    }
    if($_SESSION['majorRole']=='manager')
    {
        $leadManagerId=$_SESSION['userId'];
        $LeadOperatorId=0;
    }
    $leadId=$_POST['id'];
    $name=$_POST['name'];
    $phone=$_POST['phone'];
    $tableName="renters";
    $leadType="inventory";
    $leadPriority=$_POST['leadPriority'];
    // $leadDate=$_POST['leadDate'];
    $leadDate=trim($_POST['leadDate']);
    // echo $leadDate;
    // // $raw_date = '10/25/2014 14:00 PM';
    $new_date = DateTime::createFromFormat('m/d/Y H:i A', $leadDate);
    $leadDate=$new_date->format('Y-m-d H:i:s');
    $sources=$_POST['leadSource'];
    $typeOfWork=$_POST['typeOfWork'];
    // echo $leadClass->getSourcesID($sources);
    $equipments=$_POST['equipments'];
    $lid= $leadClass->leadUpdate($leadId,$name,$phone,$leadManagerId,$fulfilmentManagerId,$LeadOperatorId,$tableName,$leadPriority,$leadDate,$leadType,$sources,$typeOfWork);
    $leid1= $leadClass->leadEquipDelete($leadId);
    if($lid)
    {
        foreach ($equipments as $equipment)
        {
            $equipmentId=$equipment['equipName'];
            $leadIdDeactivate=0;
            $makeId=$equipment['make'];
            $location=$equipment['jobLocation'];
            $modelId=$equipment['model'];
            $district=$equipment['district'];
            $shiftTypeDay=$equipment['shiftTypeDay'];
            $projectStage=$equipment['stage'];
            $shiftTypeNight=$equipment['shiftTypeNight'];
            $expectedStartDate=$equipment['startDate'];
            $new_date = DateTime::createFromFormat('m/d/Y H:i A', $expectedStartDate);
            $expectedStartDate=$new_date->format('Y-m-d H:i:s');
            if($equipment['operatorAllowance']=="")$operatorAllowance=0;
            else $operatorAllowance=1;
            $year=$equipment['year'];
            if($equipment['food']=="")$food=0;
            else $food=1;
            $quantity=$equipment['quantity'];
            if($equipment['accomodation']=="")$accomodation=0;
            else $accomodation=1;
            $operationalHoursInADay=$equipment['operationHoursPerDay'];
            $operationalDaysInAMonth=$equipment['operationDaysPerMonth'];
            $operationalHoursInAMonth=$equipment['operationHoursPerMonth'];
            if($equipment['vehicleDocuments']=="")$vehicleDocuments=0;
            else $vehicleDocuments=1;
            if($equipment['operatorLicense']=="")$operatorLicense=0;
            else $operatorLicense=1;
            $capacity=$equipment['capacity'];
            $leid2= $leadClass->leadEquipAdd($shiftTypeDay,$shiftTypeNight,$equipmentId,$leadId,$makeId,$location,$modelId,$district,$expectedStartDate,$operatorAllowance,$year,$food,$accomodation,$operationalHoursInADay,$operationalDaysInAMonth,$vehicleDocuments,$operatorLicense,$capacity,$operationalHoursInAMonth,$projectStage,$quantity);
            if($leid2)
            {
                // echo "yoyo";
                http_response_code($success);
            }
            else{
                http_response_code($forbidden);
            }
        }
    }
    else
    {
        http_response_code($forbidden);
    }

}
else
{
    http_response_code($unauthorized);
}
    

?>

