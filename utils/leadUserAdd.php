<?php

include('config.php');
include('leadClass.php');
$leadClass = new leadClass();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    http_response_code($badRequest);
}

if(!isset($_SESSION['userId']) || empty($_SESSION['userId'])){
    session_destroy();
    http_response_code($session_error);
}

if(($_SESSION['majorRole']=='manager')||($_SESSION['majorRole']=='admin'))
{
    $majorRole=trim($_POST['majorRole']);
	$password=trim($_POST['password']);
	$email=trim($_POST['email']);
	$name=trim($_POST['name']);
	$phone=trim($_POST['phone']);
	$accessLevel=trim($_POST['accessLevel']);
	
	if(in_array($accessLevel, $_SESSION['accessLevel']))
	{
		$accessLevels=array($accessLevel);
	}
	else 
	{
		http_response_code($forbidden);
	}
	// echo $accessLevel;
	echo var_dump($accessLevels);
	if(in_array("leads", $accessLevels)||in_array("fulfillment", $accessLevels))
	{
		$email_check = preg_match('~^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.([a-zA-Z]{2,4})$~i', $email);
	    $password_check = preg_match('~^[A-Za-z0-9!@#$%^&*()_]{6,20}$~i', $password);
	    $phone_check = preg_match('~^[0-9]{10}$~i', $phone);
	    if($email_check && $password_check && strlen(trim($name))>0 && $phone_check) 
	    {
			if(($_SESSION['majorRole']=='manager'))$majorRole="operator";
			else if(($_SESSION['majorRole']=='admin'))$majorRole="manager";
			else http_response_code($unauthorized);

			$lid= $leadClass->leadUserAdd($password,$email,$name,$phone,$accessLevels,$majorRole);
			if($lid)
			{
				echo "success";
			    http_response_code($success);
			}
			else
			{
				echo "Failed";
				http_response_code($noContent);
			}
		}
		else
		{
			http_response_code($forbidden);
		}
		
	}
	else
	{
		http_response_code($unauthorized);
	}
}
else
{
	http_response_code($unauthorized);
}
?>

