<?php

include('config.php');
include('leadClass.php');
$leadClass = new leadClass();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    http_response_code($badRequest);
}

if(!isset($_SESSION['userId']) || empty($_SESSION['userId'])){
    session_destroy();
    http_response_code($session_error);
}

if(($_SESSION['majorRole']=='operator')&&(in_array("fulfillment",$_SESSION['accessLevel'])))
{
    
    $priceTag=$_POST['priceTag'];
    $leadEquipId=$_POST['id'];
    
    $lid= $leadClass->assignPriceTag($leadEquipId,$priceTag);
    if($lid)
    {
    	echo "Hello";
        http_response_code($success);
        
    }
    else
    {
        http_response_code($forbidden);
    }
}
else
{
    http_response_code($unauthorized);
}

?>

