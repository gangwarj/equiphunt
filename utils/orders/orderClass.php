<?php
 /*Functions to make:
	newOrder
	RemoveOrder
	modifyOrder
	getAllOrders
	getOrdersByCustID
	getOrdersByUserID
	getActiveOrders

 */ 
 class orderClass
 {
	
	public function newOrder($orderManagerId, $orderStartDate, $leadId, $customerId, $tableName, $renterRating,  $createdBy)
	{
		try{
			$db=getDB();    
			$st = $db->prepare("SELECT id FROM orders WHERE leadId=:leadId");  
			$st->bindParam("leadId", $leadId,PDO::PARAM_INT);
			$st->execute();
		   	$count=$st->rowCount();
			if($count<1)
			{
				$stmt = $db->prepare("INSERT INTO orders(orderManagerId, orderStartDate,  leadId,  customerId,  tableName,  renterRating, isActive, createdOn,  createdBy,  lastUpdatedOn,  lastUpdatedBy,  deletedOn,  deletedBy) VALUES (:orderManagerId, :orderStartDate, :leadId, :customerId, :tableName, :renterRating, :isActive, :createdOn, :createdBy, :lastUpdatedOn, :lastUpdatedBy, :deletedOn, :deletedBy)");  
				$isActive=1;
				$delete='';
				$time = date('Y-m-d H:i:s',time());
				$stmt->bindParam("orderManagerId", $orderManagerId,PDO::PARAM_INT) ;
				$stmt->bindParam("orderStartDate", $orderStartDate,PDO::PARAM_STR) ;
				$stmt->bindParam("leadId", $leadId,PDO::PARAM_INT) ;
				$stmt->bindParam("customerId", $customerId,PDO::PARAM_INT) ;
				$stmt->bindParam("tableName", $tableName,PDO::PARAM_STR) ;
				$stmt->bindParam("renterRating", $renterRating,PDO::PARAM_STR) ;
				$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL) ;
				$stmt->bindParam("createdOn", $time,PDO::PARAM_STR) ;
				$stmt->bindParam("createdBy", $createdBy,PDO::PARAM_INT) ;
				$stmt->bindParam("lastUpdatedOn", $time,PDO::PARAM_STR) ;
				$stmt->bindParam("lastUpdatedBy", $createdBy,PDO::PARAM_INT) ;
				$stmt->bindParam("deletedOn", $delete,PDO::PARAM_STR) ;
				$stmt->bindParam("deletedBy", $delete,PDO::PARAM_INT) ;
				$stmt->execute();
				$db = null;
				return true;
			}
			else
			{
			  $db=null;
			  return false;
			}
		   }
		   catch(PDOException $e) 
		   {
			echo '{"error":{"text":'. $e->getMessage() .'}}'; 
		   } 
	}

	public function newOrderEquipment($orderId,$orderOperatorId,$fieldOperatorId,$duration,$siteLocation,$equipmentId,$makeId,$modelId,$district,$shiftType,$expectedStartDate,$projectStage,$operatorAllowance,$batha,$food,$accommodation,$operationalHoursInADay,$operationalDaysInAMonth,$vehicleDocuments,$operatorLicense)
	{
		try
		{
			$stmt = $db->prepare("INSERT INTO orders(orderId, orderOperatorId, fieldOperatorId, duration, siteLocation, equipmentId, makeId, modelId, district, shiftType, expectedStartDate, projectStage, operatorAllowance, batha, food, accommodation, operationalHoursInADay, operationalDaysInAMonth, vehicleDocuments, operatorLicense) VALUES (:orderId,:orderOperatorId,:fieldOperatorId,:duration,:siteLocation,:equipmentId,:makeId,:modelId,:district,:shiftType,:expectedStartDate,:projectStage,:operatorAllowance,:batha,:food,:accommodation,:operationalHoursInADay,:operationalDaysInAMonth,:vehicleDocuments,:operatorLicense)");  
				$stmt->bindParam("orderId", $orderId,PDO::PARAM_INT) ;
				$stmt->bindParam("orderOperatorId", $orderOperatorId,PDO::PARAM_INT) ;
				$stmt->bindParam("fieldOperatorId", $fieldOperatorId,PDO::PARAM_INT) ;
				$stmt->bindParam("duration", $duration,PDO::PARAM_STR) ;
				$stmt->bindParam("siteLocation", $siteLocation,PDO::PARAM_STR) ;
				$stmt->bindParam("equipmentId", $equipmentId,PDO::PARAM_INT) ;
				$stmt->bindParam("makeId", $makeId,PDO::PARAM_INT) ;
				$stmt->bindParam("modelId", $modelId,PDO::PARAM_INT) ;
				$stmt->bindParam("district", $district,PDO::PARAM_STR) ;
				$stmt->bindParam("shiftType", $shiftType,PDO::PARAM_STR) ;
				$stmt->bindParam("expectedStartDate", $expectedStartDate,PDO::PARAM_STR) ;
				$stmt->bindParam("projectStage", $projectStage,PDO::PARAM_STR) ;
				$stmt->bindParam("operatorAllowance", $operatorAllowance,PDO::PARAM_STR) ;
				$stmt->bindParam("batha", $batha,PDO::PARAM_STR) ;
				$stmt->bindParam("food", $food,PDO::PARAM_BOOL) ;
				$stmt->bindParam("accommodation", $accommodation,PDO::PARAM_BOOL) ;
				$stmt->bindParam("operationalHoursInADay", $operationalHoursInADay,PDO::PARAM_STR) ;
				$stmt->bindParam("operationalDaysInAMonth", $operationalDaysInAMonth,PDO::PARAM_STR) ;
				$stmt->bindParam("vehicleDocuments", $vehicleDocuments,PDO::PARAM_STR) ;
				$stmt->bindParam("operatorLicense", $operatorLicense,PDO::PARAM_STR) ;
				$stmt->execute();
				$db = null;
				return true;
		}
		catch(PDOException $e) 
		   {
			echo '{"error":{"text":'. $e->getMessage() .'}}'; 
		   }
	}

	public function getInsertedOrderId($leadId)
	{
		try 
		{
			$db = getDB();
			$stmt = $db->prepare("SELECT id FROM orders WHERE leadId=:leadId");
			$stmt->bindParam("leadId", $leadId,PDO::PARAM_INT);  
			$stmt->execute();
			$data=$stmt->fetch(PDO::FETCH_OBJ);
			if($data)
				return $data->id;
			else
				return false;
		} 	

		catch(PDOException $e) 
		   {
			echo '{"error":{"text":'. $e->getMessage() .'}}'; 
		   }
	}

	 public function deleteOrder($id)
	 {
		try{

			$db = getDB();
			$stmt = $db->prepare("SELECT * FROM orders WHERE id=:id");
			$stmt->bindParam("id", $id,PDO::PARAM_INT);  
			$stmt->execute();
			$data=$stmt->fetch(PDO::FETCH_OBJ);
			if($data)
			{
				$isActive=0;
				$time= date('Y-m-d H:i:s',time());
				$st=$db->prepare("UPDATE orders SET isActive=:isActive,deletedBy=:deletedBy,deletedOn=:deletedOn WHERE id=:id");
				$st->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
				$st->bindParam("deletedOn",$time,PDO::PARAM_STR);
				$st->bindParam("deletedBy",$_SESSION['userid'],PDO::PARAM_INT);
				$st->bindParam("id", $id,PDO::PARAM_INT);  
				$st->execute();
				$db=null;
				return true;
			}
			else
			{
				$db=null;
				   return false;
			}     
		}
		  catch(PDOException $e) {
		  echo '{"error":{"text":'. $e->getMessage() .'}}'; 
		  }   
	 }

	/*public function orderUpdate($orderStartDate, $orderExecutiveId, $orderDuration, $siteLocation, $leadId, $customerId, $tableName, $renterRating, $leadPriority, $leadDate, $leadType, $jobDuration, $equipmentType, $make, $model, $year, $capacity, $jobLocation, $district, $shiftType, $expectedStartDate, $projectStage, $operatorAllowance, $batha, $food, $accomodation, $operationalHoursInADay, $operationalDaysInAMonth, $vehicleDocuments, $operatorLicense, $isActive, $createdOn, $createdBy, $lastUpdatedOn, $lastUpdatedBy, $deletedOn, $deletedBy)
	 {
		  try{
		  $db = getDB();
		  $st = $db->prepare("SELECT orderExecutiveId FROM orders WHERE orderExecutiveId=:orderExecutiveId");  
		  $st->bindParam("orderExecutiveId", $orderExecutiveId,PDO::PARAM_STR);
		  $st->execute();
		  $count=$st->rowCount();
		  if($count<1)
		  {
					$stmt = $db->prepare("UPDATE orders SET orderStartDate=:orderStartDate,orderDuration=:orderDuration,siteLocation=:siteLocation,leadId=:leadId,customerId=:customerId,tableName=:tableName,renterRating=:renterRating,leadPriority=:leadPriority,leadDate=:leadDate,leadType=:leadType,jobDuration=:jobDuration,equipmentType=:equipmentType,make=:make,model=:model,year=:year,capacity=:capacity,jobLocation=:jobLocation,district=:district,shiftType=:shiftType,expectedStartDate=:expectedStartDate,projectStage=:projectStage,operatorAllowance=:operatorAllowance,batha=:batha,food=:food,accomodation=:accomodation,operationalHoursInADay=:operationalHoursInADay,operationalDaysInAMonth=:operationalDaysInAMonth,vehicleDocuments=:vehicleDocuments,operatorLicense=:operatorLicense,isActive=:isActive,createdOn=:createdOn,createdBy=:createdBy,lastUpdatedOn=:lastUpdatedOn,lastUpdatedBy=:lastUpdatedBy,deletedOn=:deletedOn,deletedBy=:deletedBy WHERE orderExecutiveId=:orderExecutiveId");  
					
					$time = date('Y-m-d H:i:s',time());
					
					$stmt->bindParam("orderStartDate", $orderStartDate,PDO::PARAM_STR) ;
					//$stmt->bindParam("orderExecutiveId", $orderExecutiveId,PDO::PARAM_INT) ;
					$stmt->bindParam("orderDuration", $orderDuration,PDO::PARAM_STR) ;
					$stmt->bindParam("siteLocation", $siteLocation,PDO::PARAM_STR) ;
					$stmt->bindParam("leadId", $leadId,PDO::PARAM_INT) ;
					$stmt->bindParam("customerId", $customerId,PDO::PARAM_INT) ;
					$stmt->bindParam("tableName", $tableName,PDO::PARAM_STR) ;
					$stmt->bindParam("renterRating", $renterRating,PDO::PARAM_STR) ;
					$stmt->bindParam("leadPriority", $leadPriority,PDO::PARAM_STR) ;
					$stmt->bindParam("leadDate", $leadDate,PDO::PARAM_STR) ;
					$stmt->bindParam("leadType", $leadType,PDO::PARAM_STR) ;
					$stmt->bindParam("jobDuration", $jobDuration,PDO::PARAM_STR) ;
					$stmt->bindParam("equipmentType", $equipmentType,PDO::PARAM_STR) ;
					$stmt->bindParam("make", $make,PDO::PARAM_STR) ;
					$stmt->bindParam("model", $model,PDO::PARAM_STR) ;
					$stmt->bindParam("year", $year,PDO::PARAM_STR) ;
					$stmt->bindParam("capacity", $capacity,PDO::PARAM_STR) ;
					$stmt->bindParam("jobLocation", $jobLocation,PDO::PARAM_STR) ;
					$stmt->bindParam("district", $district,PDO::PARAM_STR) ;
					$stmt->bindParam("shiftType", $shiftType,PDO::PARAM_STR) ;
					$stmt->bindParam("expectedStartDate", $expectedStartDate,PDO::PARAM_STR) ;
					$stmt->bindParam("projectStage", $projectStage,PDO::PARAM_STR) ;
					$stmt->bindParam("operatorAllowance", $operatorAllowance,PDO::PARAM_BOOL) ;
					$stmt->bindParam("batha", $batha,PDO::PARAM_STR) ;
					$stmt->bindParam("food", $food,PDO::PARAM_BOOL) ;
					$stmt->bindParam("accomodation", $accomodation,PDO::PARAM_BOOL) ;
					$stmt->bindParam("operationalHoursInADay", $operationalHoursInADay,PDO::PARAM_STR) ;
					$stmt->bindParam("operationalDaysInAMonth", $operationalDaysInAMonth,PDO::PARAM_STR) ;
					$stmt->bindParam("vehicleDocuments", $vehicleDocuments,PDO::PARAM_STR) ;
					$stmt->bindParam("operatorLicense", $operatorLicense,PDO::PARAM_STR) ;
					$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL) ;
					$stmt->bindParam("createdOn", $time,PDO::PARAM_STR) ;
					$stmt->bindParam("createdBy", $_SESSION['userid'],PDO::PARAM_INT) ;
					$stmt->bindParam("lastUpdatedOn", $time,PDO::PARAM_STR) ;
					$stmt->bindParam("lastUpdatedBy", $_SESSION['userid'],PDO::PARAM_INT) ;
					$stmt->bindParam("deletedOn", '',PDO::PARAM_STR) ;
					$stmt->bindParam("deletedBy",  0,PDO::PARAM_INT) ;
					$stmt->execute();
					$db = null;
					return true;
			}else{
				$db=null;
				return false;

			}
			}catch(PDOException $e){
				echo '{"error":{"text":'. $e->getMessage() .'}}'; 
			}

	}  
*/

	public function showOrders($uid)
	{

		try
		{
			$db = getDB();
			$stmt = $db->prepare("SELECT * FROM orders WHERE orderManagerId=:orderManagerId");
			$stmt->bindParam("orderManagerId", $uid,PDO::PARAM_INT);  
			$stmt->execute();
			$data=$stmt->fetch(PDO::FETCH_OBJ);
			return $data;
		}
		catch(PDOException $e)
		{
			echo '{"error":{"text":'. $e->getMessage() .'}}'; 
		}
	}

	public function showOrderEquipments($orderID,$orderManagerId)
	{
		try
		{
			$db = getDB();
			$st = $db->prepare("SELECT * FROM orders WHERE orderId=:orderId and orderManagerId=:orderManagerId");
			$st->bindParam("orderId", $orderID,PDO::PARAM_INT);
			$st->bindParam("orderManagerId", $orderManagerId,PDO::PARAM_INT);  
			$st->execute();
			$count=$st->rowCount();
			if($count>=1)
			{
				$stmt = $db->prepare("SELECT * FROM order_equipment WHERE orderId=:orderId");
				$stmt->bindParam("orderId", $orderID,PDO::PARAM_INT);  
				$stmt->execute();
				$data=$stmt->fetch(PDO::FETCH_OBJ);
				return data;
			}
			else
			{
				echo "Order Not Assinged to this manager";
			}
		}
		catch(PDOException $e)
		{
			echo '{"error":{"text":'. $e->getMessage() .'}}'; 
		}
	}

	public function showOrderEquipmentsFulfillment($orderID,$fulfilmentManagerId)
	{
		try
		{
			$db = getDB();
			$st = $db->prepare("SELECT * FROM orders O,leads L,order_equipment OE WHERE OE.orderId=:orderId and L.leadId=O.leadId and L.fulfilmentManagerId=:fulfilmentManagerId and O.orderId=OE.orderId");
			$st->bindParam("orderId", $orderID,PDO::PARAM_INT);
			$st->bindParam("fulfilmentManagerId", $fulfilmentManagerId,PDO::PARAM_INT);  
			$st->execute();
			$count=$st->rowCount();

			if($count>=1)
			{
				$data=$st->fetch(PDO::FETCH_OBJ);
				return data;
			}
			else
			{
				echo "Order Not Assinged to this manager";
			}
		}
		catch(PDOException $e)
		{
			echo '{"error":{"text":'. $e->getMessage() .'}}'; 
		}
	}

	public function showEquipmentDetails($orderEquipmentId)
	{
		try
		{
			$db = getDB();
			$stmt = $db->prepare("SELECT * FROM order_equipment WHERE id=:id");
			$stmt->bindParam("id", $orderEquipmentId,PDO::PARAM_INT);  
			$stmt->execute();
			$data=$stmt->fetch(PDO::FETCH_OBJ);
			return data;
		}
		catch(PDOException $e)
		{
			echo '{"error":{"text":'. $e->getMessage() .'}}'; 
		}
	}

	public function showOperatorEquipments($operatorID)
	{
		try
		{
			$db = getDB();
			$stmt = $db->prepare("SELECT * FROM order_equipment WHERE orderOperatorId=:orderOperatorId");
			$stmt->bindParam("orderOperatorId", $operatorID,PDO::PARAM_INT);  
			$stmt->execute();
			$data=$stmt->fetch(PDO::FETCH_OBJ);
			return data;
		}
		catch(PDOException $e)
		{
			echo '{"error":{"text":'. $e->getMessage() .'}}'; 
		}
	}

	public function showOrdersFulfillmentManager($fulfilmentManagerId)
	{
		try
		{
			$db = getDB();
			$stmt = $db->prepare("SELECT * FROM orders O,leads L WHERE O.leadId = L.leadId and L.fulfilmentManagerId=:fulfilmentManagerId");
			$stmt->bindParam("fulfilmentManagerId", $fulfilmentManagerId,PDO::PARAM_INT);  
			$stmt->execute();
			$data=$stmt->fetch(PDO::FETCH_OBJ);
			return data;
		}
		catch(PDOException $e)
		{
			echo '{"error":{"text":'. $e->getMessage() .'}}'; 
		}
	}


	public function showPersonalDetails($uid)
	{
		try
		{
			$db = getDB();
			$stmt = $db->prepare("SELECT * FROM staff_login where id=:id");
			$stmt->bindParam("id", $uid,PDO::PARAM_INT);  
			$stmt->execute();
			$data=$stmt->fetch(PDO::FETCH_OBJ);
			return data;
		}
		catch(PDOException $e)
		{
			echo '{"error":{"text":'. $e->getMessage() .'}}'; 
		}
	}


 }
 ?>