<?php

include('config.php');
include('leadClass.php');
$leadClass = new leadClass();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    http_response_code($badRequest);
}

if(!isset($_SESSION['userId']) || empty($_SESSION['userId'])){
    session_destroy();
    http_response_code($session_error);
}

if(($_SESSION['majorRole']=='manager')||($_SESSION['majorRole']=='admin'))
{
    $email=trim($_POST['email']);
	$name=trim($_POST['name']);
	$newPhone=trim($_POST['newphone']);
	$oldPhone=trim($_POST['oldphone']);
	$password=trim($_POST['password']);
	if(in_array("leads", $_SESSION['accessLevel']))$accessLevel=array("leads");
	else if(in_array("fulfillment", $_SESSION['accessLevel']))$accessLevel=array("fulfillment");
	else http_response_code($unauthorized);

	if(in_array("leads", $accessLevel)||in_array("fulfillment", $accessLevel))
	{
		$email_check = preg_match('~^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.([a-zA-Z]{2,4})$~i', $email);
	    $phone_check = preg_match('~^[0-9]{10}$~i', $newPhone);
	    if($email_check && $phone_check) 
	    {
			$lid= $leadClass->leadUserProfileUpdate($email,$name,$newPhone,$oldPhone,$password);
			if($lid)
			{
			    echo var_dump($success);
			}
			else
			{
			    echo var_dump($forbidden);
			}
		}
		else
		{
			http_response_code($forbidden);
		}
		
	}
	else
	{
		http_response_code($unauthorized);
	}
}
else
{
	http_response_code($unauthorized);
}
?>

