<?php

include('config.php');
$target_dir = "uploads/";

$uploadOk = 1;

$OriginalFilename = $FinalFilename = $_FILES['fileToUpload']['name'];
// rename file if it already exists by prefixing an incrementing number
$FileCounter = 1;
$filename = pathinfo($OriginalFilename, PATHINFO_FILENAME);
$extension =  pathinfo($OriginalFilename, PATHINFO_EXTENSION);
while (file_exists( 'uploads/'.$FinalFilename ))
    $FinalFilename = $filename . '_' . $FileCounter++ . '.' . $extension;



// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($extension != "jpg" && $extension != "png" && $extension != "jpeg"
&& $extension != "gif" && $extension != "pdf") {
    echo "Sorry, only JPG, JPEG, PNG , PDF & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
	$target_file = $target_dir . $FinalFilename;
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
    {    
        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
        $db=getDB();
        $st=$db->prepare("INSERT INTO attachments (tableName,url) VALUES (:tableName,:url)");
        $tableName="lead_equipment";
        $st->bindParam("tableName",$tableName,PDO::PARAM_STR);
        $st->bindParam("url",$target_file,PDO::PARAM_STR);
        $attachment_id=$db->LastInsertedId();
    }
    else 
    {
        echo "Sorry, there was an error uploading your file.";
    }
}

?>



