<?php
class userClass
{
	 /* User Login */
     public function userLogin($phone,$password)
     {
          try{
               $db = getDB();
               $accessSpecifier="leads";
               $hash_password= hash('sha256', $password);
               $stmt = $db->prepare("SELECT userId,majorRole FROM staff_login WHERE phone=:phone AND  password=:hash_password");
               $stmt->bindParam("phone", $phone,PDO::PARAM_STR) ;
               $stmt->bindParam("hash_password", $hash_password,PDO::PARAM_STR) ;
               $stmt->execute();
               $count=$stmt->rowCount();
               $data=$stmt->fetch(PDO::FETCH_OBJ);
               if($count==1)
               {
                    // session_start();
                    $stmt = $db->prepare("SELECT access FROM access_level INNER JOIN access ON access.accessLevel=access_level.id AND access.userId=:id");
                    $stmt->bindParam("id", $data->userId,PDO::PARAM_STR) ;
                    $stmt->execute();
                    $count=$stmt->rowCount();
                    $access=$stmt->fetchAll(PDO::FETCH_OBJ);
                    // $access=$access->accessLevel;
                    $db = null;
                    $accessLevelArray=array();
                    for($i=0;$i<count($access);$i++)
                    {
                         $accessData=($access[$i]);
                         array_push($accessLevelArray,$accessData->access);
                         
                    }

                    $_SESSION['userId']=$data->userId;
                    $_SESSION['majorRole']=$data->majorRole;
                    $_SESSION['accessLevel']=$accessLevelArray;
                    
                    // if(in_array($accessSpecifier,$accessLevelArray))
                    // {
                    //      $_SESSION['userId']=$data->userId;
                    //      $_SESSION['majorRole']=$data->majorRole;
                    //      $_SESSION['accessLevel']=$accessSpecifier;
                         
                    //      $key = array_search($accessSpecifier,$accessLevelArray);
                    //      if($key!==false){
                    //          unset($accessLevelArray[$key]);
                    //      }
                    //      $accessLevelDomains=array_values($accessLevelArray);
                    //      $_SESSION['accessType']=$accessLevelDomains;
                    //      // echo var_dump($accessLevelDomains);
                    // }
                    // else
                    // {
                    //      return false;
                    // }
                    // session_write_close();
                    // echo var_dump($_SESSION['accessLevel']);
                    return true;
               }
               else
               {
                    return false;
               }
          } 
          catch(PDOException $e) {
               echo '{"error":{"text":'. $e->getMessage() .'}}'; 
          }
     }

     /* User Registration */
     public function userRegistration($password,$email,$name,$phone,$majorRole,$accessLevel)
     {
          try{
          $db = getDB();
          $st = $db->prepare("SELECT userId FROM staff_login WHERE phone=:phone OR email=:email");  
          $st->bindParam("phone", $phone,PDO::PARAM_STR);
          $st->bindParam("email", $email,PDO::PARAM_STR);
          $st->execute();
          $count=$st->rowCount();
          if($count<1)
          {
               if($_SESSION['majorRole']=='admin'){
                    $stmt = $db->prepare("INSERT INTO staff_login(password,email,name,phone,majorRole,accessLevel,adminId,isActive) VALUES (:hash_password,:email,:name,:phone,:majorRole,:accessLevel,:adminId,:isActive)");  
                    $hash_password= hash('sha256', $password);
                    $isActive=1;
                    $stmt->bindParam("hash_password", $hash_password,PDO::PARAM_STR) ;
                    $stmt->bindParam("email", $email,PDO::PARAM_STR) ;
                    $stmt->bindParam("name", $name,PDO::PARAM_STR) ;
                    $stmt->bindParam("phone", $phone,PDO::PARAM_STR) ;
                    $stmt->bindParam("majorRole", $majorRole,PDO::PARAM_STR) ;
                    $stmt->bindParam("accessLevel", $accessLevel,PDO::PARAM_STR) ;
                    $stmt->bindParam("adminId", $_SESSION['userId'],PDO::PARAM_INT) ;
                    $stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL) ;
                    $stmt->execute();
                    $db = null;
                    return "Successfully updated the User";
               }else{
                    $db = null;
                    return "UnAuthorized user";
               }
          }
          else
          {
          $db = null;
          return "Duplicate Phone/Email Try Again.";
          }
          
         
          } 
          catch(PDOException $e) {
          echo '{"error":{"text":'. $e->getMessage() .'}}'; 
          }
     }

     public function getLeadOperators()
     {
          $responseObj=new stdClass;
          try{

               $db = getDB();
               $isActive=1;
               $majorRole="manager";
               $accessLevel="leads";
               // $stmt = $db->prepare("SELECT * FROM staff_login WHERE isActive = :isActive AND majorRole=:majorRole");
               $stmt = $db->prepare("SELECT * FROM staff_login WHERE isActive = :isActive");
               $stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
               $stmt->execute();
               $data=$stmt->fetchAll(PDO::FETCH_OBJ);
               $count=$stmt->rowCount();
               $db = null;
               $responseArray=array();
               if($data)
               {
                    for($i=0;$i<count($data);$i++)
                    {
                         $userData=($data[$i]);
                         if($userData->majorRole=='manager')
                         {
                              array_push($responseArray, $userData);
                         }
                         
                    }
                    $responseData=$responseArray;
                    $status="Success";
                    $error="";
               }
               else
               {
                    $responseData="";
                    $status="No Records";
                    $error="";
               }    
          }
          catch(PDOException $e) {
               $responseData="";
               $status="Failed";
               $error=$e->getMessage();
          }
          $responseObj->data=$responseData;
          $responseObj->status=$status;
          $responseObj->error=$error;
          return $responseObj;
     }

     public function getAccessLevels($id){
          $responseObj=new stdClass;
          try{

               $db = getDB();
               $isActive=1;
               $majorRole="manager";
               $accessLevel="leads";
               $stmt = $db->prepare("SELECT access_level.access FROM staff_login WHERE isActive = :isActive AND majorRole=:majorRole");
               // $stmt = $db->prepare("SELECT * FROM staff_login WHERE isActive = :isActive");
               $stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
               $stmt->execute();
               $data=$stmt->fetchAll(PDO::FETCH_OBJ);
               // $count=$stmt->rowCount();
               $responseArray=array();
               if($data)
               {
                    for($i=0;$i<count($data);$i++)
                    {
                         $userData=($data[$i]);
                         if($userData->majorRole=='manager')
                         {
                              $stmt = $db->prepare("SELECT accessLevel FROM access WHERE userId=:id");
                              $stmt->bindParam("id", $userData->userId,PDO::PARAM_STR) ;
                              $stmt->execute();
                              $count=$stmt->rowCount();
                              $access=$stmt->fetch(PDO::FETCH_OBJ);
                              for($j=0;$j<count($access);$j++)
                              {

                              }
                              array_push($responseArray, $userData);
                         }
                         
                    }
                    $responseData=$responseArray;
                    $status="Success";
                    $error="";
               }
               else
               {
                    $responseData="";
                    $status="No Records";
                    $error="";
               }    
          }
          catch(PDOException $e) {
               $responseData="";
               $status="Failed";
               $error=$e->getMessage();
          }
          $db=null;
          $responseObj->data=$responseData;
          $responseObj->status=$status;
          $responseObj->error=$error;
          return $responseObj;

     }

      public function getLeadManagers()
     {
          $responseObj=new stdClass;
          try{

               $db = getDB();
               $isActive=1;
               $majorRole="manager";
               $accessLevel="leads";
               $stmt = $db->prepare("SELECT * FROM staff_login WHERE isActive = :isActive AND majorRole=:majorRole");
               // $stmt = $db->prepare("SELECT * FROM staff_login WHERE isActive = :isActive");
               $stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
               $stmt->execute();
               $data=$stmt->fetchAll(PDO::FETCH_OBJ);
               // $count=$stmt->rowCount();
               $responseArray=array();
               if($data)
               {
                    for($i=0;$i<count($data);$i++)
                    {
                         $userData=($data[$i]);
                         if($userData->majorRole=='manager')
                         {
                              $stmt = $db->prepare("SELECT accessLevel FROM access WHERE userId=:id");
                              $stmt->bindParam("id", $userData->userId,PDO::PARAM_STR) ;
                              $stmt->execute();
                              $count=$stmt->rowCount();
                              $access=$stmt->fetch(PDO::FETCH_OBJ);
                              for($j=0;$j<count($access);$j++)
                              {

                              }
                              array_push($responseArray, $userData);
                         }
                         
                    }
                    $responseData=$responseArray;
                    $status="Success";
                    $error="";
               }
               else
               {
                    $responseData="";
                    $status="No Records";
                    $error="";
               }    
          }
          catch(PDOException $e) {
               $responseData="";
               $status="Failed";
               $error=$e->getMessage();
          }
          $db=null;
          $responseObj->data=$responseData;
          $responseObj->status=$status;
          $responseObj->error=$error;
          return $responseObj;
     }
     public function userDelete($userId)
     {

          try{

               $db = getDB();
               $isActive=0;

               $stmt = $db->prepare("UPDATE staff_login SET isActive=:isActive WHERE userId=:userId");
               $stmt->bindParam("userId", $userId,PDO::PARAM_INT);
               $stmt->bindParam("isActive", $isActive,PDO::PARAM_INT);
               $stmt->execute();
               $db = null;
               return true; 
          }
           catch(PDOException $e) {
           echo '{"error":{"text":'. $e->getMessage() .'}}'; 
           }   
     }


      public function userUpdate($userId,$password,$email,$name,$phone,$majorRole,$accessLevel)
     {
          try{
          $db = getDB();
          $st = $db->prepare("SELECT userId FROM staff_login WHERE (phone=:phone OR email=:email) AND userId <> :userId");  
          $st->bindParam("phone", $phone,PDO::PARAM_STR);
          $st->bindParam("email", $email,PDO::PARAM_STR);
          $st->bindParam("userId", $userId,PDO::PARAM_STR);
          $st->execute();
          $count=$st->rowCount();
          $data=$st->fetch(PDO::FETCH_OBJ);
          if($count<1){
               if($_SESSION['majorRole']=='admin'){
                    $stmt = $db->prepare("UPDATE staff_login SET password=:hash_password,email=:email,name=:name,phone=:phone,majorRole=:majorRole,accessLevel=:accessLevel,adminId=:adminId WHERE userId=:userId");  
                    $hash_password= hash('sha256', $password);
                    $stmt->bindParam("hash_password", $hash_password,PDO::PARAM_STR) ;
                    $stmt->bindParam("email", $email,PDO::PARAM_STR) ;
                    $stmt->bindParam("name", $name,PDO::PARAM_STR) ;
                    $stmt->bindParam("phone", $phone,PDO::PARAM_STR) ;
                    $stmt->bindParam("majorRole", $majorRole,PDO::PARAM_STR) ;
                    $stmt->bindParam("accessLevel", $accessLevel,PDO::PARAM_STR) ;
                    $stmt->bindParam("adminId", $_SESSION['userId'],PDO::PARAM_INT) ;
                    $stmt->bindParam("userId", $userId,PDO::PARAM_INT) ;
                    $stmt->execute();
                    $db = null;
                    return "Successfully Updated the details";
               }
               else{
                    $db = null;
                    return "Unauthorised User";
               }         
          }
          else{
               return "Email or Phone exists";
          }      
          }
          catch(PDOException $e) {
               echo '{"error":{"text":'. $e->getMessage() .'}}'; 
          }
     }

}
?>