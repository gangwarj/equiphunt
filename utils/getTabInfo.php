<?php

include('config.php');
include('leadClass.php');
$leadClass = new leadClass();

$tab=$_POST['tab'];

$tabsArray=array("New","Approved By Renter","Price Card Sent","Quotation Sent","Renter Accept Quotation","Supplier Confirmed","Paperwork And Advance Done","Converted to Order");
if($tab=="New")
{
	$responseObj=($leadClass->getLeadsNotActivated());
	$responseObj->tab=$tab;
	if((in_array("leads", $_SESSION["accessLevel"]))&&(($_SESSION['majorRole']=='manager')||($_SESSION['majorRole']=='operator')))
	{
		echo json_encode($responseObj);
		http_response_code($success);
	}
	else
	{
		http_response_code($forbidden);
	}
}
if($tab=="Approved By Renter")
{
	$responseObj=($leadClass->getLeadsActivatedButPriceNotSent());
	$responseObj->tab=$tab;
    if((in_array("fulfillment", $_SESSION["accessLevel"])&&$_SESSION['majorRole']=='manager')||(in_array("leads", $_SESSION["accessLevel"])&&(($_SESSION['majorRole']=='manager')||$_SESSION['majorRole']=='operator')))
	{
		echo json_encode($responseObj);
		http_response_code($success);
	}
	else
	{
		http_response_code($forbidden);
	}
}
if($tab=="Price Card Sent")
{
	$responseObj=($leadClass->getLeadsPriceSentButQuotationNotSent());
	$responseObj->tab=$tab;
    if((in_array("fulfillment", $_SESSION["accessLevel"])&&$_SESSION['majorRole']=='manager')||(in_array("leads", $_SESSION["accessLevel"])&&(($_SESSION['majorRole']=='manager')||$_SESSION['majorRole']=='operator')))
	{
		echo json_encode($responseObj);
		http_response_code($success);
	}
	else
	{
		http_response_code($forbidden);
	}
}
if($tab=="Quotation Sent")
{
	$responseObj=($leadClass->getLeadsQuotationSentButNotAccepted());
	$responseObj->tab=$tab;
    if((in_array("leads", $_SESSION["accessLevel"]))&&(($_SESSION['majorRole']=='manager')||($_SESSION['majorRole']=='operator')))
	{
		echo json_encode($responseObj);
		http_response_code($success);
	}
	else
	{
		http_response_code($forbidden);
	}
}
if($tab=="Supplier Confirmed")
{
	$responseObj=$leadClass->getLeadsSupplierConfirmedButPaperworkNotDone();
	$responseObj->tab=$tab;
    if(($_SESSION["accessLevel"]=="manager")||(in_array("leads", $_SESSION["accessLevel"])||in_array("fulfillment", $_SESSION["accessLevel"])))
	{
		echo json_encode($responseObj);
		// echo "HIHIHIHI";
		http_response_code($success);
	}
	else
	{
		http_response_code($forbidden);
	}
}

if($tab=="Renter Accept Quotation")
{
	$responseObj=($leadClass->getLeadsQuotationAcceptedButSupplierNotConfirmed());
	$responseObj->tab=$tab;
    if(($_SESSION['majorRole']=='manager')&&(in_array("leads", $_SESSION['accessLevel'])||(in_array("fulfillment", $_SESSION['accessLevel']))))
	{
		echo json_encode($responseObj);
		http_response_code($success);
	}
	else
	{
		http_response_code($forbidden);
	}
}
if($tab=="Paperwork And Advance Done")
{
	$responseObj=($leadClass->getLeadsPaperworkDoneButNotConvertedToOrder());
	$responseObj->tab=$tab;
    if((in_array("fulfillment", $_SESSION["accessLevel"]))&&(($_SESSION['majorRole']=='manager')))
	{
		echo json_encode($responseObj);
		http_response_code($success);
	}
	else
	{
		http_response_code($forbidden);
	}
}
if($tab=="Converted to Order")
{
	$responseObj=($leadClass->getLeadsAcceptedButNotMovedToOrder());
	$responseObj->tab=$tab;
    if((in_array("leads", $_SESSION["accessLevel"]))&&(($_SESSION['majorRole']=='manager')))
	{
		echo json_encode($responseObj);
		http_response_code($success);
	}
	else
	{
		http_response_code($forbidden);
	}
}

?>

