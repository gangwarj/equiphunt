$(document).ready(function(){

	$('#create_user_btn').click(function() {
	   // event.preventDefault();
	   
	   var name = $('#newuser_name').val();
	   var email = $('#newuser_email').val();
	   var password = $('#newuser_pass').val();
	   var phone = $('#newuser_phone').val();
	   console.log(name);
		$.ajax({
			type: 'POST',
			url: '../utils/leadUserAdd.php',
			data: {email:email,password:password,name:name,phone:phone},
			success: function (data) {
				$("#add_user_modal").modal('hide');
				console.log(data);
				location.reload();
			},
			statusCode:{
				403: function() {
					alert('Email or Phone Exists');
				},
				401: function() {
					alert('Unauthorized User');
				} 
			}         
		});
	});

})