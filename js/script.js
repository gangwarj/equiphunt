  
    $(document.body).on('click','input[name="login"]', function(){
      var phone = $('input[name="phoneNumber"]').val();
      var password = $('input[name="password"]').val();
      
      if(phone==''||isNaN(phone)){
        alert('Please enter a valid Phone Number');
        return false;
      }else if(phone.length !== 10){
        alert('Please enter a 10 Digit Phone Number');
        return false;
      }else if(password==''){
        alert('Please enter Password');
        return false;
      }else{
        $.ajax({
          type: 'POST',
          url: 'utils/login.php',
          data: {phone:phone,password:password},
            success: function (data) {
              if(data=="success"){
                window.location.href='dashboard.html';  
              }else{
                alert(data);
              }
            }        
        });
      }
    });
    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    function checked(selected){
      var count = 0;
      $('input:checkbox').each(function() {
      var data = $(this).val();
      if($(this).is(':checked')){
      selected[data] = "true";
      count++;
      }else{
      selected[data] = "false";
      }
      });
      if(count>0){
        return selected;
        console.log(selected);
      }else{
        return false;
      }
    }
    $(document.body).on('click','input[name="register"]', function(){
      var name = $('input[name="name"]').val();
      var email = $('input[name="email"]').val();
      var phone = $('input[name="phoneno"]').val();
      var password = $('input[name="passwordreg"]').val();
      var role = $('#role').val();
      var selected = {};
      if(name==''){
        alert('Please Enter Name');
        return false;
      }else if(email==''){
        alert('Please enter Email');
        return false;
      }else if(!validateEmail(email)){
        alert('Enter Valid EmailId')
        return false;
      }if(phone==''||isNaN(phone)){
        alert('Please enter a valid Phone Number');
        return false;
      }else if(phone.length !== 10){
        alert('Please enter a 10 Digit Phone Number');
        return false;
      }else if(password=='' || password.length < 6){
        alert('Password Should contain atleast 6 Characters');
        return false;
      }else if(role=='0'){
        alert("Please Select Major Role");
        return false;
      }else if(!checked(selected)){
        alert('Check any one Access Level');
        return false;
      }else{
        $.ajax({
          type: 'POST',
          url: 'utils/register.php',
          data: {emailReg:email,passwordReg:password,nameReg:name,phone:phone,majorRole:role,accessLevel:selected},
            success: function (data) {
                $("#userModal").modal('hide');
                alert(data);
                location.reload();
            }        
        });
      }
      
    });