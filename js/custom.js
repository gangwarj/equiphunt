$(document).ready(function(){
	$('body').css('background','#fff')
	$('#leaddatepicker,#e_leaddatepicker,#startdatepicker,#e_startdatepicker').datetimepicker()
	// $('#leaddatepicker').datepicker({ dateFormat: 'yyyy-mm-dd' });
	$('.topbar_buttons .topbar_profile_btn').unbind('click').on('click',function(){
		$('.button_profile_container').slideToggle(500);
	})
	$('.topbar_buttons .topbar_notifications_btn').unbind('click').on('click',function(){
		$('.button_notifications_container').slideToggle(500);
	})
	$('.topbar_buttons .topbar_settings_btn').unbind('click').on('click',function(){
		$('.button_settings_container').slideToggle(500);
	})
	
	$('.dropdown-menu li').click(function(){
		$('button',$($(this).parent()).parent()).html($(this).text()+'&nbsp;<span class="caret"></span>')
	})

	$('#addleadModal .order_info .box-title').unbind('click').on('click',function(){
		// $('.order_info_form').slideToggle(500);
		$('.batha_detail').slideUp(500);
		degree=0;
		rotate()
		function rotate() {
        	$('.order_info .box-title p span').css({ WebkitTransform: 'rotate(' + degree + 'deg)'});  
	        $('.order_info .box-title p span',this).css({ '-moz-transform': 'rotate(' + degree + 'deg)'});                      
	        timer = setTimeout(function() {
	            ++degree; 
	            if(degree == 180)
	            	return;
	            rotate();
	        },1);
	    }

		if($('span',this).hasClass('glyphicon-plus-sign'))
			$('span',this).removeClass('glyphicon-plus-sign').addClass('glyphicon-minus-sign')
		else
			$('span',this).removeClass('glyphicon-minus-sign').addClass('glyphicon-plus-sign')
	})

	$('#editleadModal .order_info .box-title').unbind('click').on('click',function(){
		$('.order_info_form').slideToggle(500);
		$('.batha_detail').slideUp(500);
		degree=0;
		rotate()
		function rotate() {
        	$('.order_info .box-title p span').css({ WebkitTransform: 'rotate(' + degree + 'deg)'});  
	        $('.order_info .box-title p span',this).css({ '-moz-transform': 'rotate(' + degree + 'deg)'});                      
	        timer = setTimeout(function() {
	            ++degree; 
	            if(degree == 180)
	            	return;
	            rotate();
	        },1);
	    }

		if($('span',this).hasClass('glyphicon-plus-sign'))
			$('span',this).removeClass('glyphicon-plus-sign').addClass('glyphicon-minus-sign')
		else
			$('span',this).removeClass('glyphicon-minus-sign').addClass('glyphicon-plus-sign')

		$('')
	})

	$('.operatorallowancecheck input[name=operatorallowance]').click(function(){
		if($(this).val() == "yes")
			$('.batha_detail').slideDown(500);
		else
			$('.batha_detail').slideUp(500)			
	})

	$('.leaditem .sec3 .itemsub .leaddelete').click(function(){
		if(confirm("Are you sure you want to delete the selected Lead?"))
			$($($($(this).parent()).parent()).parent()).parent().remove()
	})

	$('.leaditem .item_check').unbind("click").click(function(){
		if($('input',this).prop('checked'))
			$($(this).parent()).parent().addClass('active')
		else
			$($(this).parent()).parent().removeClass('active')
	})

	$('.activity').each(function(i,obj){
		var height = parseInt($('.act2',obj).css('height'))
		$('.activity_line',obj).css('height',height+'px')
	})

	if($( "#slider-range" )){
		$( "#slider-range" ).slider({
	      range: true,
	      min: 0,
	      max: 10000000,
	      values: [ 100000, 5000000 ],
	      slide: function( event, ui ) {
	        $( "#amount" ).html( "Rs " + ui.values[ 0 ] + " - Rs " + ui.values[ 1 ] );
			$( "#amount1" ).val(ui.values[ 0 ]);
			$( "#amount2" ).val(ui.values[ 1 ]);
	      }
	    });
	    $('#amount1').val($( "#slider-range" ).slider( "values", 0 ))
	    $('#amount2').val($( "#slider-range" ).slider( "values", 1 ))
	    $( "#amount" ).html( "Rs " + $( "#slider-range" ).slider( "values", 0 ) +
	     " - Rs " + $( "#slider-range" ).slider( "values", 1 ) );
	}


    $('.filter_container input[name=filter]').click(function(){
    	console.log($(this).val())
    	var checkarr=[]
    	$('.filter_container input[name=filter]:checked').each(function(){
    		checkarr.push($(this).val())
    	})
    	// console.log(checkarr)

    	// $('.leaditems_container .leaditem').each(function(i,obj){
    	// 		// console.log($(obj),'1'+$('.sec1 .leadstatus',obj).html()+'1')
    	// 	if(checkarr.length != 0){
    	// 		if(checkarr.indexOf($('.sec1 .item_leadstatus',obj).html()) > -1){
	    // 			// console.log($(obj),'1'+$('.sec1 .item_leadstatus',obj).text()+'1')
	    // 			$(obj).css('display','inline-block')
	    // 		}
	    // 		else
	    // 			$(obj).css('display','none')
    	// 	}
  			// else
  			// 	$(obj).css('display','inline-block')	
    	// })

    	$('.leadtable tbody tr').each(function(i,obj){
    			// console.log($(obj),'1'+$('.sec1 .leadstatus',obj).html()+'1')
    		if(checkarr.length != 0){
    			if(checkarr.indexOf($('td:nth-child(7)',obj).html().trim()) > -1){
	    			// console.log($(obj),'1'+$('.sec1 .item_leadstatus',obj).text()+'1')
	    			$(obj).css('display','table-row')
	    		}
	    		else
	    			$(obj).css('display','none')
    		}
  			else
  				$(obj).css('display','table-row')	
    	})
    })


    $('.progressbar_container .stageitem').click(function(){
    	$('.stageitem').removeClass('selected')
    	$(this).addClass('selected')
    	$('.status_info p span').html($(this).text().trim())
    })

    $('.markstatus').click(function(){
    	var actr=0,sctr=0;
    	$('.stageitem').each(function(i,obj){
    		if($(obj).hasClass('selected')){
    			sctr=1
    			$(obj).removeClass('selected').addClass('active')
    		}
    		else if(sctr == 1){
    			$(obj).removeClass('active')
    			$(obj).removeClass('done')
    		}
    		else{
    			$(obj).removeClass('active').addClass('done')
    		}
    		
    	})    	
    })


    //CHAT SAVE
    $('.newchat_save').unbind('click').click(function(){
    	var price = $('#newchat_price').val(),
    		comment = $('#newchat_comment').val(),
    		name = $('.newchat_name').val(),
            subject = $('.newchat_subject').val()

    	// if(name == $('.profile_name').html().trim() || name == "")
			name = 'You'

		// console.log(subj)
		var m_names = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

		var d = new Date();
		var curr_date = d.getDate();
		var curr_month = d.getMonth();
		var curr_year = d.getFullYear();
		var date = curr_date + " " + m_names[curr_month] + ", " + curr_year;
        console.log(subject)
        if(subject =="CALL")
        {
            $('.chatcontainer .activity_container>p').after('<div class="activity newchat_comment"><div class="act1"><p class="activity_icon"><span class="glyphicon glyphicon-tags"></span></p><span class="activity_line" style="height: 64px;"></span></div><div class="act2"><p class="activity_subject">Price Tag</p><p class="activity_comment">'+comment+'</p></div><div class="act3"><p class="activity_time">'+date+'</p><p class="sender_name">'+name+'</p></div></div>')
        }else
        {
            $('.chatcontainer .activity_container>p').after('<div class="activity newevent_comment"><div class="act1"><p class="activity_icon"><span class="glyphicon glyphicon-tags"></span></p><span class="activity_line" style="height: 64px;"></span></div><div class="act2"><p class="activity_subject">Price Tag</p><p class="activity_comment">'+comment+'</p></div><div class="act3"><p class="activity_time">'+date+'</p><p class="sender_name">'+name+'</p></div></div>')
        }
		

    })

    //EVENT SAVE
    $('.newevent_save').unbind('click').click(function(){
        var subj = $('#newevent_subject').val(),
            comment = $('#newevent_comment').val(),
            name = $('.chat_name').val(),
            subject = $('.newevent_subject').val()

        // if(name == $('.profile_name').html().trim() || name == "")
            name = 'You'

        // console.log(subj)
        var m_names = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

        var d = new Date();
        var curr_date = d.getDate();
        var curr_month = d.getMonth();
        var curr_year = d.getFullYear();
        var date = curr_date + " " + m_names[curr_month] + ", " + curr_year;

        if(subject == "CALL")
        {
            $('.eventscontainer .activity_container>p').after('<div class="activity newchat_comment"><div class="act1"><p class="activity_icon"><span class="glyphicon glyphicon-tags"></span></p><span class="activity_line" style="height: 64px;"></span></div><div class="act2"><p class="activity_subject">'+ subj+'</p><p class="activity_comment">'+comment+'</p></div><div class="act3"><p class="activity_time">'+date+'</p><p class="sender_name">'+name+'</p></div></div>')
        }else
        {
            $('.eventscontainer .activity_container>p').after('<div class="activity newevent_comment"><div class="act1"><p class="activity_icon"><span class="glyphicon glyphicon-tags"></span></p><span class="activity_line" style="height: 64px;"></span></div><div class="act2"><p class="activity_subject">'+ subj+'</p><p class="activity_comment">'+comment+'</p></div><div class="act3"><p class="activity_time">'+date+'</p><p class="sender_name">'+name+'</p></div></div>')
        }
        

    })

    //MAIL SAVE
    $('.newmail_save').unbind('click').click(function(){
        var subj = $('#newmail_subject').val(),
            comment = $('#newmail_comment').val(),
            name = $('.chat_name').val(),
            subject = $('.newmail_subject').val()

        // if(name == $('.profile_name').html().trim() || name == "")
            name = 'You'

        // console.log(subj)
        var m_names = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

        var d = new Date();
        var curr_date = d.getDate();
        var curr_month = d.getMonth();
        var curr_year = d.getFullYear();
        var date = curr_date + " " + m_names[curr_month] + ", " + curr_year;

        if(subject=="CALL")
        {
            $('.sendmailcontainer .activity_container>p').after('<div class="activity newchat_comment"><div class="act1"><p class="activity_icon"><span class="glyphicon glyphicon-tags"></span></p><span class="activity_line" style="height: 64px;"></span></div><div class="act2"><p class="activity_subject">'+ subj+'</p><p class="activity_comment">'+comment+'</p></div><div class="act3"><p class="activity_time">'+date+'</p><p class="sender_name">'+name+'</p></div></div>')
        }else
        {
            $('.sendmailcontainer .activity_container>p').after('<div class="activity newevent_comment"><div class="act1"><p class="activity_icon"><span class="glyphicon glyphicon-tags"></span></p><span class="activity_line" style="height: 64px;"></span></div><div class="act2"><p class="activity_subject">'+ subj+'</p><p class="activity_comment">'+comment+'</p></div><div class="act3"><p class="activity_time">'+date+'</p><p class="sender_name">'+name+'</p></div></div>')
        }
        

    })


    //CHANGE TABS 
    $('.chat_container>p span').click(function(){
    	$('.chat_container>p span').removeClass('active')
    	$(this).addClass('active');
    	if($(this).hasClass('chattab')){
    		$('.sendmailcontainer').css('display','none');
            $('.eventscontainer').css('display','none');
    		$('.chatcontainer').css('display','block')
    	}
    	else if($(this).hasClass('eventstab'))
        {
    		$('.sendmailcontainer').css('display','none');
            $('.eventscontainer').css('display','block');
            $('.chatcontainer').css('display','none')
    	}else{
            $('.sendmailcontainer').css('display','block');
            $('.eventscontainer').css('display','none');
            $('.chatcontainer').css('display','none')

        }

    	$('.activity_container .activity').each(function(i,obj){
    		$('.act1 .activity_line',obj).css('height',$('.act2',obj).height() - 20)
    	})
    })

    $('.equipment_container .equipments .equipment').unbind('click').click(function(){

    	if($(this).hasClass('active'))
    		$(this).removeClass('active')
    	else
    		$(this).addClass('active')

    	if($('.equipment_container .equipments .equipment.active').length != 0)
    		$('.add_selectedbtn').css('display','block')
    	else
    		$('.add_selectedbtn').css('display','none')
    })

    $('.add_selectedbtn').click(function(){
    	$('.newbid_equipments div.bidequipment').remove()
    	$('.equipment_container .equipments .equipment.active').each(function(){
    		$('.newbid_equipments').append('<div class="bidequipment">'+$(this).html()+'</div>')
    	})
    })

    $('.newbid_save').click(function(){
    	var subj = $('.newbid_subject').val(),
    		comment = $('.newbid_comment').val(),
    		name = $('.newbid_name').val(),
    		bideq = $('.newbid_equipments>:not(:first-child)')

		if(name == $('.profile_name').html().trim() || name == "")
			name = 'You'
		// console.log(subj)
		var m_names = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

		var d = new Date();
		var curr_date = d.getDate();
		var curr_month = d.getMonth();
		var curr_year = d.getFullYear();
		var date = curr_date + " " + m_names[curr_month] + ", " + curr_year;

		var ele = '<div class="activity"><div class="act1"><p class="activity_icon"><span class="glyphicon glyphicon-tags"></span></p><span class="activity_line" style="height: 64px;"></span></div><div class="act2"><p class="activity_subject">'+subj+'</p><p class="activity_comment">'+comment+'<br>'
		$('.newbid_equipments>:not(:first-child)').each(function(i,obj){
			ele += '<div class="bidequipment" style="width:40%;">' + $(obj).html() + '</div>'
		})
		// console.log(ele)
		ele += '</p></div><div class="act3"><p class="activity_time">'+date+'</p><p class="sender_name">'+name+'</p></div></div>'
		
		$('.bidcontainer .activity_container>p').after(ele)

		$('.bidcontainer .activity_container .activity').each(function(i,obj){
    		$('.act1 .activity_line',obj).css('height',$('.act2',obj).height() - 20)
    	})
		
    })

    $('.eqfilter_search').click(function(){
    	var etype = $('.eqtype_filter button').text().trim(),
    		emake = $('.eqmake_filter button').text().trim(),
    		emodel = $('.eqmodel_filter button').text().trim(),
    		eage = $('.eqyear_filter button').text().trim(),
    		eloc = $('.eqlocation_filter input').val().toLowerCase(),
    		eprice1 = parseInt($( "#amount1" ).val()),
    		eprice2 = parseInt($( "#amount2" ).val())

    	if(etype == 'Equipment Type' || etype == 'None')
    		etype = ''
    	if(emake == 'Make' || emake == 'None')
    		emake = ''
    	if(emodel == 'Model' || emodel == 'None')
    		emodel = ''
    	if(eage == 'Year' || eage == 'None')
    		eage = ''
    	
    	$('.equipment_container .equipments .equipment').each(function(i,obj){
    		// console.log($('.equipment_make span:nth-child(2)',obj).html(),emake,$('.equipment_model span:nth-child(2)',obj).html(),emodel,$('.equipment_age span:nth-child(2)',obj).html(),eage)

    		var p = $('.equipment_price span:nth-child(2)',obj).html().split(" ")
    		var punit = p[2],price = parseInt(p[1])
    		if(punit == 'L' || punit == 'l')
    			price = price * 100000
    		console.log($('.equipment_location span:nth-child(2)',obj).html(),eloc)

    		if($('.equipment_name',obj).html().indexOf(etype) > -1 && $('.equipment_make span:nth-child(2)',obj).html().indexOf(emake) > -1 && $('.equipment_model span:nth-child(2)',obj).html().indexOf(emodel) > -1 && $('.equipment_age span:nth-child(2)',obj).html().indexOf(eage) > -1 && $('.equipment_location span:nth-child(2)',obj).html().toLowerCase().indexOf(eloc) > -1 && price >= eprice1 && price <= eprice2)
    			$(obj).css('display','inline-block')
    		else
    			$(obj).css('display','none')
    	})
    	
    })

    //Select all function in Lead Home
    $("#selectall").click(function() {
		$('.case').attr('checked', this.checked);
	});       

	$(".case").click(function() {
		if ($(".case").length == $(".case:checked").length) {
		    $("#selectall").attr("checked", "checked");
		}
		else {
		    $("#selectall").removeAttr("checked");
		}      
	});


	//Tab Changing
	$('.status_tabs li a').click(function(e) {
        e.preventDefault(); // prevent the default action
        e.stopPropagation(); // stop the click from bubbling

  //       console.log($(this).html().toUpperCase())
        var val = $(this).html().toUpperCase()
        $('.status_tabs li a').removeClass('active')
  //   	var check = 0
    	
  //       //make respective element active
		// $('.status_tabs li a').removeClass('active')
		$(this).addClass('active')

    	

    	// //changing content
     //    $('th:nth-child(12)').hide();
    	// $('.leadtable tbody tr').each(function(i,obj){
    	// 		// console.log($(obj),'1'+$('.sec1 .leadstatus',obj).html()+'1')
    			
     //            if(val == 'APPROVED BY RENTER')
     //            {
                    
     //                $('th:nth-child(12)').show();
     //            }
                
    	// 		if($('td:nth-child(7)',obj).html().trim() == val) {
	    // 			// console.log($(obj),'1'+$('.sec1 .item_leadstatus',obj).text()+'1')

	    // 			$(obj).css('display','table-row')

     //                if(val == 'APPROVED BY RENTER')
     //                {
     //                    $('td:nth-child(12)').show();
     //                    $('th:nth-child(12)').show();
     //                }else
     //                {
     //                    $('td:nth-child(12)').hide();
     //                }
	    			
	    // 		}
	    // 		else
	    // 		{
     //                $(obj).css('display','none')
                    
     //            }
    			
    	// })

        //for demo purpose only

        if($(this).html() == 'New'){
            $('#lead_new').css('display','none')
            $('#lead_approved_by_renter').css('display','block')
        }
        else{
            $('#lead_approved_by_renter').css('display','none')
            $('#lead_new').css('display','block')
        }
    });

    //Campaign Date pickers
    $('#campaignstartdatepicker,#campaignenddatepicker,#e_campaignstartdatepicker,#e_campaignenddatepicker').datetimepicker()

    $('#followupdate').datetimepicker()
    
    
    // $( "#leadfulldetailsbtn" ).click(function() {
    //     $( "#leadfulldetailscontainer" ).slideToggle();
    // });

    // $( "#orderdetailsbtn" ).click(function() {
    // $( "#orderdetailscontainer" ).slideToggle();
    // });

    $(document.body ).on('click', '.approvebtn', function(){
        var check = $(this).parent().attr("ishandled");
        if(check == "no"){
            $(this).parent().attr("ishandled", "yes");
            $( this ).addClass('approveselected');
            $('span', this ).removeClass('fa fa-thumbs-o-up');
            $('span',this ).addClass('fa fa-thumbs-up');
            
        }
        else{
            alert("action has already been taken on this lead");
        }

    });

    $(document.body).on('click','.disapprovebtn',function(){
        var check = $(this).parent().attr("ishandled");
        if(check == "no"){
            $(this).parent().attr("ishandled", "yes");
            $( this ).addClass('disapproveselected');
            $( 'span' ,this ).removeClass('fa fa-thumbs-o-up');
            $( 'span', this ).addClass('fa fa-thumbs-up');
            
        }
        else{
            alert("action has already been taken on this lead");
        }
    })

    $( ".headerlogo" ).click(function() {
        window.location='home.html'; 
    });

    $('#add_equipment_container_btn').click(function(){
        $('#add_equipment').slideToggle();
        // $('#add_equipment_edit').slideUp();
    });

    // $(document.body).on('click','.add_equipments_edit_btn',function(){
    //     $('#add_equipment').slideToggle();
    //     // $('#add_equipment').slideUp();
    // });

    
})



			

