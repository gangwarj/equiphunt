$(document).ready(function(){

	//Home redirection on logo click
	$( ".headerlogo" ).click(function() {
        window.location='home.html'; 
    });

	//dropdowns on topbar buttons
    $('.topbar_buttons .topbar_profile_btn').unbind('click').on('click',function(){
		$('.button_profile_container').slideToggle(500);
	})
	$('.topbar_buttons .topbar_notifications_btn').unbind('click').on('click',function(){
		$('.button_notifications_container').slideToggle(500);
	})
	$('.topbar_buttons .topbar_settings_btn').unbind('click').on('click',function(){
		$('.button_settings_container').slideToggle(500);
	})

	$('#delete_user').click(function(){
		var r = confirm("Are you sure?");
		if (r == true) {
		    alert('DELETED')
		} 
		

	})
})