$(document).ready(function(){
	$('body').css('background','#094b7d')
	$('.loginform').click(function(){
		$('.signup').css('display','none')
		$('.login').css('display','block')
		$('.signupform').removeClass('active')
		$(this).addClass('active')
	})
	$('.signupform').click(function(){
		$('.login').css('display','none')
		$('.signup').css('display','block')
		$('.loginform').removeClass('active')
		$(this).addClass('active')
	})

	
	$.ajax({
		type: 'POST',
		url: './utils/checkSessionNull.php',
		success: function (data) {	
			
		},
		statusCode:{
			403: function() {
				alert('already logged in');
				window.location="dashboard.html";
			},
			401: function() {
				// alert('Unauthorized User');
			} 
		}         
	});
	
	

	$('.loginbtn').click(function(){		
		var phone = $('.login_email').val();
		var password = $('.login_pass').val();
		// console.log(phone);
		// console.log(password);
		var phoneno = /^\d{10}$/;
		if(phone == '')
		{
			alert("Please enter phone no.."); 
		}  
		else if(!phone.match(phoneno))  
		{  
			alert("Please enter correct phone  number.");  
						
		}  
		else if( password == '') 
		{  
			alert("Please enter password.");  
			$('.login_pass').val('');		
		}else{
			$.ajax({
				type: 'POST',
				url: './utils/leadLogin.php',
				data: {phone:phone,password:password},
				success: function (data) {	
					console.log(data);				
					window.location="dashboard.html";
				},
				statusCode:{
					403: function() {
						alert('Invalid Credentials');
					},
					401: function() {
						alert('Unauthorized User');
					} 
				}         
			});
		}	
		
	})
})
