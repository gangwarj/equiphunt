<?php

include('config.php');
include('leadClass.php');
$leadClass = new leadClass();
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    http_response_code($badRequest);
}

if(!isset($_SESSION['userId']) || empty($_SESSION['userId'])){
    session_destroy();
    http_response_code($session_error);
}

if(((in_array("fulfillment",$_SESSION['accessLevel']))||(in_array("leads",$_SESSION['accessLevel'])))&&($_SESSION['majorRole']=='manager')){
    
    $id=$_POST['id'];
    $lid= $leadClass->leadDelete($id);
    if($lid){
    	http_response_code($success);
        echo "Success";
    }else{
        http_response_code($noContent);
        echo "Failed";
    }
}
    
    
?>

