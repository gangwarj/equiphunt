<?php
class leadClass{
	public function leadUserLogin($phone,$password)
     {
          try{
               $db = getDB();
               $accessSpecifier="leads";
               $hash_password= hash('sha256', $password);
               $stmt = $db->prepare("SELECT userId,majorRole FROM staff_login WHERE phone=:phone AND  password=:hash_password");
               $stmt->bindParam("phone", $phone,PDO::PARAM_STR) ;
               $stmt->bindParam("hash_password", $hash_password,PDO::PARAM_STR) ;
               $stmt->execute();
               $count=$stmt->rowCount();
               $data=$stmt->fetch(PDO::FETCH_OBJ);

               if($count==1)
               {
                    $stmt = $db->prepare("SELECT access FROM access_level INNER JOIN access ON access.accessLevel=access_level.id AND access.userId=:id");
                    $stmt->bindParam("id", $data->userId,PDO::PARAM_STR) ;
                    $stmt->execute();
                    $count=$stmt->rowCount();
                    $access=$stmt->fetchAll(PDO::FETCH_OBJ);
                    $db = null;
                    $accessLevelArray=array();
                    for($i=0;$i<count($access);$i++)
                    {
                         $accessData=($access[$i]);
                         array_push($accessLevelArray,$accessData->access);
                         
                    }
                    echo var_dump($accessLevelArray);
                    if(in_array("leads",$accessLevelArray)||in_array("fulfillment",$accessLevelArray))
                    {	
	                    $_SESSION['userId']=$data->userId;
	                    $_SESSION['majorRole']=$data->majorRole;
	                    $_SESSION['accessLevel']=$accessLevelArray;
	                    return true;
                    }
                    else
                    {
                    	return false;
                    }
               }
               else
               {
                    return false;
               }
          } 
          catch(PDOException $e) {
           http_response_code($GLOBALS['connection_error']);
           echo $e->getMessage(); 
          }
     }
	public function getLeadDetails($id)
     {
     	try{

	          $db = getDB();
	          $isActive=1;
	          $stmt = $db->prepare("SELECT * FROM leads WHERE id=:id AND isActive=:isActive");
	          $stmt->bindParam("id", $id,PDO::PARAM_BOOL);
	          $stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
	          $stmt->execute();
	          $data=$stmt->fetchAll(PDO::FETCH_OBJ);
	          $db = null;
	          if($data)
	          {
	          		http_response_code($GLOBALS['success']);
          			return $data;
	          }
	          else
	          {
	          		http_response_code($GLOBALS['noContent']);
          			return false;
	          } 	
     	}
	      catch(PDOException $e) {
	      	http_response_code($GLOBALS['connection_error']);
          	echo $e->getMessage(); 
	      }   
     }
	public function getAllLeads()
	 {
 		 $responseObj=new stdClass;
     	 try{

	          $db = getDB();
	          $isActive=1;
	          $stmt = $db->prepare("SELECT * FROM leads WHERE isActive=:isActive");
	          $stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
	          $stmt->execute();
	          $data=$stmt->fetchAll(PDO::FETCH_OBJ);
	          $db = null;
	          if($data)
	          {
	                http_response_code($GLOBALS['success']);
	                $responseObj->data=$data;
					$responseObj->status="Success";
	          }
	          else
	          {
	                http_response_code($GLOBALS['noContent']);
          			$responseObj->data=$data;
					$responseObj->status="Nothing Found";
	          } 	
     	}
	      catch(PDOException $e) {
	      			http_response_code($GLOBALS['connection_error']);
          			$responseObj->status=$e->getMessage();
	      }
	      return $responseObj;
     }
    public function getActivatedLeads()
     {
 		 $responseObj=new stdClass;
     	 try{

	          $db = getDB();
	          $isActive=1;
	          $activate=1;
	          $stmt = $db->prepare("SELECT * FROM leads WHERE isActive=:isActive AND activate=:activate");
	          $stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
	          $stmt->bindParam("activate", $activate,PDO::PARAM_BOOL);
	          $stmt->execute();
	          $data=$stmt->fetchAll(PDO::FETCH_OBJ);
	          $responseArr=array();
	          for($i=0;$i<count($data);$i++)
	          {
	          	$x=$data[i];
	          	if(in_array($x->leadType, $_SESSION['accessLevel']))
	          	{
	          		array_push($responseArr,$x->leadType);
	          	}
	          }
	          $db = null;
	          if($responseArr)
	          {
	                http_response_code($GLOBALS['success']);
	                $responseObj->data=$responseArr;
					$responseObj->status="Success";
	          }
	          else
	          {
	                http_response_code($GLOBALS['noContent']);
          			$responseObj->data=$data;
					$responseObj->status="Nothing Found";
	          } 	
     	}
	      catch(PDOException $e) {
	      			http_response_code($GLOBALS['connection_error']);
          			$responseObj->status=$e->getMessage();
	      }
	      return $responseObj;
     }
    public function getAcceptedLeads()
     {
 		 $responseObj=new stdClass;
     	 try{

	          $db = getDB();
	          $isActive=1;
	          $accept=1;
	          $stmt = $db->prepare("SELECT * FROM leads WHERE isActive=:isActive AND accept=:accept");
	          $stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
	          $stmt->bindParam("accept", $accept,PDO::PARAM_BOOL);
	          $stmt->execute();
	          $data=$stmt->fetchAll(PDO::FETCH_OBJ);
	          $responseArr=array();
	          for($i=0;$i<count($data);$i++)
	          {
	          	$x=$data[i];
	          	if(in_array($x->leadType, $_SESSION['accessLevel']))
	          	{
	          		array_push($responseArr,$x->leadType);
	          	}
	          }
	          $db = null;
	          if($responseArr)
	          {
	                http_response_code($GLOBALS['success']);
	                $responseObj->data=$responseArr;
					$responseObj->status="Success";
	          }
	          else
	          {
	                http_response_code($GLOBALS['noContent']);
          			$responseObj->data=$data;
					$responseObj->status="Nothing Found";
	          } 	
     	}
	      catch(PDOException $e) {
	      			http_response_code($GLOBALS['connection_error']);
          			$responseObj->status=$e->getMessage();
	      }
	      return $responseObj;
     }
    public function getMyLeads()
     {
 		 $responseObj=new stdClass;
     	 try{

	          $db = getDB();
	          $isActive=1;
	          if($_SESSION["majorRole"]=="operator")
	          {
	          	$stmt = $db->prepare("SELECT * FROM leads WHERE isActive=:isActive AND LeadOperatorId=:LeadOperatorId");
	          	$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
	          	$stmt->bindParam("LeadOperatorId", $_SESSION['userId'],PDO::PARAM_STR);
	          	$stmt->execute();
	          }
	          else if($_SESSION["majorRole"]=="manager")
	          {
	          	$stmt = $db->prepare("SELECT * FROM leads WHERE isActive=:isActive AND leadManagerId=:leadManagerId");
	          	$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
	          	$stmt->bindParam("leadManagerId", $_SESSION['userId'],PDO::PARAM_STR);
	          	$stmt->execute();
	          }
	          else
	          {
	          	http_response_code($GLOBALS['forbidden']);
           		return false;
	          }
	          $data=$stmt->fetchAll(PDO::FETCH_OBJ);
	          $responseArr=array();
	          for($i=0;$i<count($data);$i++)
	          {
	          	$x=$data[i];
	          	if(in_array($x->leadType, $_SESSION['accessLevel']))
	          	{
	          		array_push($responseArr,$x->leadType);
	          	}
	          }
	          $db = null;
	          if($responseArr)
	          {
	                http_response_code($GLOBALS['success']);
	                $responseObj->data=$responseArr;
					$responseObj->status="Success";
	          }
	          else
	          {
	                http_response_code($GLOBALS['noContent']);
          			$responseObj->data=$data;
					$responseObj->status="Nothing Found";
	          } 	
     	}
	      catch(PDOException $e) {
	      			http_response_code($GLOBALS['connection_error']);
          			$responseObj->status=$e->getMessage();
	      }
	      return $responseObj;
     }
    public function getLeadsByCustomerID($customerID)
     {
     	$responseObj=new stdClass;
     	try{
	          $db = getDB();
	          $stmt = $db->prepare("SELECT * FROM leads WHERE customerID=:customerID");
	          $stmt->bindParam("customerID", $customerID,PDO::PARAM_INT);
	          $stmt->execute();
	          $data=$stmt->fetchAll(PDO::FETCH_OBJ);
	          $db = null;
	          if($data)
	          {
	                http_response_code($GLOBALS['success']);
	                $responseObj->data=$data;
					$responseObj->status="Success";
	          }
	          else
	          {
	                http_response_code($GLOBALS['noContent']);
          			$responseObj->data=$data;
					$responseObj->status="Nothing Found";
	          } 	
     	}
	      catch(PDOException $e) {
	      			http_response_code($GLOBALS['connection_error']);
          			$responseObj->status=$e->getMessage();
	      }
	      return $responseObj;
     }
    public function getMyInfo()
     {
 	    $responseObj=new stdClass;
     	try{
			$db = getDB();
			$stmt = $db->prepare("SELECT * FROM staff_login WHERE userId=:id");
			$stmt->bindParam("id", $_SESSION['userId'],PDO::PARAM_INT);
			$stmt->execute();
			$data=$stmt->fetch(PDO::FETCH_OBJ);
			$db = null;
			if($data)
	        {
                $responseObj->majorRole=$data->majorRole;
				if($data->majorRole=="admin")
				{
					$responseObj->minorRole=array("operator","manager");
				}
				else if($data->majorRole=="manager")
				{
					$responseObj->minorRole=array("operator");
				}
				else
				{
					$responseObj->minorRole=array();
				}
				$responseObj->name=$data->name;
				if(in_array("leads",$_SESSION['accessLevel']))
				{		
					$responseObj->type="leads";
				}
				else if(in_array("fulfillment",$_SESSION['accessLevel']))
				{		
					$responseObj->type="fulfillment";
				}
				else
				{
					return false;
				}
				$responseObj->phone=$data->phone;
				if($_SESSION["majorRole"]=="manager"||$_SESSION["majorRole"]=="admin")
				{
					$responseObj->team=$this->getMyTeam();
				}
				else
				{
					return false;
				}
				$responseObj->email=$data->email;
				http_response_code($GLOBALS['success']);
                return $responseObj;
	        }
	        else
	        {
                http_response_code($GLOBALS['noContent']);
      			echo "Record Not Found";
      			return false;
	        } 	
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
	    
     }

    public function getAccessLevels($id)
     {
 	    $responseObj=new stdClass;
     	try{
			$db = getDB();
			$stmt = $db->prepare("SELECT access FROM access_level INNER JOIN access ON access.accessLevel=access_level.id AND access.userId=:id");
            $stmt->bindParam("id", $data->userId,PDO::PARAM_STR) ;
            $stmt->execute();
            $count=$stmt->rowCount();
            $access=$stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            $accessLevelArray=array();
            for($i=0;$i<count($access);$i++)
            {
                 $accessData=($access[$i]);
                 array_push($accessLevelArray,$accessData->access);
                 
            }
            return $accessLevelArray; 	
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();	
	    }
     }
    public function getCustomerDetails($id)
     {
        $responseObj=new stdClass;
     	try{
		  $db = getDB();
          $stmt = $db->prepare("SELECT * FROM staff_login WHERE userId=:id");
          $stmt->bindParam("id", $id,PDO::PARAM_INT);
          $stmt->execute();
          $data=$stmt->fetch(PDO::FETCH_OBJ);
          $db = null;
          if($data)
	        {
                http_response_code($GLOBALS['success']);
                $responseObj->data=$data;
				$responseObj->status="Success";
	        }
	        else
	        {
                http_response_code($GLOBALS['noContent']);
      			$responseObj->data=$data;
				$responseObj->status="Nothing Found";
	        } 	
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			$responseObj->status=$e->getMessage();
	    }
	    return $responseObj;
     }
    public function getMyManager()
     {
 	     $responseObj=new stdClass;
     	try{
	      $db = getDB();
          $stmt = $db->prepare("SELECT managerId FROM staff_login WHERE userId=:id");
          $stmt->bindParam("id", $_SESSION['userId'],PDO::PARAM_INT);
          $stmt->execute();
          $data=$stmt->fetch(PDO::FETCH_OBJ);
          $db = null;
          if($data)
	        {
                http_response_code($GLOBALS['success']);
                $responseObj->data=$data;
				$responseObj->status="Success";
	        }
	        else
	        {
                http_response_code($GLOBALS['noContent']);
      			$responseObj->data=$data;
				$responseObj->status="Nothing Found";
	        } 	
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			$responseObj->status=$e->getMessage();
	    }
	    return $responseObj;
     }
    public function getMyTeam()
     {
     	$responseObj=new stdClass;
     	try{
     		  $db = getDB();
              if($_SESSION["majorRole"]=="admin")
              {
              	$stmt = $db->prepare("SELECT * FROM staff_login WHERE adminId=:adminId");
              	$stmt->bindParam("adminId", $_SESSION['userId'],PDO::PARAM_INT);
	          }
	          else if($_SESSION["majorRole"]=="manager")
              {
              	$stmt = $db->prepare("SELECT * FROM staff_login WHERE leadManagerId=:leadManagerId");
              	$stmt->bindParam("leadManagerId", $_SESSION['userId'],PDO::PARAM_INT);
	          }
	          else
	          {
	          	return false;
	          }
	          $stmt->execute();
	          $data=$stmt->fetchAll(PDO::FETCH_OBJ);
	          $db = null;
	          return $data;	
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
	    
     }
    public function getLeadsByLeadOperatorID($leadOperatorId)
     {
     	$responseObj=new stdClass;
     	try{

	          $db = getDB();
	          $isActive=1;
	          $stmt = $db->prepare("SELECT * FROM leads WHERE LeadOperatorId=:LeadOperatorId");
	          $stmt->bindParam("LeadOperatorId", $LeadOperatorId,PDO::PARAM_INT);
	          $stmt->execute();
	          $data=$stmt->fetchAll(PDO::FETCH_OBJ);
	          $db = null;
	          if($data)
	        {
                http_response_code($GLOBALS['success']);
                $responseObj->data=$data;
				$responseObj->status="Success";
	        }
	        else
	        {
                http_response_code($GLOBALS['noContent']);
      			$responseObj->data=$data;
				$responseObj->status="Nothing Found";
	        } 	
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			$responseObj->status=$e->getMessage();
	    }
	    return $responseObj;
     }
    public function getLeadsByLeadManagerId($leadManagerId)
     {
     	$responseObj=new stdClass;
     	try{

	          $db = getDB();
	          $isActive=1;
	          $stmt = $db->prepare("SELECT * FROM leads WHERE leadManagerId=:leadManagerId");
	          $stmt->bindParam("leadManagerId", $leadManagerId,PDO::PARAM_INT);
	          $stmt->execute();
	          $data=$stmt->fetchAll(PDO::FETCH_OBJ);
	          $db = null;
	          if($data)
	        {
                http_response_code($GLOBALS['success']);
                $responseObj->data=$data;
				$responseObj->status="Success";
	        }
	        else
	        {
                http_response_code($GLOBALS['noContent']);
      			$responseObj->data=$data;
				$responseObj->status="Nothing Found";
	        } 	
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			$responseObj->status=$e->getMessage();
	    }
	    return $responseObj;
     }
    public function leadDelete($id)
     {
     	try{

	          $db = getDB();
	          $stmt = $db->prepare("SELECT * FROM leads WHERE id=:id");
	          $stmt->bindParam("id", $id,PDO::PARAM_INT);  
	          $stmt->execute();
	          $data=$stmt->fetch(PDO::FETCH_OBJ);
	          if($data)
	          {
	          		$isActive=0;
	          		$deletedBy=$_SESSION['userId'];
	          		$time= date('Y-m-d H:i:s',time());
	                $st=$db->prepare("UPDATE leads SET isActive=:isActive,deletedBy=:deletedBy,deletedOn=:deletedOn WHERE id=:id");
	                $st->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
	                $st->bindParam("deletedOn",$time,PDO::PARAM_STR);
                    $st->bindParam("deletedBy",$deletedBy,PDO::PARAM_INT);
	                $st->bindParam("id", $id,PDO::PARAM_INT);  
	          		$st->execute();
	          		$db=null;
	          		return true;
	          }
	          else
	          {
	          		$db=null;
	               return false;
	          } 	
     	}
	      catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
     }

    public function leadActivate($id)
     {
     	try{

	          $db = getDB();
	          $stmt = $db->prepare("SELECT * FROM leads WHERE id=:id");
	          $stmt->bindParam("id", $id,PDO::PARAM_INT);  
	          $stmt->execute();
	          $data=$stmt->fetch(PDO::FETCH_OBJ);
	          if($data)
	          {
	          		$deactivate=0;
	          		$activate=1;
	          		$lastUpdatedBy=$_SESSION['userId'];
	          		$time= date('Y-m-d H:i:s',time());
	                $st=$db->prepare("UPDATE leads SET deactivate=:deactivate,activate=:activate,lastUpdatedOn=:lastUpdatedOn,lastUpdatedBy=:lastUpdatedBy WHERE id=:id");
	                $st->bindParam("deactivate", $deactivate,PDO::PARAM_BOOL);  
	          		$st->bindParam("activate", $activate,PDO::PARAM_BOOL);  
	          		$st->bindParam("lastUpdatedOn",$time,PDO::PARAM_STR);
                    $st->bindParam("lastUpdatedBy",$lastUpdatedBy,PDO::PARAM_INT);
	                $st->bindParam("id", $id,PDO::PARAM_INT);  
	          		$st->execute();
	          		$db=null;
	          		return true;
	          }
	          else
	          {
	          		$db=null;
	               return false;
	          } 	
     	}
	      catch(PDOException $e) {
	      echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	      }   
 	 }

    public function leadDeactivate($id)
     {
     	try{

	          $db = getDB();
	          $stmt = $db->prepare("SELECT * FROM leads WHERE id=:id");
	          $stmt->bindParam("id", $id,PDO::PARAM_INT);  
	          $stmt->execute();
	          $data=$stmt->fetch(PDO::FETCH_OBJ);
	          if($data)
	          {
	          		$deactivate=1;
	          		$activate=0;
	          		$lastUpdatedBy=$_SESSION['userId'];
	          		$time= date('Y-m-d H:i:s',time());
	                $st=$db->prepare("UPDATE leads SET deactivate=:deactivate,activate=:activate,lastUpdatedOn=:lastUpdatedOn,lastUpdatedBy=:lastUpdatedBy WHERE id=:id");
	                $st->bindParam("deactivate", $deactivate,PDO::PARAM_BOOL);  
	          		$st->bindParam("activate", $activate,PDO::PARAM_BOOL);  
	          		$st->bindParam("lastUpdatedOn",$time,PDO::PARAM_STR);
                    $st->bindParam("lastUpdatedBy",$lastUpdatedBy,PDO::PARAM_INT);
	                $st->bindParam("id", $id,PDO::PARAM_INT);  
	          		$st->execute();
	          		$db=null;
	          		return true;
	          }
	          else
	          {
	          		$db=null;
	               return false;
	          } 	
     	}
	      catch(PDOException $e) {
	      echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	      }   
     }
    public function leadAccept($id)
     {
     	try{

	          $db = getDB();
	          $stmt = $db->prepare("SELECT * FROM leads WHERE id=:id");
	          $stmt->bindParam("id", $id,PDO::PARAM_INT);  
	          $stmt->execute();
	          $data=$stmt->fetch(PDO::FETCH_OBJ);
	          if($data)
	          {
	          		$accept=1;
	          		$reject=0;
	          		$lastUpdatedBy=$_SESSION['userId'];
	          		$time= date('Y-m-d H:i:s',time());
	                $st=$db->prepare("UPDATE leads SET accept=:accept,reject=:reject,lastUpdatedOn=:lastUpdatedOn,lastUpdatedBy=:lastUpdatedBy WHERE id=:id");
	                $st->bindParam("accept", $accept,PDO::PARAM_BOOL);  
	          		$st->bindParam("reject", $reject,PDO::PARAM_BOOL);  
	          		$st->bindParam("lastUpdatedOn",$time,PDO::PARAM_STR);
                    $st->bindParam("lastUpdatedBy",$lastUpdatedBy,PDO::PARAM_INT);
	                $st->bindParam("id", $id,PDO::PARAM_INT);  
	          		$st->execute();
	          		$db=null;
	          		return true;
	          }
	          else
	          {
	          		$db=null;
	               return false;
	          } 	
     	}
	      catch(PDOException $e) {
	      echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	      }   
     }
    public function leadReject($id)
     {
     	try{

	          $db = getDB();
	          $stmt = $db->prepare("SELECT * FROM leads WHERE id=:id");
	          $stmt->bindParam("id", $id,PDO::PARAM_INT);  
	          $stmt->execute();
	          $data=$stmt->fetch(PDO::FETCH_OBJ);
	          if($data)
	          {
	          		$accept=0;
	          		$reject=1;
	          		$lastUpdatedBy=$_SESSION['userId'];
	          		$time= date('Y-m-d H:i:s',time());
	                $st=$db->prepare("UPDATE leads SET accept=:accept,reject=:reject,leadStatus=:leadStatus,lastUpdatedOn=:lastUpdatedOn,lastUpdatedBy=:lastUpdatedBy WHERE id=:id");
	                $st->bindParam("accept", $accept,PDO::PARAM_BOOL);  
	          		$st->bindParam("reject", $reject,PDO::PARAM_BOOL);  
	          		$st->bindParam("lastUpdatedOn",$time,PDO::PARAM_STR);
                    $st->bindParam("lastUpdatedBy",$lastUpdatedBy,PDO::PARAM_INT);
	                $st->bindParam("id", $id,PDO::PARAM_INT);  
	          		$st->execute();
	          		$db=null;
	          		return true;
	          }
	          else
	          {
	          		$db=null;
	               return false;
	          } 	
     	}
	      catch(PDOException $e) {
	      echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	      }   
     }
     
    public function leadEquipAdd($fulfilmentOperator,$equipmentId,$leadId,$makeId,$location,$duration,$modelId,$district,$shiftType,$expectedStartDate,$projectStage,$operatorAllowance,$batha,$food,$accomodation,$operationalHoursInADay,$operationalDaysInAMonth,$vehicleDocuments,$operatorLicense)
		{
			try{
				$db = getDB();
			    $stmt = $db->prepare("INSERT INTO lead_equipment(fulfilmentOperator,equipmentId,leadId,makeId,priceTag,location,duration,modelId,district,shiftType,expectedStartDate,projectStage,operatorAllowance,batha,food,accomodation,operationalHoursInADay,operationalDaysInAMonth,vehicleDocuments,operatorLicense) VALUES (:fulfilmentOperator,:equipmentId,:leadId,:makeId,:priceTag,:location,:duration,:modelId,:district,:shiftType,:expectedStartDate,:projectStage,:operatorAllowance,:batha,:food,:accomodation,:operationalHoursInADay,:operationalDaysInAMonth,:vehicleDocuments,:operatorLicense)");  
			    
				$stmt->bindParam("fulfilmentOperator", $fulfilmentOperator,PDO::PARAM_INT) ;
				$stmt->bindParam("equipmentId", $equipmentId,PDO::PARAM_INT) ;
				$stmt->bindParam("leadId", $leadId,PDO::PARAM_INT) ;
				$stmt->bindParam("makeId", $makeId,PDO::PARAM_INT) ;
				$stmt->bindParam("location", $location,PDO::PARAM_STR) ;
				$stmt->bindParam("duration", $duration,PDO::PARAM_STR) ;
				$stmt->bindParam("modelId", $modelId,PDO::PARAM_INT) ;
				$stmt->bindParam("district", $district,PDO::PARAM_STR) ;
				$stmt->bindParam("shiftType", $shiftType,PDO::PARAM_STR) ;
				$stmt->bindParam("expectedStartDate", $expectedStartDate,PDO::PARAM_STR) ;
				$stmt->bindParam("projectStage", $projectStage,PDO::PARAM_STR) ;
				$stmt->bindParam("operatorAllowance", $operatorAllowance,PDO::PARAM_BOOL) ;
				$stmt->bindParam("batha", $batha,PDO::PARAM_STR) ;
				$stmt->bindParam("food", $food,PDO::PARAM_BOOL) ;
				$stmt->bindParam("accomodation", $accomodation,PDO::PARAM_BOOL) ;
				$stmt->bindParam("operationalHoursInADay", $operationalHoursInADay,PDO::PARAM_STR) ;
				$stmt->bindParam("operationalDaysInAMonth", $operationalDaysInAMonth,PDO::PARAM_STR) ;
				$stmt->bindParam("vehicleDocuments", $vehicleDocuments,PDO::PARAM_STR) ;
				$stmt->bindParam("operatorLicense", $operatorLicense,PDO::PARAM_STR) ;
				$stmt->bindParam("priceTag", $priceTag,PDO::PARAM_STR) ;
				return true;
			}
			catch(PDOException $e) 
			{
				echo '{"error":{"text":'. $e->getMessage() .'}}'; 
			} 

				
		}

    public function leadAdd($leadManagerId,$fulfilmentManagerId,$LeadOperatorId,$customerId,$tableName,$renterRating,$leadPriority,$leadDate,$leadType)
		{
			try{
				$db = getDB();
			    $stmt = $db->prepare("INSERT INTO leads(leadManagerId,fulfilmentManagerId,LeadOperatorId,customerId,tableName,renterRating,leadPriority,leadDate,isActive,createdOn,createdBy,lastUpdatedOn,lastUpdatedBy,deletedOn,deletedBy,leadType) VALUES (:leadManagerId,:fulfilmentManagerId,:LeadOperatorId,:customerId,:tableName,:renterRating,:leadPriority,:leadDate,:isActive,:createdOn,:createdBy,:lastUpdatedOn,:lastUpdatedBy,:deletedOn,:deletedBy,:leadType)");  
			    
			    $time = date('Y-m-d H:i:s',time());
			    $isActive=1;
			    $isVerified=0;
			    $deletedOn=$time;
			    $deletedBy=0;
		    	$createdBy=$_SESSION['userId'];
		    	$lastUpdatedBy=$_SESSION['userId'];
		    	
	    		$stmt->bindParam("leadManagerId", $leadManagerId,PDO::PARAM_INT) ;
				$stmt->bindParam("fulfilmentManagerId", $fulfilmentManagerId,PDO::PARAM_INT) ;
				$stmt->bindParam("LeadOperatorId", $LeadOperatorId,PDO::PARAM_INT) ;
				$stmt->bindParam("customerId", $customerId,PDO::PARAM_INT) ;
				$stmt->bindParam("tableName", $tableName,PDO::PARAM_STR) ;
				$stmt->bindParam("renterRating", $renterRating,PDO::PARAM_STR) ;
				$stmt->bindParam("leadPriority", $leadPriority,PDO::PARAM_STR) ;
				$stmt->bindParam("leadDate", $leadDate,PDO::PARAM_STR) ;
				$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL) ;
				$stmt->bindParam("createdOn", $time,PDO::PARAM_STR) ;
				$stmt->bindParam("createdBy", $createdBy,PDO::PARAM_INT) ;
				$stmt->bindParam("lastUpdatedOn", $time,PDO::PARAM_STR) ;
				$stmt->bindParam("lastUpdatedBy", $lastUpdatedBy,PDO::PARAM_INT) ;
				$stmt->bindParam("deletedOn", $deletedOn,PDO::PARAM_STR) ;
				$stmt->bindParam("deletedBy", $deletedBy,PDO::PARAM_INT) ;
				$stmt->bindParam("leadType", $leadType,PDO::PARAM_STR) ;
				$stmt->execute();
				$last_id = $db->lastInsertId();
				$db = null;
				return $last_id;
			}
			catch(PDOException $e) 
			{
				echo '{"error":{"text":'. $e->getMessage() .'}}'; 
			} 

		}

	public function checkLevelInDB($level)
		{
			try{
          	  $db = getDB();
	          $stmt = $db->prepare("SELECT access FROM access_level WHERE access=:access");
	          $stmt->bindParam("access", $level,PDO::PARAM_STR);
      		  $stmt->execute();
	          $data=$stmt->fetch(PDO::FETCH_OBJ);
	          $db = null;
	          if($data)
	          {
	          		return true;
	          }
	          else
	          {
	               return false;
	          }
	      }catch(PDOException $e) {
	      echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	      }   
      	
     	}

 	public function checkUserInDB($phone)
		{
			try{
          	  $db = getDB();
	          $stmt = $db->prepare("SELECT userId FROM staff_login WHERE phone=:phone");
	          $stmt->bindParam("phone", $phone,PDO::PARAM_STR);
      		  $stmt->execute();
	          $data=$stmt->fetch(PDO::FETCH_OBJ);
	          $db=null;
	          if($data)
	          {
	          		return $data->userId;
	          }
	          else
	          {
	               return false;
	          }
	      }catch(PDOException $e) {
	      
	      echo $e->getMessage(); 
	      
	      }   
      	
     	}


	public function leadUserAdd($password,$email,$name,$phone,$majorRole,$accessLevels)
	{
		try{

		  echo $_SESSION["majorRole"];
          $id=$this->checkUserInDB($phone);
          $db = getDB();
          echo $_SESSION["majorRole"];
          // echo "id";
          if($id)
          {
		   //    $accessLevels=$this->getAccessLevels($id);
		   //    $teams=array("field","order","leads","fulfillment","vendors");
		   //    $c=0;
		   //    for($j=0;$j<count($teams);$j++)
			  // {
			  // 	if(in_array($teams[$j], $accessLevels))$c++;
			  // }
			  // if($c>1){
			  // 	http_response_code($GLOBALS['forbidden']);
			  // 	return false;
			  // }

		      if($_SESSION["majorRole"]=="manager")
            	{
	                $stmt = $db->prepare("UPDATE staff_login SET managerId=:managerId,majorRole=:majorRole WHERE userId=:id");  
                	$stmt->bindParam("managerId", $_SESSION['userId'],PDO::PARAM_INT);
                	$stmt->bindParam("majorRole", $majorRole,PDO::PARAM_INT);
	      			$stmt->bindParam("id", $id,PDO::PARAM_INT);
					$stmt->execute();
      			}
      			else if($majorRole=="manager")
            	{
	                $stmt = $db->prepare("UPDATE staff_login SET adminId=:adminId,majorRole=:majorRole WHERE userId=:id");  
                	$stmt->bindParam("adminId", $_SESSION['userId'],PDO::PARAM_INT);
                	$stmt->bindParam("majorRole", $majorRole,PDO::PARAM_INT);
	      			$stmt->bindParam("id", $id,PDO::PARAM_INT);
					$stmt->execute();
      			}
      			else
            	{
	                $stmt = $db->prepare("UPDATE staff_login SET adminId=:adminId,majorRole=:majorRole WHERE userId=:id");  
                	$stmt->bindParam("adminId", $_SESSION['userId'],PDO::PARAM_INT);
                	$stmt->bindParam("majorRole", $majorRole,PDO::PARAM_INT);
	      			$stmt->bindParam("id", $id,PDO::PARAM_INT);
					$stmt->execute();
      			}
      			$db = null;
                return true;
			}
		  else{
		  	// echo"HI";
				$stmt = $db->prepare("INSERT INTO staff_login(password,email,name,phone,majorRole,adminId,managerId,isActive) VALUES (:hash_password,:email,:name,:phone,:majorRole,:adminId,:managerId,:isActive)");  
                $hash_password= hash('sha256', $password);
                $isActive=1;
                $stmt->bindParam("hash_password", $hash_password,PDO::PARAM_STR) ;
                $stmt->bindParam("email", $email,PDO::PARAM_STR) ;
                $stmt->bindParam("name", $name,PDO::PARAM_STR) ;
                $stmt->bindParam("phone", $phone,PDO::PARAM_STR) ;
                $stmt->bindParam("majorRole", $majorRole,PDO::PARAM_STR) ;
                $stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL) ;
                echo $_SESSION["majorRole"];
                if($majorRole=="operator" && $_SESSION["majorRole"]=="manager")
            	{
            		$adminId=0;
	                $stmt->bindParam("managerId", $_SESSION['userId'],PDO::PARAM_INT);
                	$stmt->bindParam("adminId", $adminId,PDO::PARAM_INT) ;
                }
      			else if($majorRole=="manager" && $_SESSION["majorRole"]=="admin")
            	{
	                $managerId=0;
	                $stmt->bindParam("adminId", $_SESSION['userId'],PDO::PARAM_INT);
	                $stmt->bindParam("managerId", $managerId,PDO::PARAM_INT);              
                }
      			else
            	{
	                if($_SESSION['majorRole']=="admin")
                	{
	                	$managerId=0;
	                	$stmt->bindParam("managerId", $managerId,PDO::PARAM_INT);     
	                 	$stmt->bindParam("adminId", $_SESSION['userId'],PDO::PARAM_INT);
                	}
                	else if($_SESSION['majorRole']=="manager")
                	{
	                	$adminId=0;
		                $stmt->bindParam("managerId", $_SESSION['userId'],PDO::PARAM_INT);
	                	$stmt->bindParam("adminId", $adminId,PDO::PARAM_INT) ;
                	}
                	else
                	{
                		$db=null;
						return false;
                	}

      			}
                $stmt->execute();
				$db=null;
				return true;
			}
           }catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
	}
	public function leadUserUpdate($password,$email,$name,$newPhone,$oldPhone)
	{
		try{
		  
		  $db = getDB();
          $id=$this->checkUserInDB($oldPhone);
          if($id)
          {

      		if($password)
      		{
			    $stmt = $db->prepare("UPDATE staff_login SET password=:hash_password,email=:email,name=:name,phone=:phone,majorRole=:majorRole,accessLevel=:accessLevel WHERE userId=:userId");  
	                    $hash_password= hash('sha256', $password);
	                    $stmt->bindParam("hash_password", $hash_password,PDO::PARAM_STR) ;
	                    $stmt->bindParam("email", $email,PDO::PARAM_STR) ;
	                    $stmt->bindParam("name", $name,PDO::PARAM_STR) ;
	                    $stmt->bindParam("phone", $newPhone,PDO::PARAM_STR) ;
	                    $stmt->bindParam("userId", $id,PDO::PARAM_INT) ;
	                    $stmt->execute();
	                    $db = null;
		                http_response_code($GLOBALS['success']);
		  				return true;
			}
			else
			{
				$stmt = $db->prepare("UPDATE staff_login SET email=:email,name=:name,phone=:phone,majorRole=:majorRole, WHERE userId=:userId");  
                $stmt->bindParam("email", $email,PDO::PARAM_STR) ;
                $stmt->bindParam("name", $name,PDO::PARAM_STR) ;
                $stmt->bindParam("phone", $newPhone,PDO::PARAM_STR) ;
                $stmt->bindParam("userId", $id,PDO::PARAM_INT) ;
                $stmt->execute();
                $db = null;
                http_response_code($GLOBALS['success']);
  				return true;
			}
		  }
			else
			{
				return false;
			}
           }catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
	}
	
	public function leadUserProfileUpdate($email,$name,$newPhone,$oldPhone)
	{
		try{
		  
		  $db = getDB();
          $id=$_SESSION['userId'];
          if($id)
          {
			$stmt = $db->prepare("UPDATE staff_login SET email=:email,name=:name,phone=:phone,majorRole=:majorRole, WHERE userId=:userId");  
            $stmt->bindParam("email", $email,PDO::PARAM_STR) ;
            $stmt->bindParam("name", $name,PDO::PARAM_STR) ;
            $stmt->bindParam("phone", $newPhone,PDO::PARAM_STR) ;
            $stmt->bindParam("userId", $id,PDO::PARAM_INT) ;
            $stmt->execute();
            $db = null;
            http_response_code($GLOBALS['success']);
			return true;
		  }
			else
			{
				return false;
			}
           }catch(PDOException $e) {
  			http_response_code($GLOBALS['connection_error']);
  			echo $e->getMessage();
	    }
	}
	

	public function leadUserAssignLevel($id,$level)
	{
		try{
		  
		  $db = getDB();
          $st = $db->prepare("SELECT * from staff_login WHERE userId=:id");  
          $st->bindParam("id", $id,PDO::PARAM_INT);
          $st->execute();
          $count=$st->rowCount();
          if($count==1)
          {
                
                $stmt = $db->prepare("SELECT id FROM access_level WHERE access=:access");
	            $stmt->bindParam("access", $level,PDO::PARAM_STR);
      		    $stmt->execute();
      		    $data=$stmt->fetch(PDO::FETCH_OBJ);
      		    if($data)
      		    {
      		    	$stmt = $db->prepare("INSERT INTO access(userId,accessLevel) VALUES (:userId,:accessLevel)");
      		    	$stmt->bindParam("userId", $id,PDO::PARAM_INT);
      		    	$stmt->bindParam("accessLevel", $data->id,PDO::PARAM_INT);
      		    	$stmt->execute();
      		    }
                $db = null;
                return true;
			}else{
				$db=null;
				return false;

           	}
           	}catch(PDOException $e){
           		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
           	}
		}

	public function leadUserDeallocateLevel($id,$level)
	{
		try{
		  
		  $db = getDB();
          $st = $db->prepare("SELECT * from staff_login WHERE userId=:id");  
          $st->bindParam("id", $id,PDO::PARAM_INT);
          $st->execute();
          $count=$st->rowCount();
          if($count==1)
          {
                
                $stmt = $db->prepare("SELECT id FROM access_level WHERE access=:access");
	            $stmt->bindParam("access", $level,PDO::PARAM_STR);
      		    $stmt->execute();
      		    $data=$stmt->fetch(PDO::FETCH_OBJ);
      		    if($data)
      		    {
      		    	$stmt = $db->prepare("DELETE from access WHERE userId=:userId,accessLevel=:accessLevel");
      		    	$stmt->bindParam("userId", $id,PDO::PARAM_INT);
      		    	$stmt->bindParam("accessLevel", $data->id,PDO::PARAM_INT);
      		    	$stmt->execute();
      		    }
                $db = null;
                return true;
			}else{
				$db=null;
				return false;

           	}
           	}catch(PDOException $e){
           		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
           	}
		}

	public function getLeadsMovedToOrders()
	{
		try{
      	  $db = getDB();
          $movedToOrders=1;
          $stmt = $db->prepare("SELECT * FROM leads WHERE movedToOrders=:movedToOrders");
          $stmt->bindParam("movedToOrders", $movedToOrders,PDO::PARAM_BOOL);
          $stmt->execute();
          $data=$stmt->fetchAll(PDO::FETCH_OBJ);
          $db = null;
          if($data)
          {
          		return $data;
          }
          else
          {
               return "noData";
          } 	
 	}
      catch(PDOException $e) {
      echo '{"error":{"text":'. $e->getMessage() .'}}'; 
      }   
	}


	public function moveToOrders($id)
	{
		try{
      $db = getDB();
      $st = $db->prepare("SELECT * from leads WHERE id=:id");  
      $st->execute();
      $count=$st->rowCount();
      if($count>=1)
      {
            $time = date('Y-m-d H:i:s',time());
            $lastUpdatedBy=$_SESSION['userId'];
            $movedToOrders=1;
	    	$stmt = $db->prepare("UPDATE leads SET lastUpdatedBy=:lastUpdatedBy,lastUpdatedOn=:lastUpdatedOn,movedToOrders=:movedToOrders WHERE id=:id");  
            $stmt->bindParam("lastUpdatedBy", $lastUpdatedBy,PDO::PARAM_INT) ;
			$stmt->bindParam("lastUpdatedOn", $time,PDO::PARAM_STR) ;
			$stmt->bindParam("movedToOrders", $movedToOrders,PDO::PARAM_BOOL);
  			$stmt->bindParam("id", $id,PDO::PARAM_INT);
  			$stmt->execute();
            $db = null;
            return true;
		}else{
			$db=null;
			return false;

       	}
       	}catch(PDOException $e){
       		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
       	}
	}

	public function setLastUpdated($id)
	{
		try{
      $db = getDB();
      $st = $db->prepare("SELECT * from leads WHERE id=:id");  
      $st->execute();
      $count=$st->rowCount();
      if($count>=1)
      {
            $time = date('Y-m-d H:i:s',time());
            $lastUpdatedBy=$_SESSION['userId'];
            $stmt = $db->prepare("UPDATE leads SET lastUpdatedBy=:lastUpdatedBy,lastUpdatedOn=:lastUpdatedOn WHERE id=:id");  
            $stmt->bindParam("lastUpdatedBy", $lastUpdatedBy,PDO::PARAM_INT) ;
			$stmt->bindParam("lastUpdatedOn", $time,PDO::PARAM_STR) ;
			$stmt->bindParam("id", $id,PDO::PARAM_INT);
  			$stmt->execute();
            $db = null;
            return true;
		}else{
			$db=null;
			return false;

       	}
       	}catch(PDOException $e){
       		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
       	}	
	}

	public function assignFulfilmentOperator($id,$fulfilmentOperator)
	{
		try{
      $db = getDB();
      $st = $db->prepare("SELECT * from lead_equipment WHERE id=:id");  
      $st->execute();
      $count=$st->rowCount();
      if($count>=1)
      {
            $stmt = $db->prepare("SELECT leadId from lead_equipment WHERE id=:id");  
            $stmt->bindParam("id", $id,PDO::PARAM_INT);
  			$stmt->execute();
            $data=$stmt->fetch(PDO::FETCH_OBJ);
      		$this->setLastUpdated($data->leadId);
            
            $stmt = $db->prepare("UPDATE lead_equipment SET fulfilmentOperator=:fulfilmentOperator WHERE id=:id");  
            $stmt->bindParam("fulfilmentOperator", $fulfilmentOperator,PDO::PARAM_INT) ;
			$stmt->bindParam("id", $id,PDO::PARAM_INT);
  			$stmt->execute();
            $db = null;
            
            return true;
		}else{
			$db=null;
			return false;

       	}
       	}catch(PDOException $e){
       		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
       	}
	}

	 public function leadEquipUpdate($id,$fulfilmentOperator,$equipmentId,$leadId,$makeId,$location,$duration,$modelId,$district,$shiftType,$expectedStartDate,$projectStage,$operatorAllowance,$batha,$food,$accomodation,$operationalHoursInADay,$operationalDaysInAMonth,$vehicleDocuments,$operatorLicense)
     {
          try{
          $db = getDB();
          $st = $db->prepare("SELECT * from lead_equipment WHERE id=:id");  
          $st->bindParam("id", $id,PDO::PARAM_INT);
          $st->execute();
          $count=$st->rowCount();
          if($count>=1)
          {
                $stmt = $db->prepare("UPDATE lead_equipment SET fulfilmentOperator=:fulfilmentOperator,equipmentId=:equipmentId,leadId=:leadId,makeId=:makeId,location=:location,duration=:duration,modelId=:modelId,district=:district,shiftType=:shiftType,expectedStartDate=:expectedStartDate,projectStage=:projectStage,operatorAllowance=:operatorAllowance,batha=:batha,food=:food,accomodation=:accomodation,operationalHoursInADay=:operationalHoursInADay,operationalDaysInAMonth=:operationalDaysInAMonth,vehicleDocuments=:vehicleDocuments,operatorLicense=:operatorLicense WHERE id=:id");  
                $stmt->bindParam("id", $id,PDO::PARAM_INT);
      			$stmt->bindParam("fulfilmentOperator", $fulfilmentOperator,PDO::PARAM_INT) ;
				$stmt->bindParam("equipmentId", $equipmentId,PDO::PARAM_INT) ;
				$stmt->bindParam("leadId", $leadId,PDO::PARAM_INT) ;
				$stmt->bindParam("makeId", $makeId,PDO::PARAM_INT) ;
				$stmt->bindParam("location", $location,PDO::PARAM_STR) ;
				$stmt->bindParam("duration", $duration,PDO::PARAM_STR) ;
				$stmt->bindParam("modelId", $modelId,PDO::PARAM_INT) ;
				$stmt->bindParam("district", $district,PDO::PARAM_STR) ;
				$stmt->bindParam("shiftType", $shiftType,PDO::PARAM_STR) ;
				$stmt->bindParam("expectedStartDate", $expectedStartDate,PDO::PARAM_STR) ;
				$stmt->bindParam("projectStage", $projectStage,PDO::PARAM_STR) ;
				$stmt->bindParam("operatorAllowance", $operatorAllowance,PDO::PARAM_BOOL) ;
				$stmt->bindParam("batha", $batha,PDO::PARAM_STR) ;
				$stmt->bindParam("food", $food,PDO::PARAM_BOOL) ;
				$stmt->bindParam("accomodation", $accomodation,PDO::PARAM_BOOL) ;
				$stmt->bindParam("operationalHoursInADay", $operationalHoursInADay,PDO::PARAM_STR) ;
				$stmt->bindParam("operationalDaysInAMonth", $operationalDaysInAMonth,PDO::PARAM_STR) ;
				$stmt->bindParam("vehicleDocuments", $vehicleDocuments,PDO::PARAM_STR) ;
				$stmt->bindParam("operatorLicense", $operatorLicense,PDO::PARAM_STR) ;
				$stmt->execute();
                $db = null;
                return true;
			}else{
				$db=null;
				return false;

           	}
           	}catch(PDOException $e){
           		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
           	}

   	}	

   	public function assignPriceTag($id,$priceTag)
     {
          try{
          $db = getDB();
          $st = $db->prepare("SELECT * from lead_equipment WHERE id=:id");  
          $st->bindParam("id", $id,PDO::PARAM_INT);
          $st->execute();
          $count=$st->rowCount();
          if($count>=1)
          {
                $stmt = $db->prepare("UPDATE lead_equipment SET priceTag=:priceTag WHERE id=:id");  
                $stmt->bindParam("id", $id,PDO::PARAM_INT);
      			$stmt->bindParam("priceTag", $priceTag,PDO::PARAM_STR) ;
				$stmt->execute();
                $db = null;
                return true;
			}else{
				$db=null;
				return false;

           	}
           	}catch(PDOException $e){
           		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
           	}

   	}	

     public function leadUpdate($id,$leadManagerId,$fulfilmentManagerId,$LeadOperatorId,$customerId,$tableName,$renterRating,$leadPriority,$leadDate)
     {
          try{
          $db = getDB();
          $st = $db->prepare("SELECT id FROM leads WHERE id=:id");  
          $st->bindParam("id", $id,PDO::PARAM_INT);
          $st->execute();
          $count=$st->rowCount();
          if($count>=1)
          {
                $stmt = $db->prepare("UPDATE leads SET leadManagerId=:leadManagerId,fulfilmentManagerId=:fulfilmentManagerId,LeadOperatorId=:LeadOperatorId,customerId=:customerId,:tableName,:renterRating,:leadPriority,:leadDate WHERE id=:id");  
                
                $time = date('Y-m-d H:i:s',time());
			    $isActive=1;
			    $isVerified=0;
			    $deletedOn=$time;
			    $deletedBy=2;
		    	$createdBy=3;
		    	$lastUpdatedBy=$_SESSION['userId'];
		    	$leadDate=$time;
	    		$expectedStartDate=$time;
	    		$lastUpdatedBy=2;
	    		$stmt->bindParam("id", $id,PDO::PARAM_INT) ;
				$stmt->bindParam("leadManagerId", $leadManagerId,PDO::PARAM_INT) ;
				$stmt->bindParam("fulfilmentManagerId", $fulfilmentManagerId,PDO::PARAM_INT) ;
				$stmt->bindParam("LeadOperatorId", $LeadOperatorId,PDO::PARAM_INT) ;
				$stmt->bindParam("customerId", $customerId,PDO::PARAM_INT) ;
				$stmt->bindParam("tableName", $tableName,PDO::PARAM_STR) ;
				$stmt->bindParam("renterRating", $renterRating,PDO::PARAM_STR) ;
				$stmt->bindParam("leadPriority", $leadPriority,PDO::PARAM_STR) ;
				$stmt->bindParam("leadDate", $leadDate,PDO::PARAM_STR) ;
				$stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL) ;
				$stmt->bindParam("createdOn", $time,PDO::PARAM_STR) ;
				$stmt->bindParam("createdBy", $createdBy,PDO::PARAM_INT) ;
				$stmt->bindParam("lastUpdatedOn", $time,PDO::PARAM_STR) ;
				$stmt->bindParam("lastUpdatedBy", $lastUpdatedBy,PDO::PARAM_INT) ;
				$stmt->bindParam("deletedOn", $deletedOn,PDO::PARAM_STR) ;
				$stmt->bindParam("deletedBy", $deletedBy,PDO::PARAM_INT) ;
				$stmt->execute();
                    $db = null;
                    return true;
			}else{
				$db=null;
				return false;

           	}
           	}catch(PDOException $e){
           		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
           	}

   	}	


}

/*
	Functions inside:
 	leadUserLogin($phone,$password)

	getLeadDetails($id)

	getAllLeads()

	getActivatedLeads()

	getAcceptedLeads()

	getMyLeads()

	getLeadsByCustomerID($customerID)

	getMyInfo()

	getCustomerDetails($id)

	getMyManager()

	getMyOperators()

	getLeadsByLeadOperatorID($leadOperatorId)

	getLeadsByLeadManagerId($leadManagerId)

	leadDelete($id)

	leadActivate($id)

	leadDeactivate($id)

	leadAccept($id)

	leadReject($id)

	leadEquipAdd($fulfilmentOperator,$equipmentId,$leadId,$makeId,$location,$duration,$modelId,$district,$shiftType,$expectedStartDate,$projectStage,$operatorAllowance,$batha,$food,$accomodation,$operationalHoursInADay,$operationalDaysInAMonth,$vehicleDocuments,$operatorLicense)

	leadAdd($leadManagerId,$fulfilmentManagerId,$LeadOperatorId,$customerId,$tableName,$renterRating,$leadPriority,$leadDate,$leadType)

	checkLevelInDB($level)

	leadUserAdd($id,$majorRole)

	leadUserAssignLevel($id,$level)

	leadUserDeallocateLevel($id,$level)

	getLeadsMovedToOrders()

	moveToOrders($id)

	setLastUpdated($id)

	assignFulfilmentOperator($id,$fulfilmentOperator)

	leadEquipUpdate($id,$fulfilmentOperator,$equipmentId,$leadId,$makeId,$location,$duration,$modelId,$district,$shiftType,$expectedStartDate,$projectStage,$operatorAllowance,$batha,$food,$accomodation,$operationalHoursInADay,$operationalDaysInAMonth,$vehicleDocuments,$operatorLicense)

	assignPriceTag($id,$priceTag)

	leadUpdate($id,$leadManagerId,$fulfilmentManagerId,$LeadOperatorId,$customerId,$tableName,$renterRating,$leadPriority,$leadDate)


 */


?>