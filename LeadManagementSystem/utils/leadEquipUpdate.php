<?php

include('config.php');
include('leadClass.php');
$leadClass = new leadClass();

if(($_SESSION['majorRole']=='operator')&&($_SESSION['accessLevel']==9)){
    
    $id=$_POST['id'];
    $fulfilmentOperator=$_SESSION['userId'];
    $equipmentId=$_POST['equipmentId'];
    $leadId=$_POST['leadId'];
    $makeId=$_POST['makeId'];
    $location=$_POST['location'];
    $duration=$_POST['duration'];
    $modelId=$_POST['modelId'];
    $district=$_POST['district'];
    $shiftType=$_POST['shiftType'];
    $expectedStartDate=$_POST['expectedStartDate'];
    $projectStage=$_POST['projectStage'];
    $operatorAllowance=$_POST['operatorAllowance'];
    $batha=$_POST['batha'];
    $food=$_POST['food'];
    $accomodation=$_POST['accomodation'];
    $operationalHoursInADay=$_POST['operationalHoursInADay'];
    $operationalDaysInAMonth=$_POST['operationalDaysInAMonth'];
    $vehicleDocuments=$_POST['vehicleDocuments'];
    $operatorLicense=$_POST['operatorLicense'];

    $lid= $leadClass->leadEquipUpdate($id,$fulfilmentOperator,$equipmentId,$leadId,$makeId,$location,$duration,$modelId,$district,$shiftType,$expectedStartDate,$projectStage,$operatorAllowance,$batha,$food,$accomodation,$operationalHoursInADay,$operationalDaysInAMonth,$vehicleDocuments,$operatorLicense);
    if($lid){
        echo "success";
    }else{
        echo "Failed";
    }
}
    

?>

