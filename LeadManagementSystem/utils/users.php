<?php
include('config.php');
include('userClass.php');
$userClass = new userClass();

if($_SESSION['majorRole']=='admin'){
	$data=$userClass->users();
	if($data!="noData")
	{
		echo json_encode($data,true);
	}
	else
	{
	    $errorMsgLogin="Error in getting details";
	    echo $errorMsgLogin;
	}
}else{
	return "Unauthorised User";
}

?>