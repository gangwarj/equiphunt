<?php
class vendorClass{
	public function renterDetails()
     {
     	try{

	          $db = getDB();
	          $isActive=1;
	          $stmt = $db->prepare("SELECT * FROM renters WHERE isActive=:isActive");
	          $stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
	          $stmt->execute();
	          $data=$stmt->fetchAll(PDO::FETCH_OBJ);
	          $db = null;
	          if($data)
	          {
	                return $data;
	          }
	          else
	          {
	               return "noData";
	          } 	
     	}
	      catch(PDOException $e) {
	      echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	      }   
     }

     public function renterDelete($id)
     {
     	try{

	          $db = getDB();
	          $stmt = $db->prepare("SELECT * FROM renters WHERE id=:id");
	          $stmt->bindParam("id", $id,PDO::PARAM_INT);  
	          $stmt->execute();
	          $data=$stmt->fetch(PDO::FETCH_OBJ);
	          if($data)
	          {
	          		$isActive=0;
	          		$time= date('Y-m-d H:i:s',time());
	                $st=$db->prepare("UPDATE renters SET isActive=:isActive,deletedBy=:deletedBy,deletedOn=:deletedOn WHERE id=:id");
	                $st->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
	                $st->bindParam("deletedOn",$time,PDO::PARAM_STR);
                    $st->bindParam("deletedBy",$_SESSION['userid'],PDO::PARAM_INT);
	                $st->bindParam("id", $id,PDO::PARAM_INT);  
	          		$st->execute();
	          		$db=null;
	          		return true;
	          }
	          else
	          {
	          		$db=null;
	               return false;
	          } 	
     	}
	      catch(PDOException $e) {
	      echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	      }   
     }

     public function renterAdd($renterType,$company,$pocName,$pocDesignation,$contactNo,$alternateContactNo,$emailId,$businessDomain,$officeLocation,$city,$address,$howDoWeKnowClient,$fromWhereDidClientGetToKnowUs,$clientFirstContactDate,$salesExecutive,$meetingDoneInPerson,$meetingRemarks,$followUpDate,$clientStatus,$renterInterestLevel,$noOfEnquiresTillNow,$smsAndEmail,$renterActiveProjects,$renterUpcomingProjects,$turnoverOfCompany,$frequencyOfEquipmentUsage,$noOfOrdersDoneTillNow,$renterPaymentPolicy,$renterPaymentHistory,$paymentCycle,$advance,$serviceTax,$secondaryResearchOnline,$feedbackOfRenterFromSupplier)
     {
          try{
          $db = getDB();
          $st = $db->prepare("SELECT id FROM renters WHERE renterType=:renterType OR company=:company");  
          $st->bindParam("renterType", $renterType,PDO::PARAM_STR);
          $st->bindParam("company", $company,PDO::PARAM_STR);
          $st->execute();
          $count=$st->rowCount();
          if($count<1)
          {
                    $stmt = $db->prepare("INSERT INTO renters(renterType,company,pocName,pocDesignation,contactNo,alternateContactNo,emailId,businessDomain,officeLocation,city,address,howDoWeKnowClient,fromWhereDidClientGetToKnowUs,clientFirstContactDate,salesExecutive,meetingDoneInPerson,meetingRemarks,followUpDate,clientStatus,renterInterestLevel,noOfEnquiresTillNow,smsAndEmail,renterActiveProjects,renterUpcomingProjects,turnoverOfCompany,frequencyOfEquipmentUsage,noOfOrdersDoneTillNow,renterPaymentPolicy,renterPaymentHistory,paymentCycle,advance,serviceTax,secondaryResearchOnline,feedbackOfRenterFromSupplier,isActive,isVerified,createdOn,createdBy,lastUpdatedOn,lastUpdatedBy) VALUES (:renterType,:company,:pocName,:pocDesignation,:contactNo,:alternateContactNo,:emailId,:businessDomain,:officeLocation,:city,:address,:howDoWeKnowClient,:fromWhereDidClientGetToKnowUs,:clientFirstContactDate,:salesExecutive,:meetingDoneInPerson,:meetingRemarks,:followUpDate,:clientStatus,:renterInterestLevel,:noOfEnquiresTillNow,:smsAndEmail,:renterActiveProjects,:renterUpcomingProjects,:turnoverOfCompany,:frequencyOfEquipmentUsage,:noOfOrdersDoneTillNow,:renterPaymentPolicy,:renterPaymentHistory,:paymentCycle,:advance,:serviceTax,:secondaryResearchOnline,:feedbackOfRenterFromSupplier,:isActive,:isVerified,:createdOn,:createdBy,:lastUpdatedOn,:lastUpdatedBy)");  
                    $isActive=1;
                    $isVerified=0;
                    $time = date('Y-m-d H:i:s',time());
                    $stmt->bindParam("renterType", $renterType,PDO::PARAM_STR) ;
                    $stmt->bindParam("company", $company,PDO::PARAM_STR) ;
                    $stmt->bindParam("pocName", $pocName,PDO::PARAM_STR) ;
                    $stmt->bindParam("pocDesignation", $pocDesignation,PDO::PARAM_STR) ;
                    $stmt->bindParam("contactNo", $contactNo,PDO::PARAM_STR) ;
                    $stmt->bindParam("alternateContactNo", $alternateContactNo,PDO::PARAM_STR) ;
                    $stmt->bindParam("emailId", $emailId,PDO::PARAM_STR) ;
                    $stmt->bindParam("businessDomain", $businessDomain,PDO::PARAM_STR) ;
                    $stmt->bindParam("officeLocation", $officeLocation,PDO::PARAM_STR) ;
                    $stmt->bindParam("city", $city,PDO::PARAM_STR) ;
                    $stmt->bindParam("address", $address,PDO::PARAM_STR) ;
                    $stmt->bindParam("howDoWeKnowClient", $howDoWeKnowClient,PDO::PARAM_STR) ;
                    $stmt->bindParam("fromWhereDidClientGetToKnowUs", $fromWhereDidClientGetToKnowUs,PDO::PARAM_STR) ;
                    $stmt->bindParam("clientFirstContactDate", $clientFirstContactDate,PDO::PARAM_STR) ;
                    $stmt->bindParam("salesExecutive", $salesExecutive,PDO::PARAM_STR) ;
                    $stmt->bindParam("meetingDoneInPerson", $meetingDoneInPerson,PDO::PARAM_BOOL) ;
                    $stmt->bindParam("meetingRemarks", $meetingRemarks,PDO::PARAM_STR) ;
                    $stmt->bindParam("followUpDate", $followUpDate,PDO::PARAM_STR) ;
                    $stmt->bindParam("clientStatus", $clientStatus,PDO::PARAM_STR) ;
                    $stmt->bindParam("renterInterestLevel", $renterInterestLevel,PDO::PARAM_STR) ;
                    $stmt->bindParam("noOfEnquiresTillNow", $noOfEnquiresTillNow,PDO::PARAM_STR) ;
                    $stmt->bindParam("smsAndEmail", $smsAndEmail,PDO::PARAM_BOOL) ;
                    $stmt->bindParam("renterActiveProjects", $renterActiveProjects,PDO::PARAM_STR) ;
                    $stmt->bindParam("renterUpcomingProjects", $renterUpcomingProjects,PDO::PARAM_STR) ;
                    $stmt->bindParam("turnoverOfCompany", $address,PDO::PARAM_STR) ;
                    $stmt->bindParam("frequencyOfEquipmentUsage", $frequencyOfEquipmentUsage,PDO::PARAM_STR) ;
                    $stmt->bindParam("noOfOrdersDoneTillNow", $noOfOrdersDoneTillNow,PDO::PARAM_STR) ;
                    $stmt->bindParam("renterPaymentPolicy", $renterPaymentPolicy,PDO::PARAM_STR) ;
                    $stmt->bindParam("renterPaymentHistory", $renterPaymentHistory,PDO::PARAM_STR) ;
                    $stmt->bindParam("paymentCycle", $paymentCycle,PDO::PARAM_STR) ;
                    $stmt->bindParam("advance", $advance,PDO::PARAM_STR) ;
                    $stmt->bindParam("serviceTax", $serviceTax,PDO::PARAM_STR) ;
                    $stmt->bindParam("secondaryResearchOnline", $secondaryResearchOnline,PDO::PARAM_STR) ;
                    $stmt->bindParam("feedbackOfRenterFromSupplier", $feedbackOfRenterFromSupplier,PDO::PARAM_STR) ;
                    $stmt->bindParam("isActive", $isActive,PDO::PARAM_BOOL);
                    $stmt->bindParam("isVerified",$isVerified,PDO::PARAM_BOOL);
                    $stmt->bindParam("createdOn",$time,PDO::PARAM_STR);
                    $stmt->bindParam("createdBy",$_SESSION['userid'],PDO::PARAM_INT);
                    $stmt->bindParam("lastUpdatedOn",$time,PDO::PARAM_STR);
                    $stmt->bindParam("lastUpdatedBy",$_SESSION['userid'],PDO::PARAM_INT);
                    $stmt->execute();
                    $db = null;
                    return true;
			}else{
				$db=null;
				return false;

           	}
           }catch(PDOException $e) {
	      echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	      } 

	  }


     public function renterUpdate($id,$contactNo,$alternateContactNo,$emailId,$businessDomain,$meetingDoneInPerson,$meetingRemarks,$followUpDate,$clientStatus,$noOfEnquiresTillNow,$smsAndEmail,$renterActiveProjects,$renterUpcomingProjects,$turnoverOfCompany,$frequencyOfEquipmentUsage,$noOfOrdersDoneTillNow,$renterPaymentPolicy,$renterPaymentHistory,$paymentCycle,$advance,$serviceTax,$secondaryResearchOnline,$feedbackOfRenterFromSupplier,$isVerified)
     {
          try{
          $db = getDB();
          $st = $db->prepare("SELECT id FROM renters WHERE contactNo=:contactNo OR email=:email");  
          $st->bindParam("contactNo", $contactNo,PDO::PARAM_STR);
          $st->bindParam("email", $emailId,PDO::PARAM_STR);
          $st->execute();
          $count=$st->rowCount();
          if($count<1)
          {
                    $stmt = $db->prepare("UPDATE renters SET contactNo=:contactNo,alternateContactNo=:alternateContactNo,emailId=:emailId,businessDomain=:businessDomain,meetingDoneInPerson=:meetingDoneInPerson,meetingRemarks=:meetingRemarks,followUpDate=:followUpDate,clientStatus=:clientStatus,noOfEnquiresTillNow=:noOfEnquiresTillNow,smsAndEmail=:smsAndEmail,renterActiveProjects=:renterActiveProjects,renterUpcomingProjects=:renterUpcomingProjects,turnoverOfCompany=:turnoverOfCompany,frequencyOfEquipmentUsage=:frequencyOfEquipmentUsage,noOfOrdersDoneTillNow=:noOfOrdersDoneTillNow,renterPaymentPolicy=:renterPaymentPolicy,renterPaymentHistory=:renterPaymentHistory,paymentCycle=:paymentCycle,advance=:advance,serviceTax=:serviceTax,secondaryResearchOnline=:secondaryResearchOnline,feedbackOfRenterFromSupplier=:feedbackOfRenterFromSupplier,isVerified=:isVerified,lastUpdatedOn=:lastUpdatedOn,lastUpdatedBy=:lastUpdatedBy WHERE id=:id");  
                    
                    $time = date('Y-m-d H:i:s',time());
                    
                    $stmt->bindParam("contactNo", $contactNo,PDO::PARAM_STR) ;
                    $stmt->bindParam("alternateContactNo", $alternateContactNo,PDO::PARAM_STR) ;
                    $stmt->bindParam("emailId", $emailId,PDO::PARAM_STR) ;
                    $stmt->bindParam("businessDomain", $businessDomain,PDO::PARAM_STR) ;
                    $stmt->bindParam("meetingDoneInPerson", $meetingDoneInPerson,PDO::PARAM_BOOL) ;
                    $stmt->bindParam("meetingRemarks", $meetingRemarks,PDO::PARAM_STR) ;
                    $stmt->bindParam("followUpDate", $followUpDate,PDO::PARAM_STR) ;
                    $stmt->bindParam("clientStatus", $clientStatus,PDO::PARAM_STR) ;
                    $stmt->bindParam("noOfEnquiresTillNow", $noOfEnquiresTillNow,PDO::PARAM_STR) ;
                    $stmt->bindParam("smsAndEmail", $smsAndEmail,PDO::PARAM_BOOL) ;
                    $stmt->bindParam("renterActiveProjects", $renterActiveProjects,PDO::PARAM_STR) ;
                    $stmt->bindParam("renterUpcomingProjects", $renterUpcomingProjects,PDO::PARAM_STR) ;
                    $stmt->bindParam("turnoverOfCompany", $address,PDO::PARAM_STR) ;
                    $stmt->bindParam("frequencyOfEquipmentUsage", $frequencyOfEquipmentUsage,PDO::PARAM_STR) ;
                    $stmt->bindParam("noOfOrdersDoneTillNow", $noOfOrdersDoneTillNow,PDO::PARAM_STR) ;
                    $stmt->bindParam("renterPaymentPolicy", $renterPaymentPolicy,PDO::PARAM_STR) ;
                    $stmt->bindParam("renterPaymentHistory", $renterPaymentHistory,PDO::PARAM_STR) ;
                    $stmt->bindParam("paymentCycle", $paymentCycle,PDO::PARAM_STR) ;
                    $stmt->bindParam("advance", $advance,PDO::PARAM_STR) ;
                    $stmt->bindParam("serviceTax", $serviceTax,PDO::PARAM_STR) ;
                    $stmt->bindParam("secondaryResearchOnline", $secondaryResearchOnline,PDO::PARAM_STR) ;
                    $stmt->bindParam("feedbackOfRenterFromSupplier", $feedbackOfRenterFromSupplier,PDO::PARAM_STR) ;
                    $stmt->bindParam("isVerified",$isVerified,PDO::PARAM_BOOL);
                    $stmt->bindParam("lastUpdatedOn",$time,PDO::PARAM_STR);
                    $stmt->bindParam("lastUpdatedBy",$_SESSION['userid'],PDO::PARAM_INT);
                    $stmt->execute();
                    $db = null;
                    return true;
			}else{
				$db=null;
				return false;

           	}
           	}catch(PDOException $e){
           		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
           	}

   	}	


}


?>