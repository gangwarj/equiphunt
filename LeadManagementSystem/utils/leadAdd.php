<?php

include('config.php');
include('leadClass.php');
$leadClass = new leadClass();


if((($_SESSION['majorRole']=='operator')||($_SESSION['majorRole']=='manager'))&&(in_array("leads",$_SESSION['accessLevel']))){
    
    $data=$_POST['data'];
    if($_SESSION['majorRole']=='operator')
    {
        $LeadOperatorId=$_SESSION['userId'];
        $leadManagerId=$leadClass->getMyManager();
    }
    if($_SESSION['majorRole']=='manager')
    {
        $leadManagerId=$_SESSION['userId'];
        $LeadOperatorId=0;
    }
    $customerId=$data['customerId'];
    $tableName=$data['tableName'];
    $renterRating=$data['renterRating'];
    $leadPriority=$data['leadPriority'];
    $leadDate=$data['leadDate'];
    $leadType=$data['leadType'];
    $equipments=$data['equipments'];
    $lid= $leadClass->leadAdd($leadManagerId,$fulfilmentManagerId,$LeadOperatorId,$customerId,$tableName,$renterRating,$leadPriority,$leadDate,$leadType);
    if($lid)
    {
        foreach ($equipment as $equipments)
        {
            $fulfilmentOperator=$equipment['fulfilmentOperator'];
            $equipmentId=$equipment['equipmentId'];
            $leadId=$lid;
            $makeId=$equipment['makeId'];
            $location=$equipment['location'];
            $duration=$equipment['duration'];
            $modelId=$equipment['modelId'];
            $district=$equipment['district'];
            $shiftType=$equipment['shiftType'];
            $expectedStartDate=$equipment['expectedStartDate'];
            $projectStage=$equipment['projectStage'];
            $operatorAllowance=$equipment['operatorAllowance'];
            $batha=$equipment['batha'];
            $food=$equipment['food'];
            $accomodation=$equipment['accomodation'];
            $operationalHoursInADay=$equipment['operationalHoursInADay'];
            $operationalDaysInAMonth=$equipment['operationalDaysInAMonth'];
            $vehicleDocuments=$equipment['vehicleDocuments'];
            $operatorLicense=$equipment['operatorLicense'];
            $priceTag=$equipment['priceTag'];
        
            $leid= $leadClass->leadEquipAdd($fulfilmentOperator,$equipmentId,$leadId,$makeId,$location,$duration,$modelId,$district,$shiftType,$expectedStartDate,$projectStage,$operatorAllowance,$batha,$food,$accomodation,$operationalHoursInADay,$operationalDaysInAMonth,$vehicleDocuments,$operatorLicense,$priceTag);
            if($leid){
            echo "added equipment $equipmentId";
            }
            else{
                echo "failed to add equipment";
            }
        }
    }
    else
    {
        echo "Unable to add lead";
    }
    
    
}
    

?>

